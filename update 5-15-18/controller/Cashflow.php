<?php

Class Cashflow extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('ssp');
        $this->load->model('Model_cashflow');
        $this->load->library('upload');
    }

    function data() {
        // nama tabel
        $table = 'tbl_cashflow';
        // nama PK
        $primaryKey = 'id_cash';
        // list field
        $columns = array(
            array('db' => 'id_cash', 'dt' => 'id_cash'),
            array('db' => 'no_kwitansi', 'dt' => 'no_kwitansi'),
            array('db' => 'tgl', 'dt' => 'tgl'),
            array('db' => 'rek_asal', 'dt' => 'rek_asal'),
            array('db' => 'nama_rek_asal', 'dt' => 'nama_rek_asal'),
            array('db' => 'rek_tujuan', 'dt' => 'rek_tujuan'),
            array('db' => 'nama_rek_tujuan', 'dt' => 'nama_rek_tujuan'),
            array('db' => 'tujuan_transfer', 'dt' => 'tujuan_transfer'),
            array('db' => 'keterangan', 'dt' => 'keterangan'),
            
            array(
                'db' => 'id_cash',
                'dt' => 'aksi',
                'formatter' => function( $d) {
                    //return "<a href='edit.php?id=$d'>EDIT</a>";
                    return anchor('cashflow/edit/'.$d,'<i class="fa fa-edit"></i>','class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"').' 
                        '.anchor('cashflow/delete/'.$d,'<i class="fa fa-trash"></i>','onclick="return konfirmasi()" class="btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Delete"');
                }
            )
        );

        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
        );
    }

    function index() {
        $this->template->load('template', 'cashflow/list');
    }

    function add() {
        if (isset($_POST['submit'])) {

            $this->Model_cashflow->save();
            redirect('cashflow');
        } else {
            $this->template->load('template', 'cashflow/add');
        }
    }
    
    function edit(){
        if(isset($_POST['submit'])){
        	
            $this->Model_cashflow->update();
            redirect('cashflow');
        }else{
            $id_cashflow      = $this->uri->segment(3);
            $data['cashflow'] = $this->db->get_where('tbl_cashflow',array('id_cash'=>$id_cashflow))->row_array();
            $this->template->load('template', 'cashflow/edit',$data);
        }
    }
    
    function delete(){
        $id_cashflow = $this->uri->segment(3);
        if(!empty($id_cashflow)){
            // proses delete data
            $this->db->where('id_cash',$id_cashflow);
            $this->db->delete('tbl_cashflow');
        }
        redirect('cashflow');
    }


}


