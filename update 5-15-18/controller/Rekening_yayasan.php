<?php

Class Rekening_yayasan extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('ssp');
        $this->load->model('Model_rekening_yayasan');
        $this->load->library('upload');
    }

    function data() {
        // nama tabel
        $table = 'tbl_rekening_yayasan';
        // nama PK
        $primaryKey = 'id_rek';
        // list field
        $columns = array(
            array('db' => 'id_rek', 'dt' => 'id_rek'),
            array('db' => 'id_rek', 'dt' => 'id_rek'),
            array('db' => 'norek', 'dt' => 'norek'),
            array('db' => 'bank', 'dt' => 'bank'),
            array('db' => 'nama', 'dt' => 'nama'),
            array('db' => 'peruntukan', 'dt' => 'peruntukan'),
            array('db' => 'saldo', 'dt' => 'saldo',
                   'formatter'=> function($d){
                    return number_format($d, 2);
                   } 

                ),
            array(
                'db' => 'id_rek',
                'dt' => 'aksi',
                'formatter' => function( $d) {
                    //return "<a href='edit.php?id=$d'>EDIT</a>";
                    return anchor('rekening_yayasan/edit/'.$d,'<i class="fa fa-edit"></i>','class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"').' 
                        '.anchor('rekening_yayasan/delete/'.$d,'<i class="fa fa-trash"></i>','onclick="return konfirmasi()" class="btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Delete"');
                }
            )
        );

        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
        );
    }

    function index() {
        $this->template->load('template', 'rekening_yayasan/list');
    }

    function add() {
        if (isset($_POST['submit'])) {

            $this->Model_rekening_yayasan->save();
            redirect('rekening_yayasan');
        } else {
            $this->template->load('template', 'rekening_yayasan/add');
        }
    }
    
    function edit(){
        if(isset($_POST['submit'])){
        	
            $this->Model_rekening_yayasan->update();
            redirect('rekening_yayasan');
        }else{
            $id_rekening_yayasan      = $this->uri->segment(3);
            $data['rekening_yayasan'] = $this->db->get_where('tbl_rekening_yayasan',array('id_rek'=>$id_rekening_yayasan))->row_array();
            $this->template->load('template', 'rekening_yayasan/edit',$data);
        }
    }
    
    function delete(){
        $id_rekening_yayasan = $this->uri->segment(3);
        if(!empty($id_rekening_yayasan)){
            // proses delete data
            $this->db->where('id_rek',$id_rekening_yayasan);
            $this->db->delete('tbl_rekening_yayasan');
        }
        redirect('rekening_yayasan');
    }


}


