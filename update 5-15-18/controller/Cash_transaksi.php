<?php

Class Cash_transaksi extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('ssp');
        $this->load->model('Model_cash_transaksi');
        $this->load->library('upload');
    }

    function data() {
        // nama tabel
        $table = 'tbl_cash_transaksi';
        // nama PK
        $primaryKey = 'id_ctran';
        // list field
        $columns = array(
            array('db' => 'id_ctran', 'dt' => 'id_ctran'),
            array('db' => 'id_ctran', 'dt' => 'id_ctran'),
            array('db' => 'tgl', 'dt' => 'tgl'),
            array('db' => 'pemberi', 'dt' => 'pemberi'),
            array('db' => 'penerima', 'dt' => 'penerima'),
            array('db' => 'norek', 'dt' => 'norek'),
            array('db' => 'keperluan', 'dt' => 'keperluan'),
            array('db' => 'keterangan', 'dt' => 'keterangan'),
            array(
                'db' => 'id_ctran',
                'dt' => 'aksi',
                'formatter' => function( $d) {
                    //return "<a href='edit.php?id=$d'>EDIT</a>";
                    return anchor('cash_transaksi/edit/'.$d,'<i class="fa fa-edit"></i>','class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"').' 
                        '.anchor('cash_transaksi/delete/'.$d,'<i class="fa fa-trash"></i>','onclick="return konfirmasi()" class="btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Delete"');
                }
            )
        );

        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
        );
    }

    function index() {
        $this->template->load('template', 'cash_transaksi/list');
    }

    function add() {
        if (isset($_POST['submit'])) {

            $this->Model_cash_transaksi->save();
            redirect('cash_transaksi');
        } else {
            $this->template->load('template', 'cash_transaksi/add');
        }
    }
    
    function edit(){
        if(isset($_POST['submit'])){
        	
            $this->Model_cash_transaksi->update();
            redirect('cash_transaksi');
        }else{
            $id_cash_transaksi      = $this->uri->segment(3);
            $data['cash_transaksi'] = $this->db->get_where('tbl_cash_transaksi',array('id_ctran'=>$id_cash_transaksi))->row_array();
            $this->template->load('template', 'cash_transaksi/edit',$data);
        }
    }
    
    function delete(){
        $id_cash_transaksi = $this->uri->segment(3);
        if(!empty($id_cash_transaksi)){
            // proses delete data
            $this->db->where('id_ctran',$id_cash_transaksi);
            $this->db->delete('tbl_cash_transaksi');
        }
        redirect('cash_transaksi');
    }


}


