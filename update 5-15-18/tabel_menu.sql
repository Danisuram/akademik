/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.1.19-MariaDB : Database - akademik
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`akademik` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `akademik`;

/*Table structure for table `tabel_menu` */

DROP TABLE IF EXISTS `tabel_menu`;

CREATE TABLE `tabel_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_menu` varchar(50) NOT NULL,
  `link` varchar(50) NOT NULL,
  `icon` varchar(30) NOT NULL,
  `is_main_menu` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

/*Data for the table `tabel_menu` */

insert  into `tabel_menu`(`id`,`nama_menu`,`link`,`icon`,`is_main_menu`) values 
(1,'DATAbase SISWA','siswa','fa fa-users',0),
(2,'DATAbase GURU','guru','fa fa-graduation-cap',0),
(3,'keuangan_guru','keuangan_guru','fa fa-shopping-cart',0),
(8,'data sekolah','sekolah','fa fa-building',0),
(9,'Data master','#','fa fa-bars',0),
(10,'Mata Pelajaran','mapel','fa fa-book',9),
(11,'Ruangan Kelas','ruangan','fa fa-building',9),
(12,'Jurusan','jurusan','fa fa-th-large',9),
(13,'Tahun Akademik','tahunakademik','fa fa-calendar-o',9),
(14,'Jadwal pelajaran','jadwal','fa fa-calendar',0),
(15,'Rombongan Belajar','rombel','fa fa-users',9),
(16,'laporan nilai','nilai','fa fa-file-excel-o',0),
(17,'Pengguna sistem','users','fa fa-cubes',0),
(19,'Kurikulum','kurikulum','fa fa-newspaper-o',9),
(20,'Wali Kelas','walikelas','fa fa-users',0),
(21,'form pembayaran','keuangan/form','fa fa-shopping-cart',0),
(22,'Peserta Didik','siswa/siswa_aktif','fa fa-graduation-cap',0),
(23,'jenis pembayaran','jenis_pembayaran','fa fa-credit-card',0),
(24,'setup biaya','keuangan/setup','fa fa-graduation-cap',0),
(25,'Raport Online','raport','fa fa-graduation-cap',0),
(26,'SMS GATEWAY','sms','fa fa-envelope-o',0),
(27,'phonebook','sms_group','fa fa-book',26),
(28,'form sms','sms','fa fa-keyboard-o',26),
(29,'Rekening Yayasan','rekening_yayasan','fa fa-paperclip',0),
(30,'Cash Transaksi','cash_transaksi','fa fa-calculator',0),
(31,'Cashflow','cashflow','	fa fa-bar-chart',0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
