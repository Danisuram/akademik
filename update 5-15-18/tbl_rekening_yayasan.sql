/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.1.19-MariaDB 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `tbl_rekening_yayasan` (
	`id_rek` int (11),
	`norek` varchar (60),
	`bank` varchar (300),
	`nama` varchar (300),
	`peruntukan` varchar (60),
	`saldo` Decimal (15)
); 
insert into `tbl_rekening_yayasan` (`id_rek`, `norek`, `bank`, `nama`, `peruntukan`, `saldo`) values('1','23123','mandiri','anu','demo','987789.00');
insert into `tbl_rekening_yayasan` (`id_rek`, `norek`, `bank`, `nama`, `peruntukan`, `saldo`) values('3','9999','mandiri','test nama','`beli nasi','9000000.00');
insert into `tbl_rekening_yayasan` (`id_rek`, `norek`, `bank`, `nama`, `peruntukan`, `saldo`) values('4','9999',NULL,NULL,NULL,NULL);
