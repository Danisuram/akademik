/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.1.19-MariaDB : Database - akademik
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`akademik` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `akademik`;

/*Table structure for table `tbl_user_rule` */

DROP TABLE IF EXISTS `tbl_user_rule`;

CREATE TABLE `tbl_user_rule` (
  `id_rule` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(11) NOT NULL,
  `id_level_user` int(11) NOT NULL,
  PRIMARY KEY (`id_rule`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_user_rule` */

insert  into `tbl_user_rule`(`id_rule`,`id_menu`,`id_level_user`) values 
(3,1,1),
(4,2,1),
(5,8,1),
(6,14,2),
(7,1,2),
(8,16,3),
(10,21,5),
(11,9,1),
(12,10,1),
(13,11,1),
(14,12,1),
(15,13,1),
(16,14,1),
(17,17,1),
(18,19,1),
(19,20,1),
(20,14,3),
(25,22,1),
(26,23,5),
(27,24,5),
(28,25,3),
(29,26,1),
(30,26,5),
(31,3,1),
(32,29,1),
(33,30,1),
(34,31,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
