/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.1.19-MariaDB 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `tbl_cashflow` (
	`id_cash` int (11),
	`no_kwitansi` varchar (150),
	`tgl` date ,
	`rek_asal` varchar (60),
	`nama_rek_asal` varchar (300),
	`rek_tujuan` varchar (60),
	`nama_rek_tujuan` varchar (300),
	`tujuan_transfer` varchar (300),
	`keterangan` text 
); 
insert into `tbl_cashflow` (`id_cash`, `no_kwitansi`, `tgl`, `rek_asal`, `nama_rek_asal`, `rek_tujuan`, `nama_rek_tujuan`, `tujuan_transfer`, `keterangan`) values('1','1234448888','2018-05-01','877878787',NULL,'79877987','anak baru','beli susu','beli macem2');
insert into `tbl_cashflow` (`id_cash`, `no_kwitansi`, `tgl`, `rek_asal`, `nama_rek_asal`, `rek_tujuan`, `nama_rek_tujuan`, `tujuan_transfer`, `keterangan`) values('2','3123','0212-02-11','121','ghjffghjdsgfdfsgsadf','12312','123123','123123',' sfwerwe3rw');
insert into `tbl_cashflow` (`id_cash`, `no_kwitansi`, `tgl`, `rek_asal`, `nama_rek_asal`, `rek_tujuan`, `nama_rek_tujuan`, `tujuan_transfer`, `keterangan`) values('3','asdasd','0002-12-11','asdasds','hgdrjtr6tymfgjnd','kljlj','kjkhk','8977','lkjlkjlkjlk ');
insert into `tbl_cashflow` (`id_cash`, `no_kwitansi`, `tgl`, `rek_asal`, `nama_rek_asal`, `rek_tujuan`, `nama_rek_tujuan`, `tujuan_transfer`, `keterangan`) values('4','56','1221-12-12','5656','i;tuyrertwtjyrfadfssgdf','565656','tert tujuan','gak tau','test lagi ');
