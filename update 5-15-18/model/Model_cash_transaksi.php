<?php

class Model_cash_transaksi extends CI_Model {

    public $table ="tbl_cash_transaksi";
    
    function save() {
        $data = array(
            'tgl'           => $this->input->post('tgl', TRUE),
            'pemberi'       => $this->input->post('pemberi', TRUE),
            'penerima'      => $this->input->post('penerima', TRUE),
            'norek'         => $this->input->post('norek', TRUE),
            'keperluan'     => $this->input->post('keperluan', TRUE),
            'keterangan'    => $this->input->post('keterangan', TRUE)
           
        );
        $this->db->insert($this->table,$data);
    }
    
    function update() {
        $data = array(
            'tgl'           => $this->input->post('tgl', TRUE),
            'pemberi'       => $this->input->post('pemberi', TRUE),
            'penerima'      => $this->input->post('penerima', TRUE),
            'norek'         => $this->input->post('norek', TRUE),
            'keperluan'     => $this->input->post('keperluan', TRUE),
            'keterangan'    => $this->input->post('keterangan', TRUE)
        );
        $id_rekening_yayasan   = $this->input->post('id_ctran');
        $this->db->where('id_ctran',$id_rekening_yayasan);
        $this->db->update($this->table,$data);
    }
    
    

    
}