<?php

class Model_rekening_yayasan extends CI_Model {

    public $table ="tbl_rekening_yayasan";
    
    function save() {
        $data = array(
            'norek'         => $this->input->post('norek', TRUE),
            'bank'          => $this->input->post('bank', TRUE),
            'nama'          => $this->input->post('nama', TRUE),
            'peruntukan'    => $this->input->post('peruntukan', TRUE),
            'saldo'         => $this->input->post('saldo', TRUE)
           
        );
        $this->db->insert($this->table,$data);
    }
    
    function update() {
        $data = array(
            'norek'         => $this->input->post('norek', TRUE),
            'bank'          => $this->input->post('bank', TRUE),
            'nama'          => $this->input->post('nama', TRUE),
            'peruntukan'    => $this->input->post('peruntukan', TRUE),
            'saldo'         => $this->input->post('saldo', TRUE)
        );
        $id_rekening_yayasan   = $this->input->post('id_rek');
        $this->db->where('id_rek',$id_rekening_yayasan);
        $this->db->update($this->table,$data);
    }
    
    

    
}