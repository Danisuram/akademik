<?php

class Model_cashflow extends CI_Model {

    public $table ="tbl_cashflow";
    
    function save() {
        $data = array(
        	'no_kwitansi'   	=> $this->input->post('no_kwitansi', TRUE),
            'tgl'           	=> $this->input->post('tgl', TRUE),
            'rek_asal'      	=> $this->input->post('rek_asal', TRUE),
            'nama_rek_asal' 	=> $this->input->post('nama_rek_asal', TRUE),
            'rek_tujuan'    	=> $this->input->post('rek_tujuan', TRUE),
            'nama_rek_tujuan'	=> $this->input->post('nama_rek_tujuan', TRUE),
            'tujuan_transfer'	=> $this->input->post('tujuan_transfer', TRUE),
            'keterangan'    	=> $this->input->post('keterangan', TRUE)
           
        );
        $this->db->insert($this->table,$data);
    }
    
    function update() {
        $data = array(
            'no_kwitansi'   	=> $this->input->post('no_kwitansi', TRUE),
            'tgl'           	=> $this->input->post('tgl', TRUE),
            'rek_asal'      	=> $this->input->post('rek_asal', TRUE),
            'nama_rek_asal' 	=> $this->input->post('nama_rek_asal', TRUE),
            'rek_tujuan'    	=> $this->input->post('rek_tujuan', TRUE),
            'nama_rek_tujuan'	=> $this->input->post('nama_rek_tujuan', TRUE),
            'tujuan_transfer'	=> $this->input->post('tujuan_transfer', TRUE),
            'keterangan'    	=> $this->input->post('keterangan', TRUE)
        );
        $id_cash   = $this->input->post('id_cash');
        $this->db->where('id_cash',$id_cash);
        $this->db->update($this->table,$data);
    }
    
    

    
}