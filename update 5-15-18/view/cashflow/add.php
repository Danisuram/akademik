<div class="col-sm-12">
    <!-- start: TEXT FIELDS PANEL -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
            Text Fields
            <div class="panel-tools">
                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                </a>
                <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                    <i class="fa fa-wrench"></i>
                </a>
                <a class="btn btn-xs btn-link panel-refresh" href="#">
                    <i class="fa fa-refresh"></i>
                </a>
                <a class="btn btn-xs btn-link panel-expand" href="#">
                    <i class="fa fa-resize-full"></i>
                </a>
                <a class="btn btn-xs btn-link panel-close" href="#">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">

            <?php
            echo form_open_multipart('cashflow/add', 'role="form" class="form-horizontal"');
            ?>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    No Kwitansi
                </label>
                <div class="col-sm-9">
                    <input type="text" name="no_kwitansi" placeholder="No Kwitansi" id="form-field-1" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Tanggal
                </label>
                
                <div class="col-sm-2">
                    <input type="date" name="tgl" placeholder="Tanggal " id="form-field-1" class="form-control" required = ''>
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Rek Asal
                </label>
                <div class="col-sm-9">
                    <input type="text" name="rek_asal" placeholder="Rek Asal" id="form-field-1" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Nama Rek Asal
                </label>
                <div class="col-sm-9">
                    <input type="text" name="nama_rek_asal " placeholder="Nama Rek Asal" id="form-field-1" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Rek Tujuan
                </label>
                
                <div class="col-sm-9">
                    <input type="text" name="rek_tujuan" placeholder="Rek Tujuan" id="form-field-1" class="form-control" required = ''>
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Nama Rek Tujuan 
                </label>
                
                <div class="col-sm-9">
                    <input type="text" name="nama_rek_tujuan" placeholder="Nama Rek Tujuan" id="form-field-1" class="form-control" required = ''>
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Tujuan Transfer
                </label>
                
                <div class="col-sm-9">
                    <input type="text" name="tujuan_transfer" placeholder="Tujuan Transfer" id="form-field-1" class="form-control" required = ''>
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Keterangan
                </label>
                
                <div class="col-sm-9">
                    <textarea type="text" name="keterangan" placeholder="Keterangan" id="form-field-1" class="form-control" required = ''> </textarea>
                </div>  
            </div>
            

            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">

                </label>
                <div class="col-sm-1">
                    <button type="submit" name="submit" class="btn btn-danger  btn-sm">SIMPAN</button>
                </div>
                <div class="col-sm-1">
                    <?php echo anchor('cashflow', 'Kembali', array('class' => 'btn btn-info btn-sm')); ?>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- end: TEXT FIELDS PANEL -->
</div>