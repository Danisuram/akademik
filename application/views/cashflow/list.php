<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-external-link-square"></i> Dynamic Chart
            <div class="panel-tools">

            </div>
        </div>
        <div class="panel-body">
             <div id="container" ></div>
        </div>
        <div class="panel-body">
             <div id="containerx" ></div>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.0/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.js"></script>
<script
    type="text/javascript" language="JavaScript">
        function konfirmasi()
        {
         tanya = confirm("Anda Yakin Akan Menghapus Data ?");
         if (tanya == true) return true;
         else return false;
        }
 </script>

  <script>

  var categories = [<?php echo $datenya;?>].map(function(date) {
      let formatOptions = { month: '2-digit', day: '2-digit', year: 'numeric' };
      return new Date(date).toLocaleDateString(undefined, formatOptions);
    });

  Highcharts.chart('container', {
    chart: {
      type: 'line'
    },
    dateRangeGrouping: true,
    title: {
      text: 'Transaksi Rekening Yayasan'
    },
    xAxis: {
      categories: categories
    },
    yAxis: {
      title: {
        text: 'Rp'
      }
    },
    plotOptions: {
      line: {
        dataLabels: {
          enabled: true
        },
        enableMouseTracking: false
      }
    },
    series: [{
      name: 'Outcome',
      data: [
        <?php echo $dataout;?>
      ]
    }, {
      name: 'Income',
      data: [
        <?php echo $datain;?>
      ]
    }]
  });

  var categoriesx = [<?php echo $datenya_cash;?>].map(function(date) {
      let formatOptions = { month: '2-digit', day: '2-digit', year: 'numeric' };
      return new Date(date).toLocaleDateString(undefined, formatOptions);
    });

  Highcharts.chart('containerx', {
    chart: {
      type: 'line'
    },
    dateRangeGrouping: true,
    title: {
      text: 'Transaksi Cash'
    },
    xAxis: {
      categories: categoriesx
    },
    yAxis: {
      title: {
        text: 'Rp'
      }
    },
    plotOptions: {
      line: {
        dataLabels: {
          enabled: true
        },
        enableMouseTracking: false
      }
    },
    series: [{
      name: 'Outcome',
      data: [
        <?php echo $dataout_cash;?>
      ]
    }, {
      name: 'Income',
      data: [
        <?php echo $datain_cash;?>
      ]
    }]
  });
</script>
