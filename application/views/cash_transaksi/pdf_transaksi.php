<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->
   
    <div class="panel panel-default">
         <h1 align="center"> <b>TRANSAKSI CASH</b></h1>
         <br>
         <br>
            <table id="mytable" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                    <tr>
                        <td><b>NOMOR BUKTI</b></td>
                        <td>:<?php echo $data_transaksi[0]; ?></td>
                        <td><b>TANGGAL TRANSAKSI</b></td>
                        <td>:<?php echo $data_transaksi[1]; ?></td>
                    </tr>
                    <tr>
                        <td><b>JENIS PEMBAYARAN</b></td>
                        <td> :<?php echo $data_transaksi[4]; ?></td>
                    </tr>
            </table>
         <hr>
         <br>
         <br>
        <div class="panel-body">
            <table id="mytable" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%" border="1">
                    <tr>
                        <td align="center" colspan="2"><b>PEMBERI</b></td>
                        
                        <td align="center" colspan="2"><b>PENERIMA</b></td>
                        
                    </tr>

                    <tr>
                        <td><b>NAMA </b></td>
                        <td><?php echo $data_transaksi[2]; ?></td>
                        <td><b>NAMA</b></td>
                        <td><?php echo $data_transaksi[3]; ?></td>
                    </tr>
                    <tr>
                        <td><b>NOMINAL TRANSFER</b></td>
                        <td>Rp. <?php echo $data_transaksi[7]; ?>,00</td>
                    </tr>
                    <tr>
                        <td><b>KETERANGAN</b></td>
                        <td><?php echo $data_transaksi[8] ?></td>
                    </tr>
                    <tr>
                        <td><b>KETERKAITAN</b></td>
                        <td><?php echo $data_transaksi[5] ?></td>
                    </tr>



            </table>


        </div>
        <h3 align="right">Tanda Tangan</h1>
        <table align="right">
            <tr>
                <td>
                    Pemberi
                </td>
                <td>
                    Penerima
                </td>
            </tr>
            <tr>
                <td>(.................................)</td>
                <td>(.................................)</td>
            </tr>
        </table>

    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>