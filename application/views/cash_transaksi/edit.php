<div class="col-sm-12">
    <!-- start: TEXT FIELDS PANEL -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
            Text Fields
            <div class="panel-tools">
                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                </a>
                <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                    <i class="fa fa-wrench"></i>
                </a>
                <a class="btn btn-xs btn-link panel-refresh" href="#">
                    <i class="fa fa-refresh"></i>
                </a>
                <a class="btn btn-xs btn-link panel-expand" href="#">
                    <i class="fa fa-resize-full"></i>
                </a>
                <a class="btn btn-xs btn-link panel-close" href="#">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">

            <?php
            echo form_open_multipart('cash_transaksi/edit', 'role="form" class="form-horizontal"');
            echo form_hidden('id_ctran', $cash_transaksi['id_ctran']);
            ?>
             <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Tanggal Transaksi
                </label>
                
                <div class="col-sm-2">
                    <input type="date" value="<?php echo $cash_transaksi['tgl']?>" name="tanggal_transaksi" placeholder="Tanggal transaksi"  class="form-control" required = '' id="tanggal">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Pemberi
                </label>
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $cash_transaksi['pemberi']?>" name="pemberi" placeholder="Pemberi" id="form-field-1" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Penerima
                </label>
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $cash_transaksi['penerima']?>" name="penerima" placeholder="Penerima" id="form-field-1" class="form-control">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Cashflow
                </label>
                <div class="col-sm-9">
                    <select name="cashflow" onchange="showJP(this)">
                        <option value="<?php echo $cash_transaksi['cashflow']?>"><?php echo $cash_transaksi['cashflow']?></option>
                    </select>
                </div>
            </div>
            <div class="form-group"  >
                <label class="col-sm-2 control-label" for="form-field-1">
                    Jenis Pembayaran
                </label>     
                <div class="col-sm-6">
                   <select name="jenispembayaran" id="jenispembayaran">
                         <option value="<?php echo $cash_transaksi['jenis_pembayaran']?>"><?php echo $cash_transaksi['jenis_pembayaran']?></option>
                    </select>
                </div> 
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Keterkaitan
                </label>
                <div class="col-sm-9">
                    <select name="keterkaitan" onchange="showKeterkaitan(this)">
                        <option value="<?php echo $cash_transaksi['keterkaitan']?>"><?php echo $cash_transaksi['keterkaitan']?></option>
                    </select>
                </div>
            </div>
            <div class="form-group" style="display: none;" id="kelas">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Kelas
                </label>  
                <div class="col-sm-6">
                   <select name="kelas" onchange="showNamasiswa(this)">
                        <option selected="selected">--PILIH KELAS--</option>
                        <?php foreach($ruangKelas as $x):?>
                            <option value="<?=$x['kd_ruangan'];?>"><?php echo $x['nama_ruangan']; ?></option>
                         <?php endforeach;?>   
                    </select>
                </div> 
            </div>
            <div class="form-group"  id="namasiswa">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Nama Keterkaitan
                </label>  
                <div class="col-sm-6">
                   <select name="nama_ket" id="nk">
                    
                    <option value="<?php echo $cash_transaksi['nama_ket']?>"><?php echo $cash_transaksi['nama_ket']?></option>
                    </select>
                </div> 
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Nominal
                </label>
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $cash_transaksi['nominal']?>" name="nominal" placeholder="Nominal" id="form-field-1" class="form-control" required = ''>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Keterangan
                </label>
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $cash_transaksi['keterangan']?>" name="keterangan" placeholder="Keterangan" id="keteranganx" class="form-control" required = ''>
                </div>  
            </div>
           
           
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">

                </label>
                <div class="col-sm-1">
                    <button type="submit" name="submit" class="btn btn-danger btn-sm">SIMPAN</button>
                </div>
                <div class="col-sm-1">
                    <?php echo anchor('cash_transaksi', 'Kembali', array('class' => 'btn btn-info btn-sm')); ?>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- end: TEXT FIELDS PANEL -->
</div>

<script>
    function showJP(a){
        //alert(a.value);
        $("#jenispembayaran").show();
        $.ajax({
            type: "POST",
            url: "getJP/"+a.value ,
            data: $("#tbl_jenis_pembayaran").serialize(),
            success: function(datanya){
                var data = JSON.parse(datanya); 
                var htm = "";

                    $.each(data, function(i){
                         htm += "<option>"+data[i].nama_jenis_pembayaran+"</option>";
                         //$('<option></option>', {text:value}).attr('value', value).appendTo('#ns');
                    })
                    
                    $('#jenispembayaran').html(htm);
            },
            error:function(){
            //toastr["error"]("Something wrong, Please inform administrator.", "Warning alert!");
            }
        });
    } 

    function showNamasiswa(a){
        //alert(a.value); 
        $.ajax({
            type: "POST",
            url: "getkelas/"+a.value ,
            data: $("#tbl_siswa").serialize(),
            success: function(datanya){
                var data = JSON.parse(datanya); 
                var htm = "";

                    $.each(data, function(i){
                         htm += "<option>"+data[i].nama+"</option>";
                         //$('<option></option>', {text:value}).attr('value', value).appendTo('#ns');
                    })
                    
                    $('#nk').html(htm);
            },
            error:function(){
            //toastr["error"]("Something wrong, Please inform administrator.", "Warning alert!");
            }
        });
    }

    function showKeterkaitan(a){
        var ktan = a.value;
        if(ktan=="siswa"){
            $("#kelas").show();
        }else if(ktan=="guru/karyawan"){
            $("#kelas").hide();
            $.ajax({
            type: "POST",
            url: "getguru/"+a.value ,
            data: $("#tbl_guru").serialize(),
            success: function(datanya){
                var data = JSON.parse(datanya); 
                var htm = "";

                    $.each(data, function(i){
                         htm += "<option>"+data[i].nama_guru+"</option>";
                         //$('<option></option>', {text:value}).attr('value', value).appendTo('#ns');
                    })
                    
                    $('#nk').html(htm);
            },
            error:function(){
            //toastr["error"]("Something wrong, Please inform administrator.", "Warning alert!");
            }
        });
        }else if(ktan=="lain-lain"){
            $("#kelas").hide();
            document.getElementById('nk').value="";
        }
    }
</script>