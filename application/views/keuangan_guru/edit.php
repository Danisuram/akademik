<div class="col-sm-12">
    <!-- start: TEXT FIELDS PANEL -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
            Text Fields
            <div class="panel-tools">
                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                </a>
                <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                    <i class="fa fa-wrench"></i>
                </a>
                <a class="btn btn-xs btn-link panel-refresh" href="#">
                    <i class="fa fa-refresh"></i>
                </a>
                <a class="btn btn-xs btn-link panel-expand" href="#">
                    <i class="fa fa-resize-full"></i>
                </a>
                <a class="btn btn-xs btn-link panel-close" href="#">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">

            <?php
            echo form_open('keuangan_guru/edit', 'role="form" class="form-horizontal"');
            echo form_hidden('id_tran', $keuangan_guru['id_tran']);
            ?>

           <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    No Induk Pegawai
                </label>
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $keuangan_guru['nip'] ?>" name="nip" placeholder="No Induk Pegawai" id="form-field-1" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Tanggal Pembayaran
                </label>
                
                <div class="col-sm-2">
                    <input type="date" value="<?php echo $keuangan_guru['tgl_pembayaran'] ?>" name="tgl_pembayaran" placeholder="Tanggal Pembayaran" id="form-field-1" class="form-control" required = ''>
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Jumlah
                </label>
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $keuangan_guru['jumlah'] ?>" name="jumlah" placeholder="Jumlah" id="form-field-1" class="form-control">
                </div>
            </div>
             <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Pembayaran
                </label>
                <div class="col-sm-5">
                    <input type="text" value="<?php echo $keuangan_guru['pembayaran'] ?>" name="pembayaran" placeholder="Pembayaran" id="form-field-1" class="form-control" required = ''>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Rek Asal
                </label>
                
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $keuangan_guru['rek_asal'] ?>" name="rek_asal" placeholder="Rek Asal" id="form-field-1" class="form-control" required = ''>
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Rek Tujuan
                </label>
                
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $keuangan_guru['rek_tujuan'] ?>" name="rek_tujuan" placeholder="Rek Tujuan" id="form-field-1" class="form-control" required = ''>
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">

                </label>
                <div class="col-sm-1">
                    <button type="submit" name="submit" class="btn btn-danger btn-sm">SIMPAN</button>
                </div>
                <div class="col-sm-1">
                    <?php echo anchor('keuangan_guru', 'Kembali', array('class' => 'btn btn-info btn-sm')); ?>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- end: TEXT FIELDS PANEL -->
</div>