<div class="col-sm-12">
    <!-- start: TEXT FIELDS PANEL -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
            Text Fields
            <div class="panel-tools">
                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                </a>
                <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                    <i class="fa fa-wrench"></i>
                </a>
                <a class="btn btn-xs btn-link panel-refresh" href="#">
                    <i class="fa fa-refresh"></i>
                </a>
                <a class="btn btn-xs btn-link panel-expand" href="#">
                    <i class="fa fa-resize-full"></i>
                </a>
                <a class="btn btn-xs btn-link panel-close" href="#">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">

            <?php
            echo form_open_multipart('pengajaran/add', 'role="form" class="form-horizontal"');
            ?>

            
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    NIP
                </label>
             <div class="col-sm-6">
                <select name="nip" onchange="showNamaguru(this)">
                    <option selected="selected">--PILIH GURU--</option>
                        <?php foreach($nip as $x):?>
                            <option value="<?=$x['nip'];?>"><?php echo $x['nip']; ?></option>
                         <?php endforeach;?>   
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Nama Guru
                </label>
                <div class="col-sm-9">
                    <input type="text" name="nama_guru" id="ng"  readonly="" placeholder="Nama Guru" id="form-field-1" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Kode Kelas
                </label>
             <div class="col-sm-6">
                <select name="kd_kelas" onchange="showRuangkelas(this)">
                    <option selected="selected">--PILIH KELAS--</option>
                        <?php foreach($kelas as $x):?>
                            <option value="<?=$x['kd_ruangan'];?>"><?php echo $x['kd_ruangan']; ?></option>
                         <?php endforeach;?>   
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Ruang Kelas
                </label>
                <div class="col-sm-9">
                    <input type="text" name="kelas" id="kelas"  readonly="" placeholder="Ruang Kelas" id="form-field-1" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Kode Mapel
                </label>
             <div class="col-sm-6">
                <select name="kd_mapel" onchange="showMatapelajaran(this)">
                    <option selected="selected">--PILIH MATA PELAJARAN--</option>
                        <?php foreach($mapel as $x):?>
                            <option value="<?=$x['kd_mapel'];?>"><?php echo $x['kd_mapel']; ?></option>
                         <?php endforeach;?>   
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Mata Pelajaran
                </label>
                <div class="col-sm-9">
                    <input type="text" name="mapel" id="mapel"  readonly="" placeholder="Mata Pelajaran" id="form-field-1" class="form-control">
                </div>
            </div>



            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">

                </label>
                <div class="col-sm-1">
                    <button type="submit" name="submit" class="btn btn-danger  btn-sm">SIMPAN</button>
                </div>
                <div class="col-sm-1">
                    <?php echo anchor('pengajaran', 'Kembali', array('class' => 'btn btn-info btn-sm')); ?>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- end: TEXT FIELDS PANEL -->
</div>


<script>
    function showNamaguru(a){
        //alert(a.value);
        $.ajax({
            type: "POST",
            url: "getNamaguru/"+a.value ,
            data: $("#tbl_guru").serialize(),
            success: function(datanya){
                var data = JSON.parse(datanya); 
                $("#ng").val(data.nama_guru);  
            },
            error:function(){
            //toastr["error"]("Something wrong, Please inform administrator.", "Warning alert!");
            }
        });
    }

    function showRuangkelas(a){
        $.ajax({
            type: "POST",
            url: "getRuangkelas/"+a.value ,
            data: $("#tbl_ruangan").serialize(),
            success: function(datanya){
                var data = JSON.parse(datanya); 
                $("#kelas").val(data.nama_ruangan);  
            },
            error:function(){
            //toastr["error"]("Something wrong, Please inform administrator.", "Warning alert!");
            }
        });
    }

    function showMatapelajaran(a){
        $.ajax({
            type: "POST",
            url: "getMatapelajaran/"+a.value ,
            data: $("#tbl_ruangan").serialize(),
            success: function(datanya){
                var data = JSON.parse(datanya); 
                $("#mapel").val(data.nama_mapel);  
            },
            error:function(){
            //toastr["error"]("Something wrong, Please inform administrator.", "Warning alert!");
            }
        });
    }
</script>