<div class="col-sm-12">
    <!-- start: TEXT FIELDS PANEL -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
            Text Fields
            <div class="panel-tools">
                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                </a>
                <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                    <i class="fa fa-wrench"></i>
                </a>
                <a class="btn btn-xs btn-link panel-refresh" href="#">
                    <i class="fa fa-refresh"></i>
                </a>
                <a class="btn btn-xs btn-link panel-expand" href="#">
                    <i class="fa fa-resize-full"></i>
                </a>
                <a class="btn btn-xs btn-link panel-close" href="#">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">

            <?php
            echo form_open_multipart('rekening/edit', 'role="form" class="form-horizontal"');
            echo form_hidden('norek', $rekening['norek']);
            ?>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    NO REKENING
                </label>
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $rekening['norek'] ?>" readonly="" placeholder="NO REKENING" id="form-field-1" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    NAMA REKENING
                </label>
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $rekening['nama_rek'] ?>" placeholder="Nama Bank" id="form-field-1" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    NAMA BANK
                </label>
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $rekening['nama_bank'] ?>" readonly="" placeholder="Nama Bank" id="form-field-1" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    NAMA PEMILIK REKENING
                </label>
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $rekening['nama_pemilik_rek'] ?>" readonly="" placeholder="A/N" id="form-field-1" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    SALDO
                </label>
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $rekening['saldo'] ?>" name="saldo" placeholder="SALDO" id="form-field-1" class="form-control" required = ''>
                </div>
            </div>
            
            
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">

                </label>
                <div class="col-sm-1">
                    <button type="submit" name="submit" class="btn btn-danger btn-sm">SIMPAN</button>
                </div>
                <div class="col-sm-1">
                    <?php echo anchor('rekening', 'Kembali', array('class' => 'btn btn-info btn-sm')); ?>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- end: TEXT FIELDS PANEL -->
</div>