<div class="col-sm-12">
    <!-- start: TEXT FIELDS PANEL -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
            Text Fields
            <div class="panel-tools">
                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                </a>
                <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                    <i class="fa fa-wrench"></i>
                </a>
                <a class="btn btn-xs btn-link panel-refresh" href="#">
                    <i class="fa fa-refresh"></i>
                </a>
                <a class="btn btn-xs btn-link panel-expand" href="#">
                    <i class="fa fa-resize-full"></i>
                </a>
                <a class="btn btn-xs btn-link panel-close" href="#">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">

            <?php
            echo form_open_multipart('rekening_yayasan/add', 'role="form" class="form-horizontal"');
            ?>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Tanggal Transaksi
                </label>
                
                <div class="col-sm-2">
                    <input type="date" value="<?php echo $currentdate = date('Y-m-d')?>" name="tanggal_transaksi" placeholder="Tanggal transaksi"  class="form-control" required = '' id="tanggal">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    No Rekening yayasan
                </label>
             <div class="col-sm-6">
                <select name="norek_yayasan" onchange="showDatarekening(this)">
                    <option selected="selected">--PILIH REKENING--</option>
                        <?php foreach($norek as $x):?>
                            <option value="<?=$x['norek'];?>"><?php echo $x['norek']; ?></option>
                         <?php endforeach;?>   
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Nama Rekening
                </label>
                <div class="col-sm-9">
                    <input type="text" name="nama_rek" placeholder="No. Rekening Yayasan" id="nry" class="form-control" required = '' readonly="">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Bank Yayasan
                </label>
                <div class="col-sm-9">
                    <input type="text" name="bank_yayasan" placeholder="Bank Yayasan" id="by" class="form-control" required = '' readonly="">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Nama Pemilik Rekening Yayasan
                </label>
                <div class="col-sm-9">
                    <input type="text" name="nama_pemilik_rek_yayasan" placeholder="Nama Pemilik Rekening Yayasan" id="npry" class="form-control" required = '' readonly="">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Nama
                </label>
                <div class="col-sm-9">
                    <input type="text" name="nama" placeholder="Nama" id="form-field-1" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    No. Rekening 
                </label>
                <div class="col-sm-9">
                    <input type="text" name="norek" placeholder="No.Rekening" id="form-field-1" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Bank 
                </label>
                <div class="col-sm-9">
                    <input type="text" name="bank" placeholder="Bank" id="form-field-1" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Cashflow
                </label>
                <div class="col-sm-9">
                    <select name="cashflow" onchange="showJP(this)">
                        <option selected="selected">--CASHFLOW--</option>
                        <option value="income">Income</option>
                        <option value="outcome">Outcome</option>
                    </select>
                </div>
            </div>
            <div class="form-group"  >
                <label class="col-sm-2 control-label" for="form-field-1">
                    Jenis Pembayaran
                </label>     
                <div class="col-sm-6">
                   <select name="jenispembayaran" id="jenispembayaran">
                         
                    </select>
                </div> 
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Keterkaitan
                </label>
                <div class="col-sm-9">
                    <select name="keterkaitan" onchange="showKeterkaitan(this)">
                        <option selected="selected">--KETERKAITAN--</option>
                        <option value="siswa">Siswa</option>
                        <option value="guru/karyawan">Guru/Karyawan</option>
                        <option value="lain-lain">Lain-lain</option>
                    </select>
                </div>
            </div>
            <div class="form-group" style="display: none;" id="kelas">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Kelas
                </label>  
                <div class="col-sm-6">
                   <select name="kelas" onchange="showNIMsiswa(this)" id="kls">
                        <option selected="selected">--PILIH KELAS--</option>
                        <?php foreach($ruangKelas as $x):?>
                            <option value="<?=$x['kd_ruangan'];?>"><?php echo $x['nama_ruangan']; ?></option>
                         <?php endforeach;?>   
                    </select>
                </div> 
            </div>
            <div class="form-group"  style="display: none;" id="nomor_induk">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Nomor Induk
                </label>  
                <div class="col-sm-6">
                   <select name="nomor_induk" id="ni" onchange="showNamasiswa(this)">
                    
                       
                    </select>
                </div> 
            </div>
            <div class="form-group"  id="namasiswa">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Nama Keterkaitan
                </label>  
                <div class="col-sm-6">
                   <select name="namaket" id="nk">
                    
                       
                    </select>
                </div> 
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Nominal
                </label>
                <div class="col-sm-9">
                    <input type="text" name="nominal" placeholder="Nominal" id="form-field-1" class="form-control" required = ''>
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Keterangan
                </label>
                <div class="col-sm-9">
                    <input type="text" name="keterangan" placeholder="Keterangan" id="keteranganx" class="form-control" required = ''>
                </div>  
            </div>
            
            

            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">

                </label>
                <div class="col-sm-1">
                    <button type="submit" name="submit" class="btn btn-danger  btn-sm">SIMPAN</button>
                </div>
                <div class="col-sm-1">
                    <?php echo anchor('rekening_yayasan', 'Kembali', array('class' => 'btn btn-info btn-sm')); ?>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- end: TEXT FIELDS PANEL -->
</div>

<script>
    function functionUbah(a){
        //alert(a.value);
        var jenis = a.value;
        if(jenis=="Bayar SPP"){
            $("#nim").show();
            $("#nip").hide();
        }else if(jenis=="Gaji"){
            $("#nip").show();
            $("#nim").hide();
        } else{
            $("#nim").hide();
            $("#nip").hide();
        }
    }
    function showJP(a){
        //alert(a.value);
        $("#jenispembayaran").show();
        $.ajax({
            type: "POST",
            url: "getJP/"+a.value ,
            data: $("#tbl_jenis_pembayaran").serialize(),
            success: function(datanya){
                var data = JSON.parse(datanya); 
                var htm = "";

                    $.each(data, function(i){
                         htm += "<option>"+data[i].nama_jenis_pembayaran+"</option>";
                         //$('<option></option>', {text:value}).attr('value', value).appendTo('#ns');
                    })
                    
                    $('#jenispembayaran').html(htm);
            },
            error:function(){
            //toastr["error"]("Something wrong, Please inform administrator.", "Warning alert!");
            }
        });
    } 

    function showDatarekening(a){
        //alert(a.value);
        $.ajax({
            type: "POST",
            url: "getdatarekening/"+a.value ,
            data: $("#tbl_rekening").serialize(),
            success: function(datanya){
                var data = JSON.parse(datanya); 
                $("#nry").val(data.nama_rek);
                $("#by").val(data.nama_bank);
                $("#npry").val(data.nama_pemilik_rek);
            },
            error:function(){
       //toastr["error"]("Something wrong, Please inform administrator.", "Warning alert!");
            }
        });
    }

    

    function showNIMsiswa(a){
        $("#nomor_induk").show();
        $.ajax({
            type: "POST",
            url: "getkelas/"+a.value ,
            data: $("#tbl_siswa").serialize(),
            success: function(datanya){
                var data = JSON.parse(datanya); 
                var htm = "";

                    $.each(data, function(i){
                         htm += "<option>"+data[i].nim+"</option>";
                         //$('<option></option>', {text:value}).attr('value', value).appendTo('#ns');
                         console.log(data[i].nim);
                    })
                    $('#ni').html(htm);
            },
            error:function(){
            //toastr["error"]("Something wrong, Please inform administrator.", "Warning alert!");
            }
        });
    }

    function showNamasiswa(a){
        //alert(a.value);
        $.ajax({
            type: "POST",
            url: "getNIM/"+a.value ,
            data: $("#tbl_siswa").serialize(),
            success: function(datanya){
                var data = JSON.parse(datanya); 
                var htm = "";

                    $.each(data, function(i){
                         htm += "<option>"+data[i].nama+"</option>";
                         //$('<option></option>', {text:value}).attr('value', value).appendTo('#ns');
                         console.log(data[i].nama);
                    })
                    $('#nk').html(htm);
            },
            error:function(){
            //toastr["error"]("Something wrong, Please inform administrator.", "Warning alert!");
            }
        });
    }

    function showKeterkaitan(a){
        var ktan = a.value;
        if(ktan=="siswa"){
            $("#kelas").show();
            $("#nomor_induk").show();
        }else if(ktan=="guru/karyawan"){
            $("#nomor_induk").hide();
            $("#kelas").hide();
            $.ajax({
            type: "POST",
            url: "getguru/"+a.value ,
            data: $("#tbl_guru").serialize(),
            success: function(datanya){
                var data = JSON.parse(datanya); 
                var htm = "";

                    $.each(data, function(i){
                         htm += "<option>"+data[i].nama_guru+"</option>";
                         //$('<option></option>', {text:value}).attr('value', value).appendTo('#ns');
                    })
                    
                    $('#nk').html(htm);
            },
            error:function(){
            //toastr["error"]("Something wrong, Please inform administrator.", "Warning alert!");
            }
        });
        }else if(ktan=="lain-lain"){
            $("#kelas").hide();
            $("#nomor_induk").hide();
            document.getElementById('nk').value="";
            document.getElementById('ni').value="";
            document.getElementById('kls').value="";
        }
    }

    



    function getnip(a){ 
        $.ajax({
            type: "POST",
            url: "getnip/"+a.value ,
            data: $("#tbl_guru").serialize(),
            success: function(datanya){
                var data = JSON.parse(datanya); 
        
                $("#keteranganx").val(data.nama_guru+data.nip);
            },
            error:function(){
            //toastr["error"]("Something wrong, Please inform administrator.", "Warning alert!");
            }
        });
    }

    function getnim(a){ 
         $.ajax({
            type: "POST",
            url: "getnim/"+a.value ,
            data: $("#tbl_siswa").serialize(),
            success: function(datanya){
                var data = JSON.parse(datanya); 
            
            $("#keteranganx").val(data.nama); 
         },
         error:function(){
       //toastr["error"]("Something wrong, Please inform administrator.", "Warning alert!");
        }
    });
}
</script>
