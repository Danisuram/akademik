<div class="col-sm-12">
    <!-- start: TEXT FIELDS PANEL -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
            Text Fields
            <div class="panel-tools">
                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                </a>
                <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                    <i class="fa fa-wrench"></i>
                </a>
                <a class="btn btn-xs btn-link panel-refresh" href="#">
                    <i class="fa fa-refresh"></i>
                </a>
                <a class="btn btn-xs btn-link panel-expand" href="#">
                    <i class="fa fa-resize-full"></i>
                </a>
                <a class="btn btn-xs btn-link panel-close" href="#">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">

            <?php
            echo form_open_multipart('rekening_yayasan/edit', 'role="form" class="form-horizontal"');
            echo form_hidden('id_transaksiRekening', $rekening_yayasan['id_transaksiRekening']);
            ?>



            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Tanggal Transaksi
                </label>
                
                <div class="col-sm-2">
                    <input type="date" name="tanggal_transaksi" placeholder="Tanggal transaksi"  class="form-control" required = '' id="tanggal" value="<?php echo $rekening_yayasan['tgl_transaksi']?>">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    No Rekening yayasan
                </label>
             <div class="col-sm-6">
                <select name="norek_yayasan" value="<?php echo $rekening_yayasan['id_rek']?>" onchange="showDatarekening(this)">


                       <option value="<?php echo $rekening_yayasan['norek_yayasan']?>"><?php echo $rekening_yayasan['norek_yayasan']?></option>

                       <?php foreach($norek as $x):?>
                            <option value="<?=$x['norek'];?>"><?php echo $x['norek']; ?></option>
                         <?php endforeach;?>   
                         
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Nama Rekening
                </label>
                <div class="col-sm-9">
                    <input type="text" readonly value="<?php echo $rekening_yayasan['nama_rek']?>" name="nama_rek" placeholder="No. Rekening Yayasan" id="nry" class="form-control" required = ''>
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Bank Yayasan
                </label>
                <div class="col-sm-9">
                    <input type="text" readonly value="<?php echo $rekening_yayasan['bank_yayasan']?>" name="bank_yayasan" placeholder="Bank Yayasan" id="by" class="form-control" required = ''>
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Nama Pemilik Rekening Yayasan
                </label>
                <div class="col-sm-9">
                    <input type="text" readonly value="<?php echo $rekening_yayasan['nama_pemilik_rek_yayasan']?>" name="nama_pemilik_rek_yayasan" placeholder="Nama Pemilik Rekening Yayasan" id="npry" class="form-control" required = ''>
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Nama
                </label>
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $rekening_yayasan['nama']?>" name="nama" placeholder="Nama" id="form-field-1" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    No. Rekening 
                </label>
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $rekening_yayasan['norek']?>" name="norek" placeholder="No.Rekening" id="form-field-1" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Bank 
                </label>
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $rekening_yayasan['bank']?>" name="bank" placeholder="Bank" id="form-field-1" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Cashflow
                </label>
                <div class="col-sm-9">
                    <select value="<?php echo $rekening_yayasan['cashflow']?>" name="cashflow" onchange="showJP(this)">
                        
                        <option value="<?php echo $rekening_yayasan['cashflow']?>" ><?php echo $rekening_yayasan['cashflow']?></option>
                    </select>
                </div>
            </div>
            <div class="form-group"  >
                <label class="col-sm-2 control-label" for="form-field-1">
                    Jenis Pembayaran
                </label>     
                <div class="col-sm-6">
                   <select value="<?php echo $rekening_yayasan['jenis_pembayaran']?>" name="jenispembayaran" id="jenispembayaran">
                         <option><?php echo $rekening_yayasan['jenis_pembayaran']?></option>
                    </select>
                </div> 
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Keterkaitan
                </label>
                <div class="col-sm-9">
                    <select value="<?php echo $rekening_yayasan['keterkaitan']?>" name="keterkaitan" onchange="showKeterkaitan(this)">
                        <option value="<?php echo $rekening_yayasan['keterkaitan']?>"><?php echo $rekening_yayasan['keterkaitan']?></option>
                    </select>
                </div>
            </div>
            <div class="form-group" style="display: none;" id="kelas">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Kelas
                </label>  
                <div class="col-sm-6">
                   <select  name="kelas" onchange="showNIMasiswa(this)">
                        <option selected="selected">--PILIH KELAS--</option>
                        <?php foreach($ruangKelas as $x):?>
                            <option value="<?=$x['kd_ruangan'];?>"><?php echo $x['nama_ruangan']; ?></option>
                         <?php endforeach;?>   
                    </select>
                </div> 
            </div>
            <div class="form-group"  style="display: none;" id="nomor_induk">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Nomor Induk
                </label>  
                <div class="col-sm-6">
                   <select value="<?php echo $rekening_yayasan['nomor_induk']?>" name="nomor_induk" id="ni" onchange="showNamasiswa(this)">
                        <option><?php echo $rekening_yayasan['nomor_induk']?></option>
                    
                       
                    </select>
                </div> 
            </div>
            <div class="form-group"  id="namasiswa">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Nama Keterkaitan
                </label>  
                <div class="col-sm-6">
                   <select value="<?php echo $rekening_yayasan['nama_ket']?>" name="namaket" id="nk">
                        <option><?php echo $rekening_yayasan['nama_ket']?></option>
                       
                    </select>
                </div> 
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Nominal
                </label>
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $rekening_yayasan['nominal']?>" name="nominal" placeholder="Nominal" id="form-field-1" class="form-control" required = ''>
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Keterangan
                </label>
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $rekening_yayasan['keterangan']?>" name="keterangan" placeholder="Keterangan" id="keteranganx" class="form-control" required = ''>
                </div>  
            </div>


           
           
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">

                </label>
                <div class="col-sm-1">
                    <button type="submit" name="submit" class="btn btn-danger btn-sm">SIMPAN</button>
                </div>
                <div class="col-sm-1">
                    <?php echo anchor('rekening_yayasan', 'Kembali', array('class' => 'btn btn-info btn-sm')); ?>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- end: TEXT FIELDS PANEL -->
</div>

<script>
    function showDatarekening(a){
        //alert(a.value);
        $.ajax({
            type: "POST",
            url: "getdatarekening/"+a.value ,
            data: $("#tbl_rekening").serialize(),
            success: function(datanya){
                var data = JSON.parse(datanya); 
                $("#nry").val(data.norek);
                $("#by").val(data.nama_bank);
                $("#npry").val(data.nama_pemilik_rek);
            },
            error:function(){
       //toastr["error"]("Something wrong, Please inform administrator.", "Warning alert!");
            }
        });
    }

    function showJP(a){
        //alert(a.value);
        $("#jenispembayaran").show();
        $.ajax({
            type: "POST",
            url: "getJP/"+a.value ,
            data: $("#tbl_jenis_pembayaran").serialize(),
            success: function(datanya){
                var data = JSON.parse(datanya); 
                var htm = "";

                    $.each(data, function(i){
                         htm += "<option>"+data[i].nama_jenis_pembayaran+"</option>";
                         //$('<option></option>', {text:value}).attr('value', value).appendTo('#ns');
                    })
                    
                    $('#jenispembayaran').html(htm);
            },
            error:function(){
            //toastr["error"]("Something wrong, Please inform administrator.", "Warning alert!");
            }
        });
    } 

    function showKeterkaitan(a){
        var ktan = a.value;
        if(ktan=="siswa"){
            $("#kelas").show();
             $("#nomor_induk").show();
        }else if(ktan=="guru/karyawan"){
             $("#nomor_induk").hide();
            $("#kelas").hide();
            $.ajax({
            type: "POST",
            url: "getguru/"+a.value ,
            data: $("#tbl_guru").serialize(),
            success: function(datanya){
                var data = JSON.parse(datanya); 
                var htm = "";

                    $.each(data, function(i){
                         htm += "<option>"+data[i].nama_guru+"</option>";
                         //$('<option></option>', {text:value}).attr('value', value).appendTo('#ns');
                    })
                    
                    $('#nk').html(htm);
            },
            error:function(){
            //toastr["error"]("Something wrong, Please inform administrator.", "Warning alert!");
            }
        });
        }else if(ktan=="lain-lain"){
            $("#kelas").hide();
            $("#nomor_induk").hide();
            document.getElementById('nk').value="";
            document.getElementById('ni').value="";
            document.getElementById('kls').value="";
        }
    }

    function showNIMsiswa(a){
        $("#nomor_induk").show();
        $.ajax({
            type: "POST",
            url: "getkelas/"+a.value ,
            data: $("#tbl_siswa").serialize(),
            success: function(datanya){
                var data = JSON.parse(datanya); 
                var htm = "";

                    $.each(data, function(i){
                         htm += "<option>"+data[i].nim+"</option>";
                         //$('<option></option>', {text:value}).attr('value', value).appendTo('#ns');
                         console.log(data[i].nim);
                    })
                    $('#ni').html(htm);
            },
            error:function(){
            //toastr["error"]("Something wrong, Please inform administrator.", "Warning alert!");
            }
        });
    }

    function showNamasiswa(a){
        //alert(a.value);
        $.ajax({
            type: "POST",
            url: "getNIM/"+a.value ,
            data: $("#tbl_siswa").serialize(),
            success: function(datanya){
                var data = JSON.parse(datanya); 
                var htm = "";

                    $.each(data, function(i){
                         htm += "<option>"+data[i].nama+"</option>";
                         //$('<option></option>', {text:value}).attr('value', value).appendTo('#ns');
                         console.log(data[i].nama);
                    })
                    $('#nk').html(htm);
            },
            error:function(){
            //toastr["error"]("Something wrong, Please inform administrator.", "Warning alert!");
            }
        });
    }
</script>