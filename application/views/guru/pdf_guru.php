<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->
   
    <div class="panel panel-default">
         <h1 align="center"> <b>DATA GURU</b></h1>
         <br>
         <hr>
         <br>
        <div class="panel-body">
            <table id="mytable" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                    <tr>
                    	<td><b>NIP</b></td>
                    	<td>:<?php echo $data_guru[0]['nip'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>NAMA</b></td>
                    	<td>:<?php echo $data_guru[0]['nama_guru'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>KELAS</b></td>
                    	<td>:<?php echo $data_guru[0]['kelas'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>JENIS KELAMIN</b></td>
                    	<td>:<?php 
                    		if ($data_guru[0]['kelamin']=="L") {
                    			echo "Laki-Laki";
                    		}elseif ($data_guru[0]['kelamin']=="P"){
                    			echo "Perempuan";
                    		}else{
                    			echo "404 error not found";
                    		}
                 
                    		?></td>
                    </tr>
                    <tr>
                    	<td><b>TEMPAT LAHIR</b></td>
                    	<td>:<?php echo $data_guru[0]['tempat_lahir'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>TANGGAL LAHIR</b></td>
                    	<td>:<?php echo $data_guru[0]['tgl_lahir'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>ALAMAT</b></td>
                    	<td>:<?php echo $data_guru[0]['alamat'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>NO TELEPON</b></td>
                    	<td>:<?php echo $data_guru[0]['no_telp'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>NO KK</b></td>
                    	<td>:<?php echo $data_guru[0]['no_kk'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>NO KTP</b></td>
                    	<td>:<?php echo $data_guru[0]['no_ktp'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>NISN</b></td>
                    	<td>:<?php echo $data_guru[0]['nisn'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>NIK</b></td>
                    	<td>:<?php echo $data_guru[0]['nik'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>RIWAYAT PENYAKIT</b></td>
                    	<td>:<?php echo $data_guru[0]['riwayat_penyakit'];  ?></td>
                    </tr>




            </table>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>