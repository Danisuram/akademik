<div class="col-sm-12">
    <!-- start: TEXT FIELDS PANEL -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
            Text Fields
            <div class="panel-tools">
                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                </a>
                <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                    <i class="fa fa-wrench"></i>
                </a>
                <a class="btn btn-xs btn-link panel-refresh" href="#">
                    <i class="fa fa-refresh"></i>
                </a>
                <a class="btn btn-xs btn-link panel-expand" href="#">
                    <i class="fa fa-resize-full"></i>
                </a>
                <a class="btn btn-xs btn-link panel-close" href="#">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">

            <?php
            echo form_open_multipart('guru/edit', 'role="form" class="form-horizontal"');
            echo form_hidden('nip', $guru['nip']);
            ?>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    No Urut
                </label>
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $guru['no_urut'] ?>" readonly="" placeholder="MASUKAN NIM" id="form-field-1" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    NIP
                </label>
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $guru['nip'] ?>" readonly="" placeholder="MASUKAN NIM" id="form-field-1" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Nama
                </label>
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $guru['nama_guru'] ?>" name="nama_guru" placeholder="MASUKAN NAMA LENGKAP" id="form-field-1" class="form-control" required = ''>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Kelas
                </label>
                <div class="col-sm-6">
                   <?php echo cmb_dinamis('kelas', 'tbl_ruangan', 'nama_ruangan', 'kd_ruangan',$guru['kelas']);?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Mata Pelajaran
                </label>
             <div class="col-sm-6">
                <select name="matpel" value="<?php echo $guru['mata_pelajaran']?>">

                       <option selected="selected" value="<?php echo $guru['mata_pelajaran']?>"><?php echo $guru['mata_pelajaran']?></option>
                        <?php foreach($mapel as $x):?>
                            <option value="<?=$x['nama_mapel'];?>"><?php echo $x['nama_mapel']; ?></option>
                         <?php endforeach;?>
                        
                         
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Kelamin
                </label>
                <div class="col-sm-2">
                    <?php
                    echo form_dropdown('kelamin', array('L' => 'LAKI LAKI', 'P' => 'PEREMPUAN'), $guru['kelamin'], "class='form-control'");
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Tempat Lahir
                </label>
                <div class="col-sm-5">
                    <input type="text" value="<?php echo $guru['tempat_lahir'] ?>"  name="tempat_lahir" placeholder="Tempat Lahir" id="form-field-1" class="form-control" required = ''>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Tanggal Lahir
                </label>
                
                <div class="col-sm-2">
                    <input type="date" value="<?php echo $guru['tgl_lahir'] ?>"  name="tanggal_lahir" placeholder="Tanggal Lahir" id="form-field-1" class="form-control" required = ''>
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Alamat
                </label>
                
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $guru['alamat'] ?>"  name="alamat" placeholder="Alamat" id="form-field-1" class="form-control" required = ''>
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    No Telepon
                </label>
                
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $guru['no_telp'] ?>"  name="no_telp" placeholder="Sekolah Asal" id="form-field-1" class="form-control" required = ''>
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    No KK
                </label>
                
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $guru['no_kk'] ?>"  name="no_kk" placeholder="Nama Ibu" id="form-field-1" class="form-control">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    File KK
                </label>
                
                <div class="col-sm-2">
                    <input type="hidden" value="<?php echo $guru['file_kk'] ?>" name="file_kk_lama" placeholder="No KK" id="form-field-1" class="form-control">  
                    <input type="file" name="file_kk">
                    <img src="<?php echo base_url()."/uploads/kk/".$guru['file_kk']?>" width="200">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    No KTP
                </label>
                
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $guru['no_ktp'] ?>"  name="no_ktp" placeholder="No KTP" id="form-field-1" class="form-control">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    File KTP
                </label>
                
                <div class="col-sm-2">
                    <input type="hidden" value="<?php echo $guru['file_ktp'] ?>" name="file_ktp_lama" placeholder="No KK" id="form-field-1" class="form-control">  
                    <input type="file" name="file_ktp">
                    <img src="<?php echo base_url()."/uploads/ktp/".$guru['file_ktp']?>" width="200">
                </div> 
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Ijazah SD
                </label>
                
                <div class="col-sm-2">
                    <input type="hidden" value="<?php echo $guru['ijazah_sd'] ?>" name="file_ijazah_sd_lama" placeholder="Ijazah SD" id="form-field-1" class="form-control">  
                    <input type="file" name="ijazah_sd">
                    <img src="<?php echo base_url()."/uploads/sd/".$guru['ijazah_sd']?>" width="200">
                </div> 
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Ijazah SMP
                </label>
                
                <div class="col-sm-2">
                    <input type="hidden" value="<?php echo $guru['ijazah_smp'] ?>" name="file_ijazah_smp_lama" placeholder="Ijazah SMP" id="form-field-1" class="form-control">  
                    <input type="file" name="ijazah_smp">
                    <img src="<?php echo base_url()."/uploads/smp/".$guru['ijazah_smp']?>" width="200">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Ijazah SMA
                </label>
                
                <div class="col-sm-2">
                    <input type="hidden" value="<?php echo $guru['ijazah_sma'] ?>" name="file_ijazah_sma_lama" placeholder="Ijazah SMA" id="form-field-1" class="form-control">  
                    <input type="file" name="Ijazah SMA">
                    <img src="<?php echo base_url()."/uploads/sma/".$guru['ijazah_sma']?>" width="200">
                </div> 
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Ijazah S1
                </label>
                
                <div class="col-sm-2">
                    <input type="hidden" value="<?php echo $guru['ijazah_s1'] ?>" name="file_ijazah_s1_lama" placeholder="Ijazah S1" id="form-field-1" class="form-control">  
                    <input type="file" name="Ijazah S1">
                    <img src="<?php echo base_url()."/uploads/s1/".$guru['ijazah_s1']?>" width="200">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Ijazah S2
                </label>
                
                <div class="col-sm-2">
                    <input type="hidden" value="<?php echo $guru['ijazah_s2'] ?>" name="file_ijazah_s2_lama" placeholder="Ijazah S2" id="form-field-1" class="form-control">  
                    <input type="file" name="ijazah_s2">
                    <img src="<?php echo base_url()."/uploads/s2/".$guru['ijazah_s2']?>" width="200">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Ijazah S3
                </label>
                
                <div class="col-sm-2">
                    <input type="hidden" value="<?php echo $guru['ijazah_s3'] ?>" name="file_ijazah_s3_lama" placeholder="No KK" id="form-field-1" class="form-control">
                    <input type="file" name="ijazah_s3">
                    <img src="<?php echo base_url()."/uploads/s3/".$guru['ijazah_s3']?>" width="200">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    NISN
                </label>
                
                <div class="col-sm-2">
                    <input type="text" value="<?php echo $guru['nisn']?>" name="nisn" placeholder="NISN" id="form-field-1" class="form-control">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    NIK
                </label>
                
                <div class="col-sm-2">
                    <input type="text" value="<?php echo $guru['nik']?>" name="nik" placeholder="NIK" id="form-field-1" class="form-control">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Riwayat Penyakit
                </label>
                
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $guru['riwayat_penyakit'] ?>" name="riwayat_penyakit" placeholder="Riawayat Penyakit" id="form-field-1" class="form-control">
                </div>  
            </div>
            
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">

                </label>
                <div class="col-sm-1">
                    <button type="submit" name="submit" class="btn btn-danger btn-sm">SIMPAN</button>
                </div>
                <div class="col-sm-1">
                    <?php echo anchor('guru', 'Kembali', array('class' => 'btn btn-info btn-sm')); ?>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- end: TEXT FIELDS PANEL -->
</div>