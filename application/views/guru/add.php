<div class="col-sm-12">
    <!-- start: TEXT FIELDS PANEL -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
            Text Fields
            <div class="panel-tools">
                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                </a>
                <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                    <i class="fa fa-wrench"></i>
                </a>
                <a class="btn btn-xs btn-link panel-refresh" href="#">
                    <i class="fa fa-refresh"></i>
                </a>
                <a class="btn btn-xs btn-link panel-expand" href="#">
                    <i class="fa fa-resize-full"></i>
                </a>
                <a class="btn btn-xs btn-link panel-close" href="#">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">

            <?php
            echo form_open_multipart('guru/add', 'role="form" class="form-horizontal"');
            ?>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    No Urut
                </label>
                <div class="col-sm-9">
                    <input type="text" name="no_urut" placeholder="No Urut" id="form-field-1" class="form-control" required = ''>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    NIP
                </label>
                <div class="col-sm-9">
                    <input type="text" name="nip" placeholder="Masukan NIP" id="form-field-1" class="form-control" required = ''>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Nama
                </label>
                <div class="col-sm-9">
                    <input type="text" name="nama_guru" placeholder="Nama Lengkap" id="form-field-1" class="form-control" required = ''>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Kelas
                </label>
                <div class="col-sm-6">
                   <?php echo cmb_dinamis('kelas', 'tbl_ruangan', 'nama_ruangan', 'kd_ruangan')?>
                </div>
            </div>
            <div class="form-group"  id="namasiswa">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Mata Pelajaran
                </label>  
                <div class="col-sm-6">
                   <select name="matpel" id="matpel">
                        <option selected="selected">--PILIH MATA PELAJARAN--</option>
                            <?php foreach($mapel as $x):?>
                            <option value="<?=$x['nama_mapel'];?>"><?php echo $x['nama_mapel']; ?></option>
                         <?php endforeach;?>
                    </select>
                </div> 
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Kelamin
                </label>
                <div class="col-sm-2">
                    <?php
                    echo form_dropdown('kelamin', array('L' => 'LAKI LAKI', 'P' => 'PEREMPUAN'), null, "class='form-control'");
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Tempat Lahir
                </label>
                <div class="col-sm-5">
                    <input type="text" name="tempat_lahir" placeholder="Tempat Lahir" id="form-field-1" class="form-control" required = ''>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Tanggal Lahir
                </label>
                
                <div class="col-sm-2">
                    <input type="date" name="tanggal_lahir" placeholder="Tanggal Lahir" id="form-field-1" class="form-control" required = ''>
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Alamat
                </label>
                
                <div class="col-sm-9">
                    <input type="text" name="alamat" placeholder="Alamat" id="form-field-1" class="form-control" required = ''>
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    No. Telepon
                </label>
                
                <div class="col-sm-9">
                    <input type="text" name="no_telp" placeholder="Nomor Telepon" id="form-field-1" class="form-control" required = ''>
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    No. KK
                </label>
                
                <div class="col-sm-9">
                    <input type="text" name="no_kk" placeholder="Nomor KK" id="form-field-1" class="form-control">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    File KK
                </label>
                
                <div class="col-sm-2">
                    <input type="file" name="file_kk">
                </div>   
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    No. KTP
                </label>
                
                <div class="col-sm-9">
                    <input type="text" name="no_ktp" placeholder="Nomor KTP" id="form-field-1" class="form-control">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    File KTP
                </label>
                
                <div class="col-sm-2">
                    <input type="file" name="file_ktp">
                </div>    
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Ijazah SD
                </label>
                
                <div class="col-sm-2">
                    <input type="file" name="ijazah_sd">
                </div>    
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Ijazah SMP
                </label>
                
                <div class="col-sm-2">
                    <input type="file" name="ijazah_smp">
                </div>    
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Ijazah SMA
                </label>
                
                <div class="col-sm-2">
                    <input type="file" name="ijazah_sma">
                </div>   
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Ijazah S1
                </label>
                
                <div class="col-sm-2">
                    <input type="file" name="ijazah_s1">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Ijazah S2
                </label>
                
                <div class="col-sm-2">
                    <input type="file" name="ijazah_s2">
                </div>    
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Ijazah S3
                </label>
                
                <div class="col-sm-2">
                    <input type="file" name="ijazah_s3">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    NISN
                </label>
                
                <div class="col-sm-2">
                    <input type="text" name="nisn" placeholder="NISN" id="form-field-1" class="form-control">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    NIK
                </label>
                
                <div class="col-sm-2">
                    <input type="text" name="nik" placeholder="NIK" id="form-field-1" class="form-control">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Riwayat Penyakit
                </label>
                <div class="col-sm-9">
                    <input type="text" name="riwayat_penyakit" placeholder="Riwayat Penyakit" id="form-field-1" class="form-control">
                </div>    
            </div>
            

            
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">

                </label>
                <div class="col-sm-1">
                    <button type="submit" name="submit" class="btn btn-danger  btn-sm">SIMPAN</button>
                </div>
                <div class="col-sm-1">
                    <?php echo anchor('guru', 'Kembali', array('class' => 'btn btn-info btn-sm')); ?>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- end: TEXT FIELDS PANEL -->
</div>

<script>
    function getPelajaran(a){
        alert(a.value);
    }
</script>