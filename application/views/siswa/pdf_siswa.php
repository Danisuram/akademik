<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->
   
    <div class="panel panel-default">
         <h1 align="center"> <b>DATA SISWA</b></h1>
         <br>
         <hr>
         <br>
        <div class="panel-body">
            <table id="mytable" class="table table-striped table-bordered table-hover table-full-width dataTable" cellspacing="0" width="100%">
                    <tr>
                    	<td><b>NIM</b></td>
                    	<td>:<?php echo $data_siswa[0]['nim'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>NAMA</b></td>
                    	<td>:<?php echo $data_siswa[0]['nama'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>KELAS</b></td>
                    	<td>:<?php echo $data_siswa[0]['kelas'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>JENIS KELAMIN</b></td>
                    	<td>:<?php 
                    		if ($data_siswa[0]['gender']=="L") {
                    			echo "Laki-Laki";
                    		}elseif ($data_siswa[0]['gender']=="P"){
                    			echo "Perempuan";
                    		}else{
                    			echo "404 error not found";
                    		}
                 
                    		?></td>
                    </tr>
                    <tr>
                    	<td><b>TEMPAT LAHIR</b></td>
                    	<td>:<?php echo $data_siswa[0]['tempat_lahir'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>TANGGAL LAHIR</b></td>
                    	<td>:<?php echo $data_siswa[0]['tanggal_lahir'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>ALAMAT</b></td>
                    	<td>:<?php echo $data_siswa[0]['alamat'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>SEKOLAH ASAL</b></td>
                    	<td>:<?php echo $data_siswa[0]['sekolah_asal'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>NAMA IBU</b></td>
                    	<td>:<?php echo $data_siswa[0]['nm_ibu'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>NAMA BAPAK</b></td>
                    	<td>:<?php echo $data_siswa[0]['nm_bapak'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>PEKERJAAN IBU</b></td>
                    	<td>:<?php echo $data_siswa[0]['pkjr_ibu'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>PEKERJAAN BAPAK</b></td>
                    	<td>:<?php echo $data_siswa[0]['pkjr_bapak'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>NAMA WALI</b></td>
                    	<td>:<?php echo $data_siswa[0]['nm_wali'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>PEKERJAAN WALI</b></td>
                    	<td>:<?php echo $data_siswa[0]['pkjr_wali'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>ALAMAT WALI</b></td>
                    	<td>:<?php echo $data_siswa[0]['alamat_wali'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>NO TELEPON</b></td>
                    	<td>:<?php echo $data_siswa[0]['no_telp'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>NO KK</b></td>
                    	<td>:<?php echo $data_siswa[0]['no_kk'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>IJAZAH</b></td>
                    	<td>:<?php echo $data_siswa[0]['ijazah'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>NISN</b></td>
                    	<td>:<?php echo $data_siswa[0]['nisn'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>NO KK</b></td>
                    	<td>:<?php echo $data_siswa[0]['nik'];  ?></td>
                    </tr>
                    <tr>
                    	<td><b>RIWAYAT PENYAKIT</b></td>
                    	<td>:<?php echo $data_siswa[0]['riwayat_penyakit'];  ?></td>
                    </tr>




            </table>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>