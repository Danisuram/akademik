<div class="col-sm-12">
    <!-- start: TEXT FIELDS PANEL -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
            Text Fields
            <div class="panel-tools">
                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                </a>
                <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                    <i class="fa fa-wrench"></i>
                </a>
                <a class="btn btn-xs btn-link panel-refresh" href="#">
                    <i class="fa fa-refresh"></i>
                </a>
                <a class="btn btn-xs btn-link panel-expand" href="#">
                    <i class="fa fa-resize-full"></i>
                </a>
                <a class="btn btn-xs btn-link panel-close" href="#">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">

            <?php
            echo form_open_multipart('siswa/edit', 'role="form" class="form-horizontal"');
            echo form_hidden('nim', $siswa['nim']);
            ?>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    No Urut
                </label>
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $siswa['no_urut'] ?>" readonly="" placeholder="MASUKAN NIM" id="form-field-1" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    No Induk
                </label>
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $siswa['nim'] ?>" readonly="" placeholder="MASUKAN NIM" id="form-field-1" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Nama
                </label>
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $siswa['nama'] ?>" name="nama" placeholder="MASUKAN NAMA LENGKAP" id="form-field-1" class="form-control" required = ''>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Kelas
                </label>
                <div class="col-sm-6">
                   <?php echo cmb_dinamis('kelas', 'tbl_ruangan', 'nama_ruangan', 'kd_ruangan',$siswa['kelas']);?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Kelamin
                </label>
                <div class="col-sm-2">
                    <?php
                    echo form_dropdown('gender', array('L' => 'LAKI LAKI', 'P' => 'PEREMPUAN'), $siswa['gender'], "class='form-control'");
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Tempat Lahir
                </label>
                <div class="col-sm-5">
                    <input type="text" value="<?php echo $siswa['tempat_lahir'] ?>"  name="tempat_lahir" placeholder="Tempat Lahir" id="form-field-1" class="form-control" required = ''>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Tanggal Lahir
                </label>
                
                <div class="col-sm-2">
                    <input type="date" value="<?php echo $siswa['tanggal_lahir'] ?>"  name="tanggal_lahir" placeholder="Tanggal Lahir" id="form-field-1" class="form-control" required = ''>
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Alamat
                </label>
                
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $siswa['alamat'] ?>"  name="alamat" placeholder="Alamat" id="form-field-1" class="form-control" required = ''>
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Sekolah Asal
                </label>
                
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $siswa['sekolah_asal'] ?>"  name="sekolah_asal" placeholder="Sekolah Asal" id="form-field-1" class="form-control" required = ''>
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Nama Ibu
                </label>
                
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $siswa['nm_ibu'] ?>"  name="nama_ibu" placeholder="Nama Ibu" id="form-field-1" class="form-control">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Nama Bapak
                </label>
                
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $siswa['nm_bapak'] ?>"  name="nama_bapak" placeholder="Nama Bapak" id="form-field-1" class="form-control">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Pekerjaan Ibu
                </label>
                
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $siswa['pkjr_ibu'] ?>"  name="pekerjaan_ibu" placeholder="Pekerjaan Ibu" id="form-field-1" class="form-control">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Pekerjaan Bapak
                </label>
                
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $siswa['pkjr_bapak'] ?>" name="pekerjaan_bapak" placeholder="Pekerjaan Bapak" id="form-field-1" class="form-control">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Nama Wali
                </label>
                
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $siswa['nm_wali'] ?>"  name="nama_wali" placeholder="Nama Wali" id="form-field-1" class="form-control">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Pekerjaan Wali
                </label>
                
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $siswa['pkjr_wali'] ?>"  name="pekerjaan_wali" placeholder="Pekerjaan Wali" id="form-field-1" class="form-control">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Alamat Wali
                </label>
                
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $siswa['alamat_wali'] ?>"  name="alamat_wali" placeholder="Alamat Wali" id="form-field-1" class="form-control">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    No Telp
                </label>
                
                <div class="col-sm-2">
                    <input type="text" value="<?php echo $siswa['no_telp'] ?>"  name="no_telp" placeholder="No Telp" id="form-field-1" class="form-control">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    No KK
                </label>
                
                <div class="col-sm-2">
                    <input type="text" value="<?php echo $siswa['no_kk'] ?>" name="no_kk" placeholder="No KK" id="form-field-1" class="form-control">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    File KK
                </label>
                
                <div class="col-sm-2">
                    <input type="hidden" value="<?php echo $siswa['file_kk'] ?>" name="file_kk_lama" placeholder="No KK" id="form-field-1" class="form-control">
                    <input type="file" name="file_kk">
                    <img src="<?php echo base_url()."/uploads/kk/".$siswa['file_kk']?>" width="200">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                   No Ijazah
                </label>
                
                <div class="col-sm-2">
                    <input type="text" value="<?php echo $siswa['ijazah'] ?>"  name="ijazah" placeholder="Ijazah" id="form-field-1" class="form-control">
                </div>  
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    File Ijazah
                </label>
                
                <div class="col-sm-2">
                    <input type="hidden" value="<?php echo $siswa['file_ijazah'] ?>" name="file_ijazah_lama" placeholder="No KK" id="form-field-1" class="form-control">  
                    <input type="file" name="file_ijazah">
                    <img src="<?php echo base_url()."/uploads/ijz_siswa/".$siswa['file_ijazah']?>" width="200">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    NISN
                </label>
                
                <div class="col-sm-2">
                    <input type="text" value="<?php echo $siswa['nisn'] ?>" name="nisn" placeholder="NISN" id="form-field-1" class="form-control">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    NIK
                </label>
                
                <div class="col-sm-2">
                    <input type="text" value="<?php echo $siswa['nik'] ?>" name="nik" placeholder="NIK" id="form-field-1" class="form-control">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    Riwayat Penyakit
                </label>
                
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $siswa['riwayat_penyakit'] ?>" name="riwayat_penyakit" placeholder="Riawayat Penyakit" id="form-field-1" class="form-control">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    ABK
                </label>
                
                <div class="col-sm-2">
                    <input type="text" value="<?php echo $siswa['abk'] ?>" name="abk" placeholder="ABK" id="form-field-1" class="form-control">
                </div>  
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">
                    No SKHU
                </label>
                
                <div class="col-sm-2">
                    <input type="text" value="<?php echo $siswa['no_skhu'] ?>"  name="NO SKHU" placeholder="no_skhu" id="form-field-1" class="form-control">
                </div>  
            </div>  
            
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1">

                </label>
                <div class="col-sm-1">
                    <button type="submit" name="submit" class="btn btn-danger btn-sm">SIMPAN</button>
                </div>
                <div class="col-sm-1">
                    <?php echo anchor('siswa', 'Kembali', array('class' => 'btn btn-info btn-sm')); ?>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- end: TEXT FIELDS PANEL -->
</div>