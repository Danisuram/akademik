<?php

class Pengajaran extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Model_pengajaran');
		$this->load->library('ssp');
	}

	function index()
	{	
		$this->template->load('template', 'pengajaran/list');
	}

	function data(){
        // nama tabel
        $table = 'tbl_pengajaran';
        // nama PK
        $primaryKey = 'nip_kdruangan_kdmapel';
        // list field        
        $columns = array(            
            array('db' => 'nama_guru', 'dt' => 'nama_guru'),           
            array('db' => 'nama_ruangan', 'dt' => 'nama_ruangan'),
            array('db' => 'nama_mapel', 'dt' => 'nama_mapel'),
            array(
                'db' => 'nip_kdruangan_kdmapel',
                'dt' => 'aksi',
                'formatter' => function( $d) {
                    //return "<a href='edit.php?id=$d'>EDIT</a>";
                    return anchor('pengajaran/delete/'.$d,'<i class="fa fa-trash"></i>','onclick="return konfirmasi()" class="btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Delete"');
                }
            )
        );

        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );

        
           echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
          );
	}

	function add()
	{	
		if(isset($_POST['submit'])){
			$this->Model_pengajaran->add();
			redirect('pengajaran');
		}else{
			$data2['nip'] 	= $this->Model_pengajaran->getNip();
			$data2['kelas'] = $this->Model_pengajaran->getKelas();
			$data2['mapel'] = $this->Model_pengajaran->getMapel();
			$this->template->load('template','pengajaran/add', $data2);	
		}
		
	}

	function delete(){
        $id = $this->uri->segment(3);
       
        if(!empty($id)){
            // proses delete data
            $this->db->where('nip_kdruangan_kdmapel',$id);
            $this->db->delete('tbl_pengajaran');
        }
        redirect('pengajaran');
    }

	function getNamaguru($nip){
		$data = $this->Model_pengajaran->getNamaguru($nip);
		echo json_encode($data[0]);
	}

	function getRuangkelas($kd_kelas){
		$data = $this->Model_pengajaran->getRuangkelas($kd_kelas);
		echo json_encode($data[0]);
	}

	function getMatapelajaran($kd_mapel){
		$data = $this->Model_pengajaran->getMatapelajaran($kd_mapel);
		echo json_encode($data[0]);
	}

	function export_pengajaran(){
		$this->load->library('excel');
      //activate worksheet number 1
      $this->excel->setActiveSheetIndex(0);
      //name the worksheet
      $this->excel->getActiveSheet()->setTitle('test worksheet');
      //set cell A1 content with some text
      $this->excel->getActiveSheet()->setCellValue('A1', 'nip');
      $this->excel->getActiveSheet()->setCellValue('B1', 'nama_guru');
        $this->excel->getActiveSheet()->setCellValue('C1', 'kd_ruangan');
        $this->excel->getActiveSheet()->setCellValue('D1', 'nama_ruangan');
        $this->excel->getActiveSheet()->setCellValue('E1', 'kd_mapel');
        $this->excel->getActiveSheet()->setCellValue('F1', 'nama_mapel');
        

      $data = $this->Model_pengajaran->get_export_pengajaran();

      $exceldata=array();
        foreach ($data as $row){
                $exceldata[] = $row;
        }

      $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A2');

      $filename='Pengajaran.xls'; //save our workbook as this file name
      header('Content-Type: application/vnd.ms-excel'); //mime type
      header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
      header('Cache-Control: max-age=0'); //no cache

      //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
      //if you want to save it as .XLSX Excel 2007 format
      $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
      //force user to download the Excel file without writing it to server's HD
      $objWriter->save('php://output');
    
	}

}

/* End of file Pengajaran.php */
/* Location: ./application/controllers/Pengajaran.php */