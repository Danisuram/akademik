<?php 
class Riwayat_keuangan extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('ssp');
		$this->load->library('session');
	}

	function data(){
		$datauser = $this->session->userdata('username');
		// print_r($datauser);
		// exit;

        // nama tabel
        $table = 'tbl_rekening_yayasan';
        // nama PK
        $primaryKey = 'id_transaksiRekening';
        // list field

        
        $columns = array(
            array('db' => 'tgl_transaksi', 'dt' => 'tgl_transaksi'),
            array('db' => 'norek_yayasan', 'dt' => 'norek_yayasan'),
            array('db' => 'bank_yayasan', 'dt' => 'bank_yayasan'),
            array('db' => 'nama_pemilik_rek_yayasan', 'dt' => 'nama_pemilik_rek_yayasan'),
            array('db' => 'nama', 'dt' => 'nama'),
            array('db' => 'norek', 'dt' => 'norek'),
            array('db' => 'bank', 'dt' => 'bank'),
            array('db' => 'jenis_pembayaran', 'dt' => 'jenis_pembayaran'),
            array('db' => 'nama_ket', 'dt' => 'nama_ket'),
            array('db' => 'nominal', 'dt' => 'nominal',
            		'formatter'=> function($d){
                    return number_format($d, 2);
                   }
        		 )
        );

        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );

     	
        $whereAll = "nomor_induk = ".$datauser."";	
        
        echo json_encode(
                SSP::complex($_GET, $sql_details, $table, $primaryKey, $columns, null, $whereAll)

         );
         
         
	}
	
	function index()
	{

		$this->template->load('template','riwayat_keuangan/list.php');	
	}


}

/* End of file Riwayat_keuangan.php */
/* Location: ./application/controllers/Riwayat_keuangan.php */