<?php

class Transaksi_hari_ini extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('ssp');
	}

	function data(){
		$datauser = $this->session->userdata('username');
		$table = 'tbl_rekening_yayasan';
        // nama PK
        $primaryKey = 'id_transaksiRekening';
        // list field

        
        $columns = array(
            array('db' => 'tgl_transaksi', 'dt' => 'tgl_transaksi'),
            array('db' => 'norek_yayasan', 'dt' => 'norek_yayasan'),
            array('db' => 'bank_yayasan', 'dt' => 'bank_yayasan'),
            array('db' => 'nama_pemilik_rek_yayasan', 'dt' => 'nama_pemilik_rek_yayasan'),
            array('db' => 'nama', 'dt' => 'nama'),
            array('db' => 'norek', 'dt' => 'norek'),
            array('db' => 'bank', 'dt' => 'bank'),
            array('db' => 'jenis_pembayaran', 'dt' => 'jenis_pembayaran'),
            array('db' => 'nama_ket', 'dt' => 'nama_ket'),
            array('db' => 'nominal', 'dt' => 'nominal',
            		'formatter'=> function($d){
                    return number_format($d, 2);
                   }
        		 )
        );

        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );

        $currentdate = date('Y-m-d');

     	
        $whereAll = "tgl_transaksi = ".$currentdate."";	
        
        echo json_encode(
                SSP::complex($_GET, $sql_details, $table, $primaryKey, $columns, null, $whereAll)

         );
	
	}

	function index()
	{
		$this->template->load('template','Transaksi_hari_ini/list.php');	
	}

}

/* End of file Transaksi_hari_ini.php */
/* Location: ./application/controllers/Transaksi_hari_ini.php */