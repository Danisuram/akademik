<?php

class Riwayat_keuangan_siswa extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('ssp');
	}

	function data(){
        // nama tabel
        $table = 'tbl_rekening_yayasan';
        // nama PK
        $primaryKey = 'id_transaksiRekening';
        // list field

        
        $columns = array(
            array('db' => 'tgl_transaksi', 'dt' => 'tgl_transaksi'),
            array('db' => 'norek_yayasan', 'dt' => 'norek_yayasan'),
            array('db' => 'bank_yayasan', 'dt' => 'bank_yayasan'),
            array('db' => 'nama_pemilik_rek_yayasan', 'dt' => 'nama_pemilik_rek_yayasan'),
            array('db' => 'nama', 'dt' => 'nama'),
            array('db' => 'norek', 'dt' => 'norek'),
            array('db' => 'bank', 'dt' => 'bank'),
            array('db' => 'jenis_pembayaran', 'dt' => 'jenis_pembayaran'),
            array('db' => 'nama_ket', 'dt' => 'nama_ket'),
            array('db' => 'nominal', 'dt' => 'nominal',
            		'formatter'=> function($d){
                    return number_format($d, 2);
                   }
        		 )
        );

        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );

     	
        $whereAll = "keterkaitan = 'siswa'";	
        
        echo json_encode(
                SSP::complex($_GET, $sql_details, $table, $primaryKey, $columns, null, $whereAll)

         );
         
         
	}

	function index()
	{
		$this->template->load('template','riwayat_keuangan_siswa/list.php');	
	}

	function export_riwayatkeuangansiswa(){
		$this->load->library('excel');
      //activate worksheet number 1
      $this->excel->setActiveSheetIndex(0);
      //name the worksheet
      $this->excel->getActiveSheet()->setTitle('test worksheet');
      //set cell A1 content with some text
      $this->excel->getActiveSheet()->setCellValue('A1', 'id_transaksiRekening');
  		$this->excel->getActiveSheet()->setCellValue('B1', 'id_rek');
  		$this->excel->getActiveSheet()->setCellValue('C1', 'tgl_transaksi');
  		$this->excel->getActiveSheet()->setCellValue('D1', 'norek_yayasan');
  		$this->excel->getActiveSheet()->setCellValue('E1', 'bank_yayasan');
  		$this->excel->getActiveSheet()->setCellValue('F1', 'nama_pemilik_rek_yayasan');
  		$this->excel->getActiveSheet()->setCellValue('G1', 'nama');
  		$this->excel->getActiveSheet()->setCellValue('H1', 'norek');
  		$this->excel->getActiveSheet()->setCellValue('I1', 'bank');
  		$this->excel->getActiveSheet()->setCellValue('J1', 'cashflow');
  		$this->excel->getActiveSheet()->setCellValue('K1', 'jenis_pembayaran');
  		$this->excel->getActiveSheet()->setCellValue('L1', 'keterkaitan');
      $this->excel->getActiveSheet()->setCellValue('M1', 'nama_ket');
      $this->excel->getActiveSheet()->setCellValue('N1', 'nominal');
      $this->excel->getActiveSheet()->setCellValue('O1', 'keterangan');
      $this->excel->getActiveSheet()->setCellValue('P1', 'saldo');

      $data = $this->Model_riwayat_keuangan_siswa->get_export_riwayatkeuangansiswa();

      $exceldata=array();
  		foreach ($data as $row){
  				$exceldata[] = $row;
  		}

      $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A2');

      $filename='riwayat_keuangan_siswa.xls'; //save our workbook as this file name
      header('Content-Type: application/vnd.ms-excel'); //mime type
      header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
      header('Cache-Control: max-age=0'); //no cache

      //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
      //if you want to save it as .XLSX Excel 2007 format
      $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
      //force user to download the Excel file without writing it to server's HD
      $objWriter->save('php://output');
	}

}

/* End of file Riwayat_keuangan_siswa.php */
/* Location: ./application/controllers/Riwayat_keuangan_siswa.php */