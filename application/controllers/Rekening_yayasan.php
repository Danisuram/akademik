<?php

Class Rekening_yayasan extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('ssp');
        $this->load->model('Model_rekening_yayasan');
        $this->load->library('upload');
        $this->load->library('dompdf_gen');
        $this->load->library('session');

    }

    function data() {
        $datauser = $this->session->userdata('username');
        //print_r($datauser);


        // nama tabel
        $table = 'tbl_rekening_yayasan';
        // nama PK
        $primaryKey = 'id_transaksiRekening';
        // list field

        
        $columns = array(
            array('db' => 'tgl_transaksi', 'dt' => 'tgl_transaksi'),
            array('db' => 'norek_yayasan', 'dt' => 'norek_yayasan'),
            array('db' => 'bank_yayasan', 'dt' => 'bank_yayasan'),
            array('db' => 'nama_pemilik_rek_yayasan', 'dt' => 'nama_pemilik_rek_yayasan'),
            array('db' => 'nama', 'dt' => 'nama'),
            array('db' => 'norek', 'dt' => 'norek'),
            array('db' => 'bank', 'dt' => 'bank'),
            array('db' => 'jenis_pembayaran', 'dt' => 'jenis_pembayaran'),
            array('db' => 'nama_ket', 'dt' => 'nama_ket'),
            array('db' => 'nominal', 'dt' => 'nominal'),
            array('db' => 'saldo', 'dt' => 'saldo',
                   'formatter'=> function($d){
                    return number_format($d, 2);
                   }

                ),
            array(
                'db' => 'id_transaksiRekening',
                'dt' => 'aksi',
                'formatter' => function( $d) {
                    //return "<a href='edit.php?id=$d'>EDIT</a>";
                    return anchor('rekening_yayasan/edit/'.$d,'<i class="fa fa-edit"></i>','class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"').'
                        '.anchor('rekening_yayasan/delete/'.$d,'<i class="fa fa-trash"></i>','onclick="return konfirmasi()" class="btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Delete"');
                }
            ),
            array(
                'db' => 'id_transaksiRekening',
                'dt' => 'export',
                'formatter' => function( $d){
                    return anchor('rekening_yayasan/export_pertransaksi/'.$d,'<i>EXPORT</i>');
                }
            )
        );

        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );

        if($datauser=="keuanganpusat"){
           echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)

          );
         
        } else{
          
          if($datauser=="keuangansd"){
           $kategori = "1";
          
          }elseif ($datauser=="keuangansmp") {
            $kategori = "2";
            
          }elseif ($datauser=="keuangansma") {
            $kategori = "3";
           
          }elseif ($datauser=="keuangankantin") {
            $kategori="4";
          }else{
            echo"404 error not found hehe.";
            
          }
          $whereAll = "kategori_keuangan =".$kategori."";
          
          echo json_encode(
             SSP::complex($_GET, $sql_details, $table, $primaryKey, $columns, null, $whereAll)
           );
        }
    }

    function index() {
        $this->template->load('template', 'rekening_yayasan/list');
    }

    function getNIP($nip){
       $x = $this->Model_rekening_yayasan->getDataguru($nip);
       echo json_encode($x[0]);
    }

    function getNIM($nim){
       $x = $this->Model_rekening_yayasan->getDatasiswa($nim);
       echo json_encode($x);
    }

    function getDatarekening($norek){
        $datauser = $this->session->userdata('username');
            if($datauser=="keuangansd"){
              $kategori = "1";
          
            }elseif ($datauser=="keuangansmp") {
              $kategori = "2";
            
            }elseif ($datauser=="keuangansma") {
               $kategori = "3";
           
            }elseif ($datauser=="keuangankantin") {
               $kategori = "4";
            }elseif ($datauser=="keuanganpusat") {
                $kategori="5";
            }else{
                echo "404 not found";
            }
       $x = $this->Model_rekening_yayasan->getDatarek($norek, $kategori);
       echo json_encode($x[0]);
    }
    function getKelas($kelas){
       $x = $this->Model_rekening_yayasan->getNamasiswa($kelas);
       echo json_encode($x);
    }

    function getJP($cashflow){
        $datauser = $this->session->userdata('username');
            if($datauser=="keuangansd"){
              $kategori = "1";
          
            }elseif ($datauser=="keuangansmp") {
              $kategori = "2";
            
            }elseif ($datauser=="keuangansma") {
               $kategori = "3";
           
            }elseif ($datauser=="keuangankantin") {
               $kategori = "4";
            }elseif ($datauser=="keuanganpusat") {
                $kategori="5";
            }else{
                echo "404 not found";
            }

        $x = $this->Model_rekening_yayasan->getJP($cashflow, $kategori);
       echo json_encode($x);
    }

     function getGuru(){
        $x = $this->Model_rekening_yayasan->getGuru();
       echo json_encode($x);
    }



    function add() {
        if (isset($_POST['submit'])) {
            $data       = $this->input->post();
            $no_rek     = $this->input->post('norek_yayasan');
            $uangmasuk  = $this->input->post('nominal');
            $cashflow   = $this->input->post('cashflow');
            $datauser = $this->session->userdata('username');
            if($datauser=="keuangansd"){
              $kategori = "1";
          
            }elseif ($datauser=="keuangansmp") {
              $kategori = "2";
            
            }elseif ($datauser=="keuangansma") {
               $kategori = "3";
           
            }elseif ($datauser=="keuanganpusat") {
                $kategori="5";
            }else{
                echo "404 not found";
            }
            // print_r($kategori);
            // exit;

            if($cashflow=="outcome"){

                $saldoawal=$this->Model_rekening_yayasan->getSaldo($no_rek);
                //print_r($saldoawal[0]['saldo']." + ".$uangmasuk);

                $saldoakhir = $saldoawal[0]['saldo'] - $uangmasuk;
                // print_r($saldoawal[0]['saldo']." - ".$uangmasuk." = ".$saldoakhir);
                // exit;
                 $this->Model_rekening_yayasan->savemin($data,$saldoakhir,$kategori);
                redirect('rekening_yayasan');

            } else if($cashflow=="income"){

                $saldoawal=$this->Model_rekening_yayasan->getSaldo($no_rek);
                //print_r($data);
                //print_r($no_rek." & ".$saldoawal[0]['saldo']);
                //exit;
                $saldoakhir = $saldoawal[0]['saldo'] + $uangmasuk;
                //print_r($saldoawal[0]['saldo']." + ".$uangmasuk." = ".$saldoakhir);
                //exit;
                $this->Model_rekening_yayasan->saveplus($data,$saldoakhir,$kategori);
                redirect('rekening_yayasan');

            }

        } else {
            $datauser = $this->session->userdata('username');
            if($datauser=="keuangansd"){
              $kategori = "1";
          
            }elseif ($datauser=="keuangansmp") {
              $kategori = "2";
            
            }elseif ($datauser=="keuangansma") {
               $kategori = "3";
           
            }elseif ($datauser=="keuanganpusat") {
                $kategori="5";
            }else{
                echo "404 not found";
            }
            $data2['jenisPembayaran']    = $this->Model_rekening_yayasan->getJenisPembayaran($kategori);
            $data2['nip']                = $this->Model_rekening_yayasan->getNIPguru();
            $data2['nim']                = $this->Model_rekening_yayasan->getNIMsiswa();
            $data2['JPincome']           = $this->Model_rekening_yayasan->getJPincome();
            $data2['JPoutcome']          = $this->Model_rekening_yayasan->getJPoutcome();
            $data2['ruangKelas']         = $this->Model_rekening_yayasan->getKelas();
            $data2['namaGuru']           = $this->Model_rekening_yayasan->getNamaguru();
            
            $data2['norek']              = $this->Model_rekening_yayasan->getNorek($kategori);
            $this->template->load('template', 'rekening_yayasan/add',$data2);
        }
    }

    function edit(){
        if(isset($_POST['submit'])){
            $data = $this->input->post();
            $id_transaksiRekening = $this->uri->segment(3);
            $no_rek =$this->input->post('norek_yayasan');
            $uangmasuk  = $this->input->post('nominal');
            $cashflow   = $this->input->post('cashflow');
            // print_r($data);
            // exit;

            if($cashflow=="outcome"){
                $norek = $data['norek_yayasan'];
                $saldoLama = $this->Model_rekening_yayasan->getSaldolama($norek);
                $id_transaksiRekening = $data['id_transaksiRekening'];
                $nominalLama = $this->Model_rekening_yayasan->getNominallama($id_transaksiRekening);
                $undoSaldo = $saldoLama[0]['saldo'] + $nominalLama[0]['nominal'];
                $saldoakhir = $undoSaldo - $uangmasuk;
                $this->Model_rekening_yayasan->update($data, $saldoakhir);
                redirect('rekening_yayasan');

            } elseif ($cashflow=="income") {
            # code...
                $norek = $data['norek_yayasan'];
                $saldoLama = $this->Model_rekening_yayasan->getSaldolama($norek);
                $id_transaksiRekening = $data['id_transaksiRekening'];
                $nominalLama = $this->Model_rekening_yayasan->getNominallama($id_transaksiRekening);
                $undoSaldo = $saldoLama[0]['saldo'] - $nominalLama[0]['nominal'];
                $saldoakhir = $undoSaldo + $uangmasuk;
                 $this->Model_rekening_yayasan->update($data, $saldoakhir);
                  redirect('rekening_yayasan');
            }


        }else{
            $id_rekening_yayasan      = $this->uri->segment(3);
            $data['rekening_yayasan'] = $this->db->get_where('tbl_rekening_yayasan',array('id_transaksiRekening'=>$id_rekening_yayasan))->row_array();
            $datauser = $this->session->userdata('username');
            if($datauser=="keuangansd"){
              $kategori = "1";
          
            }elseif ($datauser=="keuangansmp") {
              $kategori = "2";
            
            }elseif ($datauser=="keuangansma") {
               $kategori = "3";
           
            }elseif ($datauser=="keuangankantin") {
               $kategori = "4";
            }else{
                $kategori=null;
            }

            $data['no_rek']              = $this->Model_rekening_yayasan->getNorek($kategori);
            $this->template->load('template', 'rekening_yayasan/edit',$data);
        }
    }

    function delete(){
        $id_transaksiRekening = $this->uri->segment(3);
        $data_transaksi = $this->Model_rekening_yayasan->getNorektrans($id_transaksiRekening);

        if ($data_transaksi[0]['cashflow']=="outcome") {
          # code...
          $saldo = $this->Model_rekening_yayasan->getSaldolama($data_transaksi[0]['norek_yayasan']);
        $undoSaldo = $saldo[0]['saldo'] + $data_transaksi[0]['nominal'];
        }elseif ($data_transaksi[0]['cashflow']=="income") {
          # code...
          $saldo = $this->Model_rekening_yayasan->getSaldolama($data_transaksi[0]['norek_yayasan']);
          $undoSaldo = $saldo[0]['saldo'] - $data_transaksi[0]['nominal'];
        }
        
        

        //$data = $this->Model_rekening_yayasan->getUang($id_rekening_yayasan);
        //$updatesaldo = $data[0]['saldo'] + $data[0]['nominal'];
        //print_r($saldo[0]['saldo']." + ".$data_transaksi[0]['nominal']." = ".$undoSaldo);
        //exit;


        if(!empty($id_transaksiRekening)){
            $this->Model_rekening_yayasan->undoSaldo($undoSaldo, $data_transaksi[0]['norek_yayasan']);
            $this->Model_rekening_yayasan->hapusTransaksi($id_transaksiRekening);
        }
        redirect('rekening_yayasan');
    }

    function export_pertransaksi($d){
     

      $x = $d;
      
        $data = $this->Model_rekening_yayasan->get_export_pertransaksi($x);
        $cashflow = $data[0]['cashflow'];
        // print_r($cashflow);
        // exit;
  
        // print_r($data2);
        // exit;
      
        if ($cashflow=="outcome") {
          $data2['data_transaksi'] = array($data[0]['id_transaksiRekening'], $data[0]['tgl_transaksi'], $data[0]['jenis_pembayaran'],$data[0]['norek_yayasan'],$data[0]['norek'],$data[0]['nama_pemilik_rek_yayasan'],$data[0]['nama'],$data[0]['bank_yayasan'],$data[0]['bank'],$data[0]['nominal'], $data[0]['keterangan'],$data[0]['keterkaitan']);
          // print_r($data2);
          // exit;
          

        } elseif ($cashflow=="income") {
            $data2['data_transaksi'] = array($data[0]['id_transaksiRekening'], $data[0]['tgl_transaksi'], $data[0]['jenis_pembayaran'],$data[0]['norek'],$data[0]['norek_yayasan'],$data[0]['nama'],$data[0]['nama_pemilik_rek_yayasan'],$data[0]['bank'],$data[0]['bank_yayasan'],$data[0]['nominal'], $data[0]['keterangan'],$data[0]['keterkaitan']);
            
          # code...
          //   print_r($data2);
          // exit;
        }else{
          echo"error 404 not found";
          exit;
        }

        $this->load->view('rekening_yayasan/pdf_transaksi', $data2);

        $html = $this->output->get_output();

        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("IDTRANSREK".$data[0]['id_transaksiRekening'].".pdf");
        
    }

    function export_rekyayasan(){

            //load our new PHPExcel library
      $this->load->library('excel');
      //activate worksheet number 1
      $this->excel->setActiveSheetIndex(0);
      //name the worksheet
      $this->excel->getActiveSheet()->setTitle('test worksheet');
      //set cell A1 content with some text
      $this->excel->getActiveSheet()->setCellValue('A1', 'id_transaksiRekening');
  		$this->excel->getActiveSheet()->setCellValue('B1', 'id_rek');
  		$this->excel->getActiveSheet()->setCellValue('C1', 'tgl_transaksi');
  		$this->excel->getActiveSheet()->setCellValue('D1', 'norek_yayasan');
  		$this->excel->getActiveSheet()->setCellValue('E1', 'bank_yayasan');
  		$this->excel->getActiveSheet()->setCellValue('F1', 'nama_pemilik_rek_yayasan');
  		$this->excel->getActiveSheet()->setCellValue('G1', 'nama');
  		$this->excel->getActiveSheet()->setCellValue('H1', 'norek');
  		$this->excel->getActiveSheet()->setCellValue('I1', 'bank');
  		$this->excel->getActiveSheet()->setCellValue('J1', 'cashflow');
  		$this->excel->getActiveSheet()->setCellValue('K1', 'jenis_pembayaran');
  		$this->excel->getActiveSheet()->setCellValue('L1', 'keterkaitan');
      $this->excel->getActiveSheet()->setCellValue('M1', 'nama_ket');
      $this->excel->getActiveSheet()->setCellValue('N1', 'nominal');
      $this->excel->getActiveSheet()->setCellValue('O1', 'keterangan');
      $this->excel->getActiveSheet()->setCellValue('P1', 'saldo');

      $data = $this->Model_rekening_yayasan->get_export_rekyayasan();

      $exceldata=array();
  		foreach ($data as $row){
  				$exceldata[] = $row;
  		}

      $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A2');

      $filename='rekening_yayasan.xls'; //save our workbook as this file name
      header('Content-Type: application/vnd.ms-excel'); //mime type
      header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
      header('Cache-Control: max-age=0'); //no cache

      //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
      //if you want to save it as .XLSX Excel 2007 format
      $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
      //force user to download the Excel file without writing it to server's HD
      $objWriter->save('php://output');
    }


}
