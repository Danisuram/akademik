<?php

Class Cash_transaksi extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('ssp');
        $this->load->model('Model_cash_transaksi');
        $this->load->library('upload');
        $this->load->library('dompdf_gen');
        $this->load->library('session');
    }

    function data() {
       $datauser = $this->session->userdata('username');
        // nama tabel
        $table = 'tbl_cash_transaksi';
        // nama PK
        $primaryKey = 'id_ctran';
        // list field
        $columns = array(
            array('db' => 'tgl', 'dt' => 'tgl'),
            array('db' => 'pemberi', 'dt' => 'pemberi'),
            array('db' => 'penerima', 'dt' => 'penerima'),
            array('db' => 'cashflow', 'dt' => 'cashflow'),
            array('db' => 'jenis_pembayaran', 'dt' => 'jenis_pembayaran'),
            array('db' => 'keterkaitan', 'dt' => 'keterkaitan'),
            array('db' => 'nama_ket', 'dt' => 'nama_ket'),
            array('db' => 'nominal', 'dt' => 'nominal'),
            array('db' => 'saldo', 'dt' => 'saldo'),
            array(
                'db' => 'id_ctran',
                'dt' => 'aksi',
                'formatter' => function( $d) {
                    //return "<a href='edit.php?id=$d'>EDIT</a>";
                    return anchor('cash_transaksi/edit/'.$d,'<i class="fa fa-edit"></i>','class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"').'
                        '.anchor('cash_transaksi/delete/'.$d,'<i class="fa fa-trash"></i>','onclick="return konfirmasi()" class="btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Delete"');
                }
            ),
            array(
                'db' => 'id_ctran',
                'dt' => 'export',
                'formatter' => function( $d){
                    return anchor('cash_transaksi/export_pertransaksi/'.$d,'<i>EXPORT</i>');
                }
            )
        );

        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );

        if($datauser=="keuanganpusat"){
           echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)

          );
         
        } else{
          
          if($datauser=="keuangansd"){
           $kategori = "1";
          
          }elseif ($datauser=="keuangansmp") {
            $kategori = "2";
            
          }elseif ($datauser=="keuangansma") {
            $kategori = "3";
           
          }elseif ($datauser=="keuangankantin") {
            $kategori="4";
          }else{
            echo"404 error not found hehe.";
            
          }
          $whereAll = "kategori_keuangan =".$kategori."";
          
          echo json_encode(
             SSP::complex($_GET, $sql_details, $table, $primaryKey, $columns, null, $whereAll)
           );
        }

    }

    function index() {
        $this->template->load('template', 'cash_transaksi/list');
    }

    function add() {
        if (isset($_POST['submit'])) {
            //$data = $this->input->post();
            // print_r($data);
            // exit;
            $datauser = $this->session->userdata('username');
            if($datauser=="keuangansd"){
              $kategori = "1";
          
            }elseif ($datauser=="keuangansmp") {
              $kategori = "2";
            
            }elseif ($datauser=="keuangansma") {
               $kategori = "3";
           
            }elseif ($datauser=="keuangankantin") {
               $kategori = "4";
            }elseif ($datauser=="keuanganpusat") {
                $kategori="5";
            }else{
                echo "404 not found";
            }

            // print_r($kategori);
            // exit;
            $this->Model_cash_transaksi->save($kategori);
            redirect('cash_transaksi');
        } else {
            $data2['ruangKelas']         = $this->Model_cash_transaksi->getKelas();
            $this->template->load('template', 'cash_transaksi/add',$data2);
        }
    }

    function getJP($cashflow){
      $datauser = $this->session->userdata('username');
            if($datauser=="keuangansd"){
              $kategori = "1";
          
            }elseif ($datauser=="keuangansmp") {
              $kategori = "2";
            
            }elseif ($datauser=="keuangansma") {
               $kategori = "3";
           
            }elseif ($datauser=="keuangankantin") {
               $kategori = "4";
            }elseif ($datauser=="keuanganpusat") {
                $kategori="5";
            }else{
                echo "404 not found";
            }

        $x = $this->Model_cash_transaksi->getJP($cashflow, $kategori);
       echo json_encode($x);
    }

    function getKelas($kelas){
       $x = $this->Model_cash_transaksi->getNamasiswa($kelas);
       echo json_encode($x);
    }

    function getGuru(){
        $x = $this->Model_cash_transaksi->getGuru();
       echo json_encode($x);
    }

    function edit(){
        if(isset($_POST['submit'])){
            $data = $this->input->post();
            print_r($data);
            exit;


            $this->Model_cash_transaksi->update();
          
            redirect('cash_transaksi');
        }else{
            $id_cash_transaksi      = $this->uri->segment(3);
            $data['cash_transaksi'] = $this->db->get_where('tbl_cash_transaksi',array('id_ctran'=>$id_cash_transaksi))->row_array();

            $this->template->load('template', 'cash_transaksi/edit',$data);
        }
    }

    function delete(){
        $id_cash_transaksi = $this->uri->segment(3);
        if(!empty($id_cash_transaksi)){
            // proses delete data
            $this->db->where('id_ctran',$id_cash_transaksi);
            $this->db->delete('tbl_cash_transaksi');
        }
        redirect('cash_transaksi');
    }

    function export_pertransaksi($d){
      $x = $d;
      // echo $x;

      $data = $this->Model_cash_transaksi->get_export_pertransaksi($x);
      $cashflow = $data[0]['cashflow'];

      // if ($cashflow=="outcome") {
        $data2['data_transaksi'] = array($data[0]['id_ctran'],$data[0]['tgl'],$data[0]['pemberi'],$data[0]['penerima'],$data[0]['jenis_pembayaran'],$data[0]['keterkaitan'],$data[0]['nama_ket'],$data[0]['nominal'],$data[0]['keterangan']);
      //   # code...
      // }elseif($cashflow=="income"){
      //   $data2['data_transaksi'] = array($data[0]['id_ctran'],$data[0]['tgl'],$data[0]['penerima'],$data[0]['pemberi'],$data[0]['jenis_pembayaran'],$data[0]['keterkaitan'],$data[0]['nama_ket'],$data[0]['nominal'],$data[0]['keterangan']);

      // }else{
      //   echo "404 eror not found";
      // }
      $this->load->view('cash_transaksi/pdf_transaksi', $data2);

        $html = $this->output->get_output();

        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("IDTRANSCASH".$data[0]['id_ctran'].".pdf");
    }

    function export_cashtransaction(){

            //load our new PHPExcel library
      $this->load->library('excel');
      //activate worksheet number 1
      $this->excel->setActiveSheetIndex(0);
      //name the worksheet
      $this->excel->getActiveSheet()->setTitle('test worksheet');
      //set cell A1 content with some text
      $this->excel->getActiveSheet()->setCellValue('A1', 'id_ctran');
  		$this->excel->getActiveSheet()->setCellValue('B1', 'tgl');
  		$this->excel->getActiveSheet()->setCellValue('C1', 'pemberi');
  		$this->excel->getActiveSheet()->setCellValue('D1', 'penerima');
  		$this->excel->getActiveSheet()->setCellValue('E1', 'cashflow');
  		$this->excel->getActiveSheet()->setCellValue('F1', 'jenis_pembayaran');
  		$this->excel->getActiveSheet()->setCellValue('G1', 'keterkaitan');
  		$this->excel->getActiveSheet()->setCellValue('H1', 'nama_ket');
  		$this->excel->getActiveSheet()->setCellValue('I1', 'nominal');
  		$this->excel->getActiveSheet()->setCellValue('J1', 'keterangan');
  		$this->excel->getActiveSheet()->setCellValue('K1', 'saldo');

      $data = $this->Model_cash_transaksi->get_export_cashtransaction();

      $exceldata=array();
  		foreach ($data as $row){
  				$exceldata[] = $row;
  		}

      $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A2');

      $filename='cash_transaksi.xls'; //save our workbook as this file name
      header('Content-Type: application/vnd.ms-excel'); //mime type
      header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
      header('Cache-Control: max-age=0'); //no cache

      //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
      //if you want to save it as .XLSX Excel 2007 format
      $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
      //force user to download the Excel file without writing it to server's HD
      $objWriter->save('php://output');
    }


}
