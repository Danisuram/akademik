<?php

Class Siswa extends CI_Controller {

    function __construct() {
        parent::__construct();
        //chekAksesModule();
        $this->load->library('ssp');
        $this->load->model('Model_siswa');
        $this->load->library('upload');
        $this->load->library('dompdf_gen');
    }

    
    
    function data() {
   
        // nama tabel
        $table = 'tbl_siswa';
        // nama PK
        $primaryKey = 'nim';
        // list field
        $columns = array(
            /*array('db' => 'foto',
                'dt' => 'foto',
                'formatter' => function( $d) {
                   if(empty($d)){
                       return "<img width='30px' src='".  base_url()."/uploads/user-siluet.jpg'>";
                   }else{
                       return "<img width='20px' src='".  base_url()."/uploads/".$d."'>";
                   }   
                }
            ),*/
            array('db' => 'nim', 'dt' => 'nim'),
            array('db' => 'nama', 'dt' => 'nama'),
            array('db' => 'tempat_lahir', 'dt' => 'tempat_lahir'),
            array('db' => 'tanggal_lahir', 'dt' => 'tanggal_lahir'),
            array('db' => 'alamat', 'dt' => 'alamat'),
            array('db' => 'file_ijazah', 'dt' => 'file_ijazah',
                    'formatter' => function( $d) {
                    if($d=="")
                    {$a="Belum ada file";}
                    else
                        {$a="<a href='".base_url()."/uploads/ijz_siswa/".$d."' target='_blank'>Download</a>";}
                    //return "<a href='edit.php?id=$d'>EDIT</a>";
                   return $a;
                }
                ),
            array('db' => 'file_kk', 'dt' => 'file_kk',
                    'formatter' => function( $d) {
                    if($d=="")
                    {$a="Belum ada file";}
                    else
                        {$a="<a href='".base_url()."/uploads/file_kk/".$d."' target='_blank'>Download</a>";}
                    //return "<a href='edit.php?id=$d'>EDIT</a>";
                   return $a;
                }
                ),
            array(
                'db' => 'nim',
                'dt' => 'aksi',
                'formatter' => function( $d) {
                    //return "<a href='edit.php?id=$d'>EDIT</a>";
                    return anchor('siswa/edit/'.$d,'<i class="fa fa-edit"></i>','class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"').' 
                        '.anchor('siswa/delete/'.$d,'<i class="fa fa-trash"></i>', 'onclick="return konfirmasi()" class="btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Delete"');
                }
            ),
            array(
                'db' => 'nim',
                'dt' => 'export',
                'formatter' => function( $d){
                    return anchor('siswa/export_persiswa/'.$d,'<i>EXPORT</i>');
                }
            )
        );

        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
        );
    }

    function index() {
        $this->template->load('template', 'siswa/list');
    }

    function add() {
        if (isset($_POST['submit'])) {
            $uploadFoto = $this->upload_kk_siswa();
            $uploadFoto1 = $this->upload_ijazah_siswa();
            $this->Model_siswa->save($uploadFoto,$uploadFoto1);
            redirect('siswa');
        } else {
            $this->template->load('template', 'siswa/add');
        }
    }
    
    function edit(){
        if(isset($_POST['submit'])){
            $uploadFoto = $this->upload_kk_siswa();
            $uploadFoto1 = $this->upload_ijazah_siswa();
            $this->Model_siswa->update($uploadFoto,$uploadFoto1);
            redirect('siswa');
        }else{
            $nim           = $this->uri->segment(3);
            $data['siswa'] = $this->db->get_where('tbl_siswa',array('nim'=>$nim))->row_array();
            $this->template->load('template', 'siswa/edit',$data);
        }
    }
    
    function delete(){
        $nim = $this->uri->segment(3);
        if(!empty($nim)){
            // proses delete data
            $this->db->where('nim',$nim);
            $this->db->delete('tbl_siswa');
        }
        redirect('siswa');
    }

    function export_persiswa( $d){
        $x = $d;
        $data['data_siswa'] = $this->Model_siswa->get_export_persiswa($x);
        
        $this->load->view('siswa/pdf_siswa', $data);
        $html = $this->output->get_output();

        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream($data['data_siswa'][0]['nim']
          .$data['data_siswa'][0]['nama'].".pdf");

    }
    
    function upload_kk_siswa(){
            // proses upload
        $config['upload_path'] = './uploads/kk/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload

        $this->upload->initialize($config);
        if(!empty($_FILES['file_kk']['name'])){
            if ($this->upload->do_upload('file_kk')){

                $gbr = $this->upload->data();
                $config['image_library']='gd2';
                $config['source_image']='./uploads/kk/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 800;
                $config['height']= 600;
                $config['new_image']= './uploads/kk/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $gambar=$gbr['file_name'];
                return $gambar;
            }
        }
        else
        {
            return $this->input->post("file_kk_lama");
            
        }
    }

    function upload_ijazah_siswa(){
            // proses upload
        $config['upload_path'] = './uploads/ijz_siswa/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload

        $this->upload->initialize($config);
        if(!empty($_FILES['file_ijazah']['name'])){
            if ($this->upload->do_upload('file_ijazah')){

                $gbr = $this->upload->data();
                $config['image_library']='gd2';
                $config['source_image']='./uploads/ijz_siswa/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 800;
                $config['height']= 600;
                $config['new_image']= './uploads/ijz_siswa/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $gambar=$gbr['file_name'];
                return $gambar;
            }
        }
        else
        {
            return $this->input->post("file_ijazah_lama");
            
        }
    }
    
    
    function siswa_aktif(){
        $this->template->load('template', 'siswa/siswa_aktif');
    }
    
    function load_data_siswa_by_rombel(){
        $rombel = $_GET['rombel'];
        
        echo "<table class='table table-bordered'>
            <tr><th width='90'>NIM</th><th>NAMA</th></tr>";
        $this->db->where('id_rombel',$rombel);
        $siswa = $this->db->get('tbl_siswa');
        foreach ($siswa->result() as $row){
            echo "<tr><td>$row->nim</td><td>$row->nama</td></tr>";
        }
        echo"</table>";
    }
    
    function data_by_rombel_excel(){
        $this->load->library('CPHP_excel');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'NIM');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'SISWA');
        
        $rombel = $_POST['rombel'];
        $this->db->where('id_rombel',$rombel);
        $siswa = $this->db->get('tbl_siswa');
        $no=2;
        foreach ($siswa->result() as $row){
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$no, $row->nim);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$no, $row->nama);
            $no++;
        }
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
        $objWriter->save("data-siswa.xlsx");
        $this->load->helper('download');
        force_download('data-siswa.xlsx', NULL);
    }

    function export_siswa(){

            //load our new PHPExcel library
      $this->load->library('excel');
      //activate worksheet number 1
      $this->excel->setActiveSheetIndex(0);
      //name the worksheet
      $this->excel->getActiveSheet()->setTitle('test worksheet');
      //set cell A1 content with some text
      $this->excel->getActiveSheet()->setCellValue('A1', 'no_urut');
        $this->excel->getActiveSheet()->setCellValue('B1', 'nim');
        $this->excel->getActiveSheet()->setCellValue('C1', 'nama');
        $this->excel->getActiveSheet()->setCellValue('D1', 'kelas');
        $this->excel->getActiveSheet()->setCellValue('E1', 'gender');
        $this->excel->getActiveSheet()->setCellValue('F1', 'tanggal_lahir');
        $this->excel->getActiveSheet()->setCellValue('G1', 'tempat_lahir');
        $this->excel->getActiveSheet()->setCellValue('H1', 'alamat');
        $this->excel->getActiveSheet()->setCellValue('I1', 'sekolah_asal');
        $this->excel->getActiveSheet()->setCellValue('J1', 'nm_ibu');
        $this->excel->getActiveSheet()->setCellValue('K1', 'nm_bapak');
        $this->excel->getActiveSheet()->setCellValue('L1', 'pkjr_ibu');
      $this->excel->getActiveSheet()->setCellValue('M1', 'pkjr_bapak');
      $this->excel->getActiveSheet()->setCellValue('N1', 'nm_wali');
      $this->excel->getActiveSheet()->setCellValue('O1', 'pkjr_wali');
      $this->excel->getActiveSheet()->setCellValue('P1', 'alamat_wali');
      $this->excel->getActiveSheet()->setCellValue('Q1', 'no_telp');
      $this->excel->getActiveSheet()->setCellValue('R1', 'no_kk');
      $this->excel->getActiveSheet()->setCellValue('S1', 'file_kk');
      $this->excel->getActiveSheet()->setCellValue('T1', 'ijazah');
      $this->excel->getActiveSheet()->setCellValue('U1', 'file_ijazah');
      $this->excel->getActiveSheet()->setCellValue('V1', 'nisn');
      $this->excel->getActiveSheet()->setCellValue('W1', 'nik');
      $this->excel->getActiveSheet()->setCellValue('X1', 'riwayat_penyakit');
      $this->excel->getActiveSheet()->setCellValue('Y1', 'abk');
      $this->excel->getActiveSheet()->setCellValue('Z1', 'no_skhu');
      $this->excel->getActiveSheet()->setCellValue('AA1', 'kd_agama');
      $this->excel->getActiveSheet()->setCellValue('AB1', 'foto');
      $this->excel->getActiveSheet()->setCellValue('AC1', 'id_rombel');

      $data = $this->Model_siswa->get_export_siswa();

      $exceldata=array();
        foreach ($data as $row){
                $exceldata[] = $row;
        }

      $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A2');

      $filename='siswa.xls'; //save our workbook as this file name
      header('Content-Type: application/vnd.ms-excel'); //mime type
      header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
      header('Cache-Control: max-age=0'); //no cache

      //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
      //if you want to save it as .XLSX Excel 2007 format
      $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
      //force user to download the Excel file without writing it to server's HD
      $objWriter->save('php://output');
    }

}