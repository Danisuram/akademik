<?php 
	Class Rekening extends CI_Controller {

	   function __construct() {
        parent::__construct();
        $this->load->library('ssp');
        $this->load->model('Model_rekening');
        $this->load->library('upload');
        $this->load->library('session');
     
    }

        function data() {
            $datauser = $this->session->userdata('username');

        // nama tabel
        $table = 'tbl_rekening';
        // nama PK
        $primaryKey = 'norek';
        // list field
        $columns = array(
            array('db' => 'norek', 'dt' => 'norek'),
            array('db' => 'nama_rek', 'dt' => 'nama_rek'),
            array('db' => 'nama_bank', 'dt' => 'nama_bank'),
            array('db' => 'nama_pemilik_rek', 'dt' => 'nama_pemilik_rek'),
            array('db' => 'saldo', 'dt' => 'saldo',
                   'formatter'=> function($d){
                    return number_format($d, 2);
                   } 

                ),
            array(
                'db' => 'norek',
                'dt' => 'aksi',
                'formatter' => function( $d) {
                    //return "<a href='edit.php?id=$d'>EDIT</a>";
                    return anchor('rekening/edit/'.$d,'<i class="fa fa-edit"></i>','class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"').' 
                        '.anchor('rekening/delete/'.$d,'<i class="fa fa-trash"></i>','onclick="return konfirmasi()" class="btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Delete"');
                }
            )
        );

        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );

        
          if($datauser=="keuangansd"){
           $kategori = "1";
          }elseif ($datauser=="keuangansmp"){
            $kategori = "2";
          }elseif ($datauser=="keuangansma"){
            $kategori = "3";
          }elseif ($datauser=="keuangankantin"){
            $kategori="4";
          }elseif ($datauser=="keuanganpusat"){
            $kategori="5";
          }else{
            echo"404 error not found hehe.";
            
          }
          $whereAll = "kategori_rekening =".$kategori."";
          
          echo json_encode(
             SSP::complex($_GET, $sql_details, $table, $primaryKey, $columns, null, $whereAll)
           );
        
        
    }

		function index() {
        $this->template->load('template', 'rekening/list');
    	}

    	function add() {
        if (isset($_POST['submit'])) {
        //	$this->load->model('Model_rekening');
            $datauser = $this->session->userdata('username');
            if($datauser=="keuangansd"){
              $kategori = "1";
          
            }elseif ($datauser=="keuangansmp") {
              $kategori = "2";
            
            }elseif ($datauser=="keuangansma") {
               $kategori = "3";
           
            }elseif ($datauser=="keuangankantin") {
               $kategori = "4";
            }elseif ($datauser=="keuanganpusat") {
                $kategori="5";
            }else{
                echo "404 not found";
            }
            	$this->Model_rekening->save($kategori);
            	redirect('Rekening');
        } else {
            	$this->template->load('template', 'rekening/add');
        	}
    	}

        function edit(){
        if(isset($_POST['submit'])){
            
            $this->Model_rekening->update();
            redirect('rekening');
        }else{
            $norek      = $this->uri->segment(3);
            // print_r($id);
            // exit;
            $data['rekening'] = $this->db->get_where('tbl_rekening',array('norek'=>$norek))->row_array();
            $this->template->load('template', 'rekening/edit',$data);
          }
        }

        function delete(){
        $norek = $this->uri->segment(3);
        if(!empty($norek)){
            // proses delete data
            $this->db->where('norek',$norek);
            $this->db->delete('tbl_rekening');
        }
        redirect('rekening');
    }

	}
?>
 