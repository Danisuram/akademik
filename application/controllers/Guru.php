<?php

Class Guru extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('ssp');
        $this->load->model('Model_guru');
         $this->load->library('upload');
         $this->load->library('dompdf_gen');
    }

    function data() {
        // nama tabel
        $table = 'tbl_guru';
        // nama PK
        $primaryKey = 'nip';
        // list field
        $columns = array(
            array('db' => 'nip', 'dt' => 'nip'),
            array('db' => 'nip', 'dt' => 'nip'),
            array('db' => 'nama_guru', 'dt' => 'nama_guru'),
            array('db' => 'kelamin', 
                  'dt' => 'kelamin',
                  'formatter' => function( $d) {
                    //return "<a href='edit.php?id=$d'>EDIT</a>";
                    return $d=='P'?'Perempuan':'Laki-laki';
                }),
            array('db' => 'tempat_lahir', 'dt' => 'tempat_lahir'),
            array('db' => 'tgl_lahir', 'dt' => 'tgl_lahir'),
            array('db' => 'alamat', 'dt' => 'alamat'),
            array(
                'db' => 'nip',
                'dt' => 'aksi',
                'formatter' => function( $d) {
                    //return "<a href='edit.php?id=$d'>EDIT</a>";
                    return anchor('guru/edit/'.$d,'<i class="fa fa-edit"></i>','class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"').' 
                        '.anchor('guru/delete/'.$d,'<i class="fa fa-trash"></i>','onclick="return konfirmasi()" class="btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Delete"');
                }
            ),
            array(
                'db' => 'nip',
                'dt' => 'export',
                'formatter' => function( $d){
                    return anchor('guru/export_perguru/'.$d,'<i>EXPORT</i>');
                }
            )
        );

        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
        );
    }

    function index() {
        $this->template->load('template', 'guru/list');
    }

    function add() {
        if (isset($_POST['submit'])) {
            // $data = $this->input->post();
            // print_r($data);
            // exit;
        	$uploadFoto = $this->upload_kk();
        	$uploadFoto1 = $this->upload_ktp();
        	$uploadFoto2 = $this->upload_sd();
        	$uploadFoto3 = $this->upload_smp();
        	$uploadFoto4 = $this->upload_sma();
        	$uploadFoto5 = $this->upload_s1();
        	$uploadFoto6 = $this->upload_s2();
        	$uploadFoto7 = $this->upload_s3();

            $this->Model_guru->save($uploadFoto,$uploadFoto1,$uploadFoto2,$uploadFoto3,$uploadFoto4,$uploadFoto5,$uploadFoto6,$uploadFoto7);
            redirect('guru');
        } else {
            $data['mapel'] = $this->Model_guru->getMapel();
            $this->template->load('template', 'guru/add', $data);

        }
    }
    
    function edit(){
        if(isset($_POST['submit'])){
        	$uploadFoto = $this->upload_kk();
        	$uploadFoto1 = $this->upload_ktp();
        	$uploadFoto2 = $this->upload_sd();
        	$uploadFoto3 = $this->upload_smp();
        	$uploadFoto4 = $this->upload_sma();
        	$uploadFoto5 = $this->upload_s1();
        	$uploadFoto6 = $this->upload_s2();
        	$uploadFoto7 = $this->upload_s3();
            $this->Model_guru->update($uploadFoto,$uploadFoto1,$uploadFoto2,$uploadFoto3,$uploadFoto4,$uploadFoto5,$uploadFoto6,$uploadFoto7);
            redirect('guru');
        }else{
            $id_guru        = $this->uri->segment(3);
            $data['guru']   = $this->db->get_where('tbl_guru',array('nip'=>$id_guru))->row_array();
            $data['mapel']  = $this->Model_guru->getMapel();
            $this->template->load('template', 'guru/edit',$data);
        }
    }
    
    function delete(){
        $id_guru = $this->uri->segment(3);
        if(!empty($id_guru)){
            // proses delete data
            $this->db->where('nip',$id_guru);
            $this->db->delete('tbl_guru');
        }
        redirect('guru');
    }

    function export_perguru( $d){
        $x = $d;
        $data['data_guru'] = $this->Model_guru->get_export_perguru($x);

        
        $this->load->view('guru/pdf_guru', $data);
        $html = $this->output->get_output();

        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream($data['data_guru'][0]['nip']
            .$data['data_guru'][0]['nama_guru'].".pdf");
    }


    function upload_kk(){
            // proses upload
        $config['upload_path'] = './uploads/kk/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload

        $this->upload->initialize($config);
        if(!empty($_FILES['file_kk']['name'])){
            if ($this->upload->do_upload('file_kk')){

                $gbr = $this->upload->data();
                $config['image_library']='gd2';
                $config['source_image']='./uploads/kk/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 800;
                $config['height']= 600;
                $config['new_image']= './uploads/kk/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $gambar=$gbr['file_name'];
                return $gambar;
            }
        }
        else
        {
            return $this->input->post("file_kk_lama");
            
        }
    }

    function upload_ktp(){
            // proses upload
        $config['upload_path'] = './uploads/ktp/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload

        $this->upload->initialize($config);
        if(!empty($_FILES['file_ktp']['name'])){
            if ($this->upload->do_upload('file_ktp')){

                $gbr = $this->upload->data();
                $config['image_library']='gd2';
                $config['source_image']='./uploads/ktp/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 800;
                $config['height']= 600;
                $config['new_image']= './uploads/ktp/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $gambar=$gbr['file_name'];
                return $gambar;
            }
        }
        else
        {
            return $this->input->post("file_ktp_lama");
            
        }
    }

     function upload_sd(){
            // proses upload
        $config['upload_path'] = './uploads/sd/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload

        $this->upload->initialize($config);
        if(!empty($_FILES['ijazah_sd']['name'])){
            if ($this->upload->do_upload('ijazah_sd')){

                $gbr = $this->upload->data();
                $config['image_library']='gd2';
                $config['source_image']='./uploads/sd/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 800;
                $config['height']= 600;
                $config['new_image']= './uploads/sd/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $gambar=$gbr['file_name'];
                return $gambar;
            }
        }
        else
        {
            return $this->input->post("ijazah_sd_lama");
            
        }
    }

     function upload_smp(){
            // proses upload
        $config['upload_path'] = './uploads/smp/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload

        $this->upload->initialize($config);
        if(!empty($_FILES['ijazah_smp']['name'])){
            if ($this->upload->do_upload('ijazah_smp')){

                $gbr = $this->upload->data();
                $config['image_library']='gd2';
                $config['source_image']='./uploads/smp/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 800;
                $config['height']= 600;
                $config['new_image']= './uploads/smp/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $gambar=$gbr['file_name'];
                return $gambar;
            }
        }
        else
        {
            return $this->input->post("ijazah_smp_lama");
            
        }
    }

     function upload_sma(){
            // proses upload
        $config['upload_path'] = './uploads/sma/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload

        $this->upload->initialize($config);
        if(!empty($_FILES['ijazah_sma']['name'])){
            if ($this->upload->do_upload('ijazah_sma')){

                $gbr = $this->upload->data();
                $config['image_library']='gd2';
                $config['source_image']='./uploads/sma/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 800;
                $config['height']= 600;
                $config['new_image']= './uploads/sma/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $gambar=$gbr['file_name'];
                return $gambar;
            }
        }
        else
        {
            return $this->input->post("ijazah_sma_lama");
            
        }
    }

     function upload_s1(){
            // proses upload
        $config['upload_path'] = './uploads/s1/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload

        $this->upload->initialize($config);
        if(!empty($_FILES['ijazah_s1']['name'])){
            if ($this->upload->do_upload('ijazah_s1')){

                $gbr = $this->upload->data();
                $config['image_library']='gd2';
                $config['source_image']='./uploads/s1/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 800;
                $config['height']= 600;
                $config['new_image']= './uploads/s1/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $gambar=$gbr['file_name'];
                return $gambar;
            }
        }
        else
        {
            return $this->input->post("ijazah_s1_lama");
            
        }
    }

     function upload_s2(){
            // proses upload
        $config['upload_path'] = './uploads/s2/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload

        $this->upload->initialize($config);
        if(!empty($_FILES['ijazah_s2']['name'])){
            if ($this->upload->do_upload('ijazah_s2')){

                $gbr = $this->upload->data();
                $config['image_library']='gd2';
                $config['source_image']='./uploads/s2/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 800;
                $config['height']= 600;
                $config['new_image']= './uploads/s2/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $gambar=$gbr['file_name'];
                return $gambar;
            }
        }
        else
        {
            return $this->input->post("ijazah_s2_lama");
            
        }
    }

     function upload_s3(){
            // proses upload
        $config['upload_path'] = './uploads/s3/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload

        $this->upload->initialize($config);
        if(!empty($_FILES['ijazah_s3']['name'])){
            if ($this->upload->do_upload('ijazah_s3')){

                $gbr = $this->upload->data();
                $config['image_library']='gd2';
                $config['source_image']='./uploads/s3/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 800;
                $config['height']= 600;
                $config['new_image']= './uploads/s3/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $gambar=$gbr['file_name'];
                return $gambar;
            }
        }
        else
        {
            return $this->input->post("ijazah_s3_lama");
            
        }
    }

    function export_guru(){

            //load our new PHPExcel library
      $this->load->library('excel');
      //activate worksheet number 1
      $this->excel->setActiveSheetIndex(0);
      //name the worksheet
      $this->excel->getActiveSheet()->setTitle('test worksheet');
      //set cell A1 content with some text
      $this->excel->getActiveSheet()->setCellValue('A1', 'no_urut');
      $this->excel->getActiveSheet()->setCellValue('B1', 'nip');
        $this->excel->getActiveSheet()->setCellValue('C1', 'nama_guru');
        $this->excel->getActiveSheet()->setCellValue('D1', 'kelas');
        $this->excel->getActiveSheet()->setCellValue('E1', 'kelamin');
        $this->excel->getActiveSheet()->setCellValue('F1', 'tempat_lahir');
        $this->excel->getActiveSheet()->setCellValue('G1', 'tgl_lahir');
        $this->excel->getActiveSheet()->setCellValue('H1', 'alamat');
        $this->excel->getActiveSheet()->setCellValue('I1', 'no_telp');
        $this->excel->getActiveSheet()->setCellValue('J1', 'no_kk');
        $this->excel->getActiveSheet()->setCellValue('K1', 'file_kk');
        $this->excel->getActiveSheet()->setCellValue('L1', 'no_ktp');
      $this->excel->getActiveSheet()->setCellValue('M1', 'file_ktp');
      $this->excel->getActiveSheet()->setCellValue('N1', 'ijazah_sd');
      $this->excel->getActiveSheet()->setCellValue('O1', 'ijazah_smp');
      $this->excel->getActiveSheet()->setCellValue('P1', 'ijazah_sma');
      $this->excel->getActiveSheet()->setCellValue('Q1', 'ijazah_sma');
      $this->excel->getActiveSheet()->setCellValue('R1', 'ijazah_s1');
      $this->excel->getActiveSheet()->setCellValue('S1', 'ijazah_s2');
      $this->excel->getActiveSheet()->setCellValue('T1', 'ijazah_s3');
      $this->excel->getActiveSheet()->setCellValue('U1', 'nisn');
      $this->excel->getActiveSheet()->setCellValue('V1', 'nik');
      $this->excel->getActiveSheet()->setCellValue('W1', 'riwayat_penyakit');

      $data = $this->Model_guru->get_export_guru();

      $exceldata=array();
        foreach ($data as $row){
                $exceldata[] = $row;
        }

      $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A2');

      $filename='guru.xls'; //save our workbook as this file name
      header('Content-Type: application/vnd.ms-excel'); //mime type
      header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
      header('Cache-Control: max-age=0'); //no cache

      //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
      //if you want to save it as .XLSX Excel 2007 format
      $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
      //force user to download the Excel file without writing it to server's HD
      $objWriter->save('php://output');
    }
}


