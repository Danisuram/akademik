<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master_cash extends CI_Controller {

	function __construct() {
			parent::__construct();
        	$this->load->model('Model_master_cash');
   		 }

		function index() {
        	$this->template->load('template', 'master_cash/list');
    	}

    	function add() {
        if (isset($_POST['submit'])) {
        		//$this->load->model('Model_master_cash');
            	$this->Model_master_cash->save();
            	redirect('Master_cash');
        	} else {
            	$this->template->load('template', 'master_cash/add');
        	}
    	}

}

/* End of file Master_cash.php */
/* Location: ./application/controllers/Master_cash.php */

