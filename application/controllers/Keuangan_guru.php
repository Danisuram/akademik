<?php

Class Keuangan_guru extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('ssp');
        $this->load->model('Model_keuangan_guru');
    }

    function data() {
        // nama tabel
        $table = 'tbl_keu_guru';
        // nama PK
        $primaryKey = 'id_tran';
        // list field
        $columns = array(
            array('db' => 'nip', 'dt' => 'nip'),
            array('db' => 'tgl_pembayaran', 'dt' => 'tgl_pembayaran'),
            array('db' => 'jumlah', 'dt' => 'jumlah'),
            array('db' => 'pembayaran', 'dt' => 'pembayaran'),
            array('db' => 'rek_asal', 'dt' => 'rek_asal'),
            array('db' => 'rek_tujuan', 'dt' => 'rek_tujuan'),
            
            array(
                'db' => 'id_tran',
                'dt' => 'aksi',
                'formatter' => function( $d) {
                    //return "<a href='edit.php?id=$d'>EDIT</a>";
                    return anchor('keuangan_guru/edit/'.$d,'<i class="fa fa-edit"></i>','class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"').' 
                        '.anchor('keuangan_guru/delete/'.$d,'<i class="fa fa-trash"></i>','onclick="return konfirmasi()"class="btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Delete"');
                }
            )
        );

        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
        );
    }

    function index() {
        $this->template->load('template', 'keuangan_guru/list');
    }

    function add() {
        if (isset($_POST['submit'])) {
            $this->Model_keuangan_guru->save();
            redirect('keuangan_guru');
        } else {
            $this->template->load('template', 'keuangan_guru/add');
        }
    }
    
    function edit(){
        if(isset($_POST['submit'])){
            $this->Model_keuangan_guru->update();
            redirect('keuangan_guru');
        }else{
            $id_tran      = $this->uri->segment(3);
            $data['keuangan_guru'] = $this->db->get_where('tbl_keu_guru',array('id_tran'=>$id_tran))->row_array();
            $this->template->load('template', 'keuangan_guru/edit',$data);
        }
    }
    
    function delete(){
        $id_tran= $this->uri->segment(3);
        if(!empty($id_tran)){
            // proses delete data
            $this->db->where('id_tran',$id_tran);
            $this->db->delete('tbl_keu_guru');
        }
        redirect('keuangan_guru');
    }


    function upload_kk(){
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'jpg|png';
        $config['max_size']             = 1024; // imb
        $config['width']= 800;
        $config['height']= 600;
                
        $this->load->library('upload', $config);
            // proses upload
        $this->upload->do_upload('userfile');
        $upload = $this->upload->data();
        return $upload['file_name'];
    }
}