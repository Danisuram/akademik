<?php

class Model_rekening_yayasan extends CI_Model {

    public $table ="tbl_rekening_yayasan";
    //public $table2="tbl_rekening"

    function saveplus($data, $saldoakhir, $kategori) {
        $no_rek = $this->input->post('norek_yayasan',TRUE);
        $data["saldo"] = $saldoakhir;
        $vKategori = $kategori;

    


        //$vSaldoakhir = $saldoakhir;

      //  unset($data["submit"]);

        $data = array(
            'tgl_transaksi'                     => $this->input->post('tanggal_transaksi', TRUE),
            'norek_yayasan'                     => $this->input->post('norek_yayasan', TRUE),
            'nama_rek'                          => $this->input->post('nama_rek', TRUE),
            'bank_yayasan'                      => $this->input->post('bank_yayasan', TRUE),
            'nama_pemilik_rek_yayasan'          => $this->input->post('nama_pemilik_rek_yayasan', TRUE),
            'nama'                              => $this->input->post('nama', TRUE),
            'norek'                             => $this->input->post('norek', TRUE),
            'bank'                              => $this->input->post('bank', TRUE),
            'cashflow'                          => $this->input->post('cashflow',TRUE),
            'jenis_pembayaran'                  => $this->input->post('jenispembayaran',TRUE),
            'keterkaitan'                       => $this->input->post('keterkaitan',TRUE),
            'nomor_induk'                       => $this->input->post('nomor_induk', TRUE),    
            'nama_ket'                          => $this->input->post('namaket',TRUE),
            'nominal'                           => $this->input->post('nominal', TRUE),
            'keterangan'                        => $this->input->post('keterangan', TRUE),
            'kategori_keuangan'                 => $vKategori,
            'saldo'                             => $saldoakhir
        );

        

        $this->db->insert($this->table,$data);
        $this->db->set('saldo',$saldoakhir);
        $this->db->where('norek',$no_rek);
        $this->db->update('tbl_rekening');


    }


    function savemin($data, $saldoakhir, $kategori) {
        $no_rek = $this->input->post('norek_yayasan',TRUE);
        $data["saldo"] = $saldoakhir;
        $vKategori = $kategori;


        //$vSaldoakhir = $saldoakhir;

       // unset($data["submit"]);

        $data = array(
            'tgl_transaksi'                     => $this->input->post('tanggal_transaksi', TRUE),
            'norek_yayasan'                     => $this->input->post('norek_yayasan', TRUE),
            'nama_rek'                          => $this->input->post('nama_rek', TRUE),
            'bank_yayasan'                      => $this->input->post('bank_yayasan', TRUE),
            'nama_pemilik_rek_yayasan'          => $this->input->post('nama_pemilik_rek_yayasan', TRUE),
            'nama'                              => $this->input->post('nama', TRUE),
            'norek'                             => $this->input->post('norek', TRUE),
            'bank'                              => $this->input->post('bank', TRUE),
            'cashflow'                          => $this->input->post('cashflow',TRUE),
            'jenis_pembayaran'                  => $this->input->post('jenispembayaran',TRUE),
            'keterkaitan'                       => $this->input->post('keterkaitan',TRUE),
            'nomor_induk'                       => $this->input->post('nomor_induk', TRUE),
            'nama_ket'                          => $this->input->post('namaket',TRUE),
            'nominal'                           => $this->input->post('nominal', TRUE),
            'keterangan'                        => $this->input->post('keterangan', TRUE),
            'kategori_keuangan'                 => $vKategori,
            'saldo'                             => $saldoakhir
        );

        // print_r($data);
        // exit;
        

        $this->db->insert($this->table,$data);
        $this->db->set('saldo',$saldoakhir);
        $this->db->where('norek',$no_rek);
        $this->db->update('tbl_rekening');
    }



    function getNIPguru(){
        $this->db->SELECT('nip');
        $query = $this->db->get('tbl_guru');
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function getNIMsiswa(){
        $this->db->SELECT('nim');
        $query = $this->db->get('tbl_siswa');
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function getDataguru($nip){
        $this->db->SELECT('nip, nama_guru');
        $this->db->WHERE('nip =', $nip);
        $query = $this->db->get('tbl_guru');
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function getNamaguru(){
        $this->db->SELECT('nip, nama_guru');
        // $this->db->WHERE('nip =', $nip);
        $query = $this->db->get('tbl_guru');
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function getNorek($kategori){
        $this->db->SELECT('norek');
        $this->db->WHERE('kategori_rekening =', $kategori);
        $query = $this->db->get('tbl_rekening');
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function getDatasiswa($nim){
        $this->db->SELECT('nama');
        $this->db->WHERE('nim =', $nim);
        $query = $this->db->get('tbl_siswa');
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function getDatarek($norek, $kategori){
        $this->db->SELECT('nama_rek, nama_bank, nama_pemilik_rek');
        $this->db->WHERE('norek =', $norek);
        $this->db->WHERE('kategori_rekening =', $kategori);
        $query = $this->db->get('tbl_rekening');
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }
    function getNamasiswa($kelas){
        $this->db->SELECT('nim');
        $this->db->WHERE('kelas =', $kelas);
        $query = $this->db->get('tbl_siswa');
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function getJP($cashflow, $kategori){
            $this->db->SELECT('nama_jenis_pembayaran');
            $this->db->WHERE('cashflow =', $cashflow);
            $this->db->WHERE('kategori_pembayaran =', $kategori);
            $query = $this->db->get('tbl_jenis_pembayaran');
            if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function getJenisPembayaran(){
        $this->db->SELECT('nama_jenis_pembayaran');
        //$this->db->FROM('tbl_jenis_pembayaran');
        $query = $this->db->get('tbl_jenis_pembayaran');
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function getJPincome(){
        $this->db->SELECT('nama_jenis_pembayaran');
        $this->db->WHERE('cashflow = "income"');
        $query = $this->db->get('tbl_jenis_pembayaran');
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function getJPoutcome(){
        $this->db->SELECT('nama_jenis_pembayaran');
        $this->db->WHERE('cashflow = "outcome"');
        $query = $this->db->get('tbl_jenis_pembayaran');
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function getKelas(){
        $this->db->SELECT('kd_ruangan, nama_ruangan');
        $query = $this->db->get('tbl_ruangan');
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }


    function getSaldo($no_rek){
        $this->db->SELECT('saldo');
        //$this->db->FROM('tbl_rekening');
        $this->db->WHERE('norek =',$no_rek);
        $query = $this->db->get('tbl_rekening');


            if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }



    function getGuru(){
        $this->db->SELECT('nama_guru');
        $query = $this->db->get('tbl_guru');
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function getSaldolama($norek){
        $this->db->select('saldo');
        $this->db->where('norek', $norek);
        $query = $this->db->get('tbl_rekening');
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }
    function getNominallama($id_transaksiRekening){
        $this->db->select('nominal');
        $this->db->where('id_transaksiRekening', $id_transaksiRekening);
        $query = $this->db->get('tbl_rekening_yayasan');
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function getNorektrans($id_transaksiRekening){
        $this->db->select('norek_yayasan, cashflow, nominal, saldo');
        $this->db->where('id_transaksiRekening', $id_transaksiRekening);
        $query = $this->db->get('tbl_rekening_yayasan');
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function undoSaldo($undoSaldo, $norek_yayasan){
        $this->db->set("saldo", $undoSaldo);
        $this->db->WHERE("norek", $norek_yayasan);
        $this->db->update("tbl_rekening");
    }

    function hapusTransaksi($id_transaksiRekening){
        $this->db->WHERE('id_transaksiRekening', $id_transaksiRekening);
        $this->db->delete('tbl_rekening_yayasan');
    }

    function update($data, $saldoakhir) {
        $no_rek = $this->input->post('norek_yayasan',TRUE);
        $data["saldo"] = $saldoakhir;

        $data = array(
            'tgl_transaksi'                     => $this->input->post('tanggal_transaksi', TRUE),
            'norek_yayasan'                     => $this->input->post('norek_yayasan', TRUE),
            'nama_rek'                          => $this->input->post('nama_rek', TRUE),
            'bank_yayasan'                      => $this->input->post('bank_yayasan', TRUE),
            'nama_pemilik_rek_yayasan'          => $this->input->post('nama_pemilik_rek_yayasan', TRUE),
            'nama'                              => $this->input->post('nama', TRUE),
            'norek'                             => $this->input->post('norek', TRUE),
            'bank'                              => $this->input->post('bank', TRUE),
            'cashflow'                          => $this->input->post('cashflow',TRUE),
            'jenis_pembayaran'                  => $this->input->post('jenispembayaran',TRUE),
            'keterkaitan'                       => $this->input->post('keterkaitan',TRUE),
            'nama_ket'                          => $this->input->post('namaket',TRUE),
            'nominal'                           => $this->input->post('nominal', TRUE),
            'keterangan'                        => $this->input->post('keterangan', TRUE),
            'saldo'                             => $saldoakhir

        );

        // print_r($data);
        // exit;


        $id_rekening_yayasan   = $this->input->post('id_transaksiRekening');
        $this->db->where('id_transaksiRekening',$id_rekening_yayasan);
        $this->db->update($this->table,$data);
        $this->db->set('saldo',$saldoakhir);
        $this->db->where('norek',$no_rek);
        $this->db->update('tbl_rekening');
    }

    function get_export_rekyayasan(){

      $query = $this->db->get('tbl_rekening_yayasan');


          if ($query->num_rows())
          {
              return $query->result_array();
          }
          else
          {
              return FALSE;
          }
    }

    function get_export_pertransaksi($x){
        $id_transRek = $x;
        $this->db->SELECT('id_transaksiRekening, tgl_transaksi, norek_yayasan, bank_yayasan, nama_pemilik_rek_yayasan, nama, norek, bank, cashflow, jenis_pembayaran, keterkaitan, nominal, keterangan');
        $this->db->WHERE('id_transaksiRekening =', $id_transRek);
        $query = $this->db->get('tbl_rekening_yayasan');


          if ($query->num_rows())
          {
              return $query->result_array();
          }
          else
          {
              return FALSE;
          }

    }


}
