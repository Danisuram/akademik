<?php

class Model_riwayat_keuangan_guru extends CI_Model {

	public $table ="tbl_rekening_yayasan";

	function get_export_riwayatkeuanganguru(){

		$this->db->WHERE('keterkaitan = guru/karyawan');	
      $query = $this->db->get('tbl_rekening_yayasan');


          if ($query->num_rows())
          {
              return $query->result_array();
          }
          else
          {
              return FALSE;
          }
    }

}

/* End of file Model_riwayat_keuangan_guru.php */
/* Location: ./application/models/Model_riwayat_keuangan_guru.php */