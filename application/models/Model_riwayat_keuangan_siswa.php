<?php

class Model_riwayat_keuangan_siswa extends CI_Model {

	public $table ="tbl_rekening_yayasan";

	function get_export_riwayatkeuangansiswa(){

		$this->db->WHERE('keterkaitan = siswa');	
      $query = $this->db->get('tbl_rekening_yayasan');


          if ($query->num_rows())
          {
              return $query->result_array();
          }
          else
          {
              return FALSE;
          }
    }

}

/* End of file Model_riwayat_keuangan_siswa.php */
/* Location: ./application/models/Model_riwayat_keuangan_siswa.php */