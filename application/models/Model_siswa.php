<?php

class Model_siswa extends CI_Model {

    public $table ="tbl_siswa";
    
    function save($foto,$foto1) {
        $data = array(
            'no_urut'       => $this->input->post('no_urut', TRUE),
            'nim'           => $this->input->post('nim', TRUE),    
            'nama'          => $this->input->post('nama', TRUE),
            'kelas'         => $this->input->post('kelas', true),
            'gender'        => $this->input->post('gender', TRUE),
            'tempat_lahir'  => $this->input->post('tempat_lahir', TRUE),
            'tanggal_lahir' => $this->input->post('tanggal_lahir', TRUE),
            'alamat'        => $this->input->post('alamat',TRUE),
            'sekolah_asal'  => $this->input->post('sekolah_asal', true),
            'nm_ibu'        => $this->input->post('nama_ibu',true),
            'nm_bapak'      => $this->input->post('nama_bapak',TRUE),
            'pkjr_ibu'      => $this->input->post('pekerjaan_ibu',TRUE),
            'pkjr_bapak'    => $this->input->post('pekerjaan_bapak',TRUE),
            'nm_wali'       => $this->input->post('nama_wali',TRUE),
            'pkjr_wali'     => $this->input->post('pekerjaan_wali',TRUE),
            'alamat_wali'   => $this->input->post('alamat_wali',TRUE),
            'no_telp'       => $this->input->post('no_telp',TRUE),
            'no_kk'         => $this->input->post('no_kk',TRUE),
            'file_kk'       => $foto,
            'ijazah'        => $this->input->post('ijazah',TRUE),
            'file_ijazah'   => $foto1,
            'nisn'          => $this->input->post('nisn',TRUE),
            'nik'          => $this->input->post('nik',TRUE),
            'riwayat_penyakit'    => $this->input->post('riwayat_penyakit',TRUE),
            'abk'           => $this->input->post('abk',TRUE),
            'no_skhu'       => $this->input->post('no_skhu',TRUE)
        );
        $this->db->insert($this->table,$data);
        
        $tahun_akademik = $this->db->get_where('tbl_tahun_akademik',array('is_aktif'=>'y'))->row_array();
        
        $history =  array(
            'nim'                 =>  $this->input->post('nim', TRUE),
            'id_tahun_akademik'   =>  $tahun_akademik['id_tahun_akademik'],
            'id_rombel'           =>  $this->input->post('kelas', TRUE)
            );
        $this->db->insert('tbl_history_kelas',$history);
    }
    
    function update($foto, $foto1) {
            // update with foto
            $data = array(
                'nama'          => $this->input->post('nama', TRUE),
                'kelas'         => $this->input->post('kelas', true),
                'gender'        => $this->input->post('gender', TRUE),
                'tempat_lahir'  => $this->input->post('tempat_lahir', TRUE),
                'tanggal_lahir' => $this->input->post('tanggal_lahir', TRUE),
                'alamat'        => $this->input->post('alamat',TRUE),
                'sekolah_asal'  => $this->input->post('sekolah_asal', true),
                'nm_ibu'        => $this->input->post('nama_ibu',true),
                'nm_bapak'      => $this->input->post('nama_bapak',TRUE),
                'pkjr_ibu'      => $this->input->post('pekerjaan_ibu',TRUE),
                'pkjr_bapak'    => $this->input->post('pekerjaan_bapak',TRUE),
                'nm_wali'       => $this->input->post('nama_wali',TRUE),
                'pkjr_wali'     => $this->input->post('pekerjaan_wali',TRUE),
                'alamat_wali'   => $this->input->post('alamat_wali',TRUE),
                'no_telp'       => $this->input->post('no_telp',TRUE),
                'no_kk'         => $this->input->post('no_kk',TRUE),
                'file_kk'       => $foto,
                'ijazah'        => $this->input->post('ijazah',TRUE),
                'file_ijazah'   => $foto1,
                'nisn'          => $this->input->post('nisn',TRUE),
                'nik'          => $this->input->post('nik',TRUE),
                'riwayat_penyakit'    => $this->input->post('riwayat_penyakit',TRUE),
                'abk'           => $this->input->post('abk',TRUE),
                'no_skhu'       => $this->input->post('no_skhu',TRUE)
        );
        
        $nim   = $this->input->post('nim');
        $this->db->where('nim',$nim);
        $this->db->update($this->table,$data);
    }

    function get_export_siswa(){

      $query = $this->db->get('tbl_siswa');


          if ($query->num_rows())
          {
              return $query->result_array();
          }
          else
          {
              return FALSE;
          }
    }

    function get_export_persiswa($x){
        $nim = $x;

        $this->db->Select('nim, nama, kelas, gender,
                            tanggal_lahir, tempat_lahir, alamat, sekolah_asal,
                            nm_ibu, nm_bapak, pkjr_ibu, pkjr_bapak, nm_wali, pkjr_wali, alamat_wali, no_telp, no_kk, ijazah, nisn, nik, riwayat_penyakit');
        $this->db->where('nim =',$nim);
        $query = $this->db->get('tbl_siswa');
        if ($query->num_rows())
          {
              return $query->result_array();
          }
          else
          {
              return FALSE;
          }


    }

}