<?php

class Model_cashflow extends CI_Model {

    public $table ="tbl_cashflow";

    function save() {
        $data = array(
        	'no_kwitansi'   	=> $this->input->post('no_kwitansi', TRUE),
            'tgl'           	=> $this->input->post('tgl', TRUE),
            'rek_asal'      	=> $this->input->post('rek_asal', TRUE),
            'nama_rek_asal' 	=> $this->input->post('nama_rek_asal', TRUE),
            'rek_tujuan'    	=> $this->input->post('rek_tujuan', TRUE),
            'nama_rek_tujuan'	=> $this->input->post('nama_rek_tujuan', TRUE),
            'tujuan_transfer'	=> $this->input->post('tujuan_transfer', TRUE),
            'keterangan'    	=> $this->input->post('keterangan', TRUE)

        );
        $this->db->insert($this->table,$data);
    }

    function update() {
        $data = array(
            'no_kwitansi'   	=> $this->input->post('no_kwitansi', TRUE),
            'tgl'           	=> $this->input->post('tgl', TRUE),
            'rek_asal'      	=> $this->input->post('rek_asal', TRUE),
            'nama_rek_asal' 	=> $this->input->post('nama_rek_asal', TRUE),
            'rek_tujuan'    	=> $this->input->post('rek_tujuan', TRUE),
            'nama_rek_tujuan'	=> $this->input->post('nama_rek_tujuan', TRUE),
            'tujuan_transfer'	=> $this->input->post('tujuan_transfer', TRUE),
            'keterangan'    	=> $this->input->post('keterangan', TRUE)
        );
        $id_cash   = $this->input->post('id_cash');
        $this->db->where('id_cash',$id_cash);
        $this->db->update($this->table,$data);
    }

    function cashflow_gettgl($kategori){
      $this->db->where('kategori_keuangan', $kategori);
      $this->db->group_by('tgl_transaksi');
      $this->db->order_by('tgl_transaksi', 'asc');
      $query = $this->db->get('tbl_rekening_yayasan');
      if ($query->num_rows())
      {
          return $query->result_array();
      }
      else
      {
          return FALSE;
      }
    }

    function cashflow_gettgl_cash($kategori){
      $this->db->where('kategori_keuangan', $kategori);
      $this->db->group_by('tgl');
      $this->db->order_by('tgl', 'asc');
      $query = $this->db->get('tbl_cash_transaksi');
      if ($query->num_rows())
      {
          return $query->result_array();
      }
      else
      {
          return FALSE;
      }
    }

    function cashflow($tgl, $kategori){
      $this->db->select("sum(nominal) as nominal");
      $this->db->where('tgl_transaksi', $tgl);
      $this->db->where('cashflow', 'income');
      $this->db->where('kategori_keuangan', $kategori);
      $this->db->group_by('tgl_transaksi');
      $this->db->order_by('tgl_transaksi', 'asc');
      $query = $this->db->get('tbl_rekening_yayasan');
      if ($query->num_rows())
      {
          return $query->result_array();
      }
      else
      {
          return FALSE;
      }
    }

    function cashflow_cash($tgl, $kategori){
      $this->db->select("sum(nominal) as nominal");
      $this->db->where('tgl', $tgl);
      $this->db->where('cashflow', 'income');
      $this->db->where('kategori_keuangan', $kategori);
      $this->db->group_by('tgl');
      $this->db->order_by('tgl', 'asc');
      $query = $this->db->get('tbl_cash_transaksi');
      if ($query->num_rows())
      {
          return $query->result_array();
      }
      else
      {
          return FALSE;
      }
    }

    function cashflow_out($tgl, $kategori){
      $this->db->select("sum(nominal) as nominal");
      $this->db->where('tgl_transaksi', $tgl);
      $this->db->where('cashflow', 'outcome');
      $this->db->where('kategori_keuangan', $kategori);
      $this->db->group_by('tgl_transaksi');
      $this->db->order_by('tgl_transaksi', 'asc');
      $query = $this->db->get('tbl_rekening_yayasan');
      if ($query->num_rows())
      {
          return $query->result_array();
      }
      else
      {
          return FALSE;
      }
    }

    function cashflow_cash_out($tgl, $kategori){
      $this->db->select("sum(nominal) as nominal");
      $this->db->where('tgl', $tgl);
      $this->db->where('cashflow', 'outcome');
      $this->db->where('kategori_keuangan', $kategori);
      $this->db->group_by('tgl');
      $this->db->order_by('tgl', 'asc');
      $query = $this->db->get('tbl_cash_transaksi');
      if ($query->num_rows())
      {
          return $query->result_array();
      }
      else
      {
          return FALSE;
      }
    }


}
