<?php

class Model_cash_transaksi extends CI_Model {

    public $table ="tbl_cash_transaksi";

    function save($kategori) {
        $vKategori = $kategori;

        $data = array(
            'tgl'                               => $this->input->post('tanggal_transaksi', TRUE),
            'pemberi'                           => $this->input->post('pemberi', TRUE),
            'penerima'                          => $this->input->post('penerima', TRUE),
            'cashflow'                          => $this->input->post('cashflow',TRUE),
            'jenis_pembayaran'                  => $this->input->post('jenispembayaran',TRUE),
            'keterkaitan'                       => $this->input->post('keterkaitan',TRUE),
            'nama_ket'                          => $this->input->post('namaket',TRUE),
            'nominal'                           => $this->input->post('nominal', TRUE),
            'keterangan'                        => $this->input->post('keterangan', TRUE),
            'kategori_keuangan'                 => $vKategori
        );

        $this->db->insert($this->table,$data);
    }

    function getJP($cashflow, $kategori){
            $this->db->SELECT('nama_jenis_pembayaran');
            $this->db->WHERE('cashflow =', $cashflow);
            $this->db->WHERE('kategori_pembayaran =', $kategori);
            $query = $this->db->get('tbl_jenis_pembayaran');
            if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function getGuru(){
        $this->db->SELECT('nama_guru');
        $query = $this->db->get('tbl_guru');
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function getNamasiswa($kelas){
        $this->db->SELECT('nama');
        $this->db->WHERE('kelas =', $kelas);
        $query = $this->db->get('tbl_siswa');
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function getKelas(){
        $this->db->SELECT('kd_ruangan, nama_ruangan');
        $query = $this->db->get('tbl_ruangan');
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }



    function update() {
        $data = array(
            'tgl'                               => $this->input->post('tanggal_transaksi', TRUE),
            'pemberi'                           => $this->input->post('pemberi', TRUE),
            'penerima'                          => $this->input->post('penerima', TRUE),
            'cashflow'                          => $this->input->post('cashflow',TRUE),
            'jenis_pembayaran'                  => $this->input->post('jenispembayaran',TRUE),
            'keterkaitan'                       => $this->input->post('keterkaitan',TRUE),
            'nama_ket'                          => $this->input->post('nama_ket',TRUE),
            'nominal'                           => $this->input->post('nominal', TRUE),
            'keterangan'                        => $this->input->post('keterangan', TRUE),
        );
        print_r($data['nama_ket']);
        exit;
        $id_rekening_yayasan   = $this->input->post('id_ctran');
        $this->db->where('id_ctran',$id_rekening_yayasan);
        $this->db->update($this->table,$data);
    }

    function get_export_cashtransaction(){

      $query = $this->db->get('tbl_cash_transaksi');


          if ($query->num_rows())
          {
              return $query->result_array();
          }
          else
          {
              return FALSE;
          }
    }

    function get_export_pertransaksi($x){
        $id_transcash = $x;
        $this->db->SELECT('id_ctran, tgl, pemberi, penerima, cashflow, jenis_pembayaran, keterkaitan, nama_ket, nominal, keterangan');
        $this->db->WHERE('id_ctran =',$id_transcash);
        $query = $this->db->get('tbl_cash_transaksi');
        if ($query->num_rows())
          {
              return $query->result_array();
          }
          else
          {
              return FALSE;
          }
    }


}
