<?php

class Model_keuangan_guru extends CI_Model {

    public $table ="tbl_keu_guru";
    
    function save() {
        $data = array(
            'nip'           => $this->input->post('nip', TRUE),
            'tgl_pembayaran'=> $this->input->post('tgl_pembayaran', TRUE),
            'jumlah'        => $this->input->post('jumlah', TRUE),
            'pembayaran'    => $this->input->post('pembayaran', TRUE),
            'rek_asal'      => $this->input->post('rek_asal', TRUE),
            'rek_tujuan'    => $this->input->post('rek_tujuan', TRUE)
            
        );
        $this->db->insert($this->table,$data);
    }
    
    function update() {
        $data = array(
           'nip'           => $this->input->post('nip', TRUE),
            'tgl_pembayaran'=> $this->input->post('tgl_pembayaran', TRUE),
            'jumlah'        => $this->input->post('jumlah', TRUE),
            'pembayaran'    => $this->input->post('pembayaran', TRUE),
            'rek_asal'      => $this->input->post('rek_asal', TRUE),
            'rek_tujuan'    => $this->input->post('rek_tujuan', TRUE)
        );
        $id_tran   = $this->input->post('id_tran');
        $this->db->where('id_tran',$id_tran);
        $this->db->update($this->table,$data);
    }
    
    

    
}