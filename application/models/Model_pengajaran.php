<?php

class Model_pengajaran extends CI_Model {
	
	public $table = "tbl_pengajaran";

	function add(){

		$vnip					= $this->input->post('nip',TRUE);
		$vkd_ruangan			= $this->input->post('kd_kelas',TRUE);
		$vkd_mapel				= $this->input->post('kd_mapel', TRUE);

		$data = array(
			'nip_kdruangan_kdmapel'	=> $vnip.$vkd_ruangan.$vkd_mapel,
			'nip'					=> $this->input->post('nip',TRUE),
			'nama_guru'				=> $this->input->post('nama_guru',TRUE),
			'kd_ruangan'			=> $this->input->post('kd_kelas',TRUE),
			'nama_ruangan'			=> $this->input->post('kelas',TRUE), 
			'kd_mapel'				=> $this->input->post('kd_mapel', TRUE),
			'nama_mapel'			=> $this->input->post('mapel',TRUE)
		);

		


		$this->db->insert($this->table, $data);

	}



	 function getNip(){
        $this->db->SELECT('nip');
        $query = $this->db->get('tbl_guru');
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function getNamaguru($nip){
    	$this->db->SELECT('nama_guru');
    	$this->db->WHERE('nip =', $nip);
        $query = $this->db->get('tbl_guru');
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function getKelas(){
    	$this->db->SELECT('kd_ruangan');
    	$query = $this->db->get('tbl_ruangan');
    	if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function getRuangkelas($kd_kelas){
    	$this->db->SELECT('nama_ruangan');
    	$this->db->WHERE('kd_ruangan =', $kd_kelas);
    	$query = $this->db->get('tbl_ruangan');
    	if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function getMapel(){
    	$this->db->SELECT('kd_mapel');
    	$query = $this->db->get('tbl_mapel');
    	if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function getMatapelajaran($kd_mapel){
    	$this->db->SELECT('nama_mapel');
    	$this->db->WHERE('kd_mapel =', $kd_mapel);
    	$query = $this->db->get('tbl_mapel');
    	if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function get_export_pengajaran(){
    	$this->db->SELECT('nip, nama_guru, kd_ruangan, nama_ruangan, kd_mapel, nama_mapel');
    	$query = $this->db->get('tbl_pengajaran');
    	if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }
    

}

/* End of file Model_pengajaran.php */
/* Location: ./application/models/Model_pengajaran.php */