<?php

class Model_guru extends CI_Model {

    public $table ="tbl_guru";
    
    function save($foto,$foto1,$foto2,$foto3,$foto4,$foto5,$foto6,$foto7) {
        $data = array(
            'no_urut'       => $this->input->post('no_urut', TRUE),
            'nip'           => $this->input->post('nip', TRUE),
            'nama_guru'     => $this->input->post('nama_guru', TRUE),
            'kelas'         => $this->input->post('kelas', TRUE),
            'mata_pelajaran'=> $this->input->post('matpel', TRUE),
            'kelamin'       => $this->input->post('kelamin', TRUE),
            'tempat_lahir'  => $this->input->post('tempat_lahir', TRUE),
            'tgl_lahir'     => $this->input->post('tanggal_lahir', TRUE),
            'alamat'        => $this->input->post('alamat', TRUE),
            'no_telp'       => $this->input->post('no_telp', TRUE),
            'no_kk'         => $this->input->post('no_kk', TRUE),
            'file_kk'       => $foto,
            'no_ktp'        => $this->input->post('no_ktp', TRUE),
            'file_ktp'      => $foto1,
            'ijazah_sd'     => $foto2,
            'ijazah_smp'    => $foto3,
            'ijazah_sma'    => $foto4,
            'ijazah_s1'     => $foto5,
            'ijazah_s2'     => $foto6,
            'ijazah_s3'     => $foto7,
            'nisn'          => $this->input->post('nisn', TRUE),
            'nik'           => $this->input->post('nik',TRUE),
            'riwayat_penyakit'   => $this->input->post('riwayat_penyakit', TRUE)
        );
        $this->db->insert($this->table,$data);
    }
    
    function update($foto,$foto1,$foto2,$foto3,$foto4,$foto5,$foto6,$foto7) {
        $data = array(
            'nama_guru'     => $this->input->post('nama_guru', TRUE),
            'kelas'         => $this->input->post('kelas', TRUE),
            'mata_pelajaran'=> $this->input->post('matpel', TRUE),
            'kelamin'       => $this->input->post('kelamin', TRUE),
            'tempat_lahir'  => $this->input->post('tempat_lahir', TRUE),
            'tgl_lahir'     => $this->input->post('tanggal_lahir', TRUE),
            'alamat'        => $this->input->post('alamat', TRUE),
            'no_telp'       => $this->input->post('no_telp', TRUE),
            'no_kk'         => $this->input->post('no_kk', TRUE),
            'file_kk'       => $foto,
            'no_ktp'        => $this->input->post('no_ktp', TRUE),
            'file_ktp'      => $foto1,
            'ijazah_sd'     => $foto2,
            'ijazah_smp'    => $foto3,
            'ijazah_sma'    => $foto4,
            'ijazah_s1'     => $foto5,
            'ijazah_s2'     => $foto6,
            'ijazah_s3'     => $foto7,
            'nisn'          => $this->input->post('nisn', TRUE),
            'nik'           => $this->input->post('nik',TRUE),
            'riwayat_penyakit'   => $this->input->post('riwayat_penyakit', TRUE)
        );
        $id_guru   = $this->input->post('nip');
        $this->db->where('nip',$id_guru);
        $this->db->update($this->table,$data);
    }
    
    function chekLogin($username,$password){
        $this->db->where('username',$username);
        $this->db->where('password',  md5($password));
        $user = $this->db->get('tbl_user')->row_array();
        return $user;
    }

    function getMapel(){
        $this->db->SELECT('nama_mapel');
        // $this->db->WHERE('nip =', $nip);
        $query = $this->db->get('tbl_mapel');
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function get_export_guru(){

      $query = $this->db->get('tbl_guru');


          if ($query->num_rows())
          {
              return $query->result_array();
          }
          else
          {
              return FALSE;
          }
    }

    function get_export_perguru($x){
        $nip = $x;

        $this->db->Select('nip, nama_guru, kelas, kelamin,
                            tempat_lahir, tgl_lahir, alamat, no_telp,
                            no_kk, no_ktp, nisn, nik, riwayat_penyakit');
        $this->db->where('nip =',$nip);
        $query = $this->db->get('tbl_guru');
        if ($query->num_rows())
          {
              return $query->result_array();
          }
          else
          {
              return FALSE;
          }


    }

    
}