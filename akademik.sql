﻿# Host: localhost  (Version 5.5.5-10.1.37-MariaDB)
# Date: 2019-02-14 23:32:04
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "tabel_menu"
#

DROP TABLE IF EXISTS `tabel_menu`;
CREATE TABLE `tabel_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_menu` varchar(50) NOT NULL,
  `link` varchar(50) NOT NULL,
  `icon` varchar(30) NOT NULL,
  `is_main_menu` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

#
# Data for table "tabel_menu"
#

INSERT INTO `tabel_menu` VALUES (1,'DATAbase SISWA','siswa','fa fa-users',0),(2,'DATAbase GURU','guru','fa fa-graduation-cap',0),(3,'keuangan_guru','keuangan_guru','fa fa-shopping-cart',0),(8,'data sekolah','sekolah','fa fa-building',0),(9,'Data master','#','fa fa-bars',0),(10,'Mata Pelajaran','mapel','fa fa-book',9),(11,'Ruangan Kelas','ruangan','fa fa-building',9),(12,'Jurusan','jurusan','fa fa-th-large',9),(13,'Tahun Akademik','tahunakademik','fa fa-calendar-o',9),(14,'Jadwal pelajaran','jadwal','fa fa-calendar',0),(15,'Rombongan Belajar','rombel','fa fa-users',9),(16,'laporan nilai','nilai','fa fa-file-excel-o',0),(17,'Pengguna sistem','users','fa fa-cubes',0),(19,'Kurikulum','kurikulum','fa fa-newspaper-o',9),(20,'Wali Kelas','walikelas','fa fa-users',0),(21,'form pembayaran','keuangan/form','fa fa-shopping-cart',0),(22,'Peserta Didik','siswa/siswa_aktif','fa fa-graduation-cap',0),(23,'jenis pembayaran','jenis_pembayaran','fa fa-credit-card',0),(24,'setup biaya','keuangan/setup','fa fa-graduation-cap',0),(25,'Raport Online','raport','fa fa-graduation-cap',0),(26,'SMS GATEWAY','sms','fa fa-envelope-o',0),(27,'phonebook','sms_group','fa fa-book',26),(28,'form sms','sms','fa fa-keyboard-o',26),(29,'Rekening Transaksi','rekening_yayasan','fa fa-paperclip',0),(30,'Cash Transaksi','cash_transaksi','fa fa-calculator',0),(31,'Cashflow','cashflow','fa fa-bar-chart',0),(32,'Rekening','Rekening','',9),(33,'Rekening','Rekening','',0),(34,'Phonebook','sms_group','fa fa-book',0);

#
# Structure for table "tbl_agama"
#

DROP TABLE IF EXISTS `tbl_agama`;
CREATE TABLE `tbl_agama` (
  `kd_agama` varchar(2) NOT NULL,
  `nama_agama` varchar(30) NOT NULL,
  PRIMARY KEY (`kd_agama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "tbl_agama"
#

INSERT INTO `tbl_agama` VALUES ('01','ISLAM'),('02','KRISTEN/ PROTESTAN'),('03','KATHOLIK'),('04','HINDU'),('05','BUDHA'),('06','KHONG HU CHU'),('99','LAIN LAIN');

#
# Structure for table "tbl_biaya_sekolah"
#

DROP TABLE IF EXISTS `tbl_biaya_sekolah`;
CREATE TABLE `tbl_biaya_sekolah` (
  `id_biaya` int(11) NOT NULL AUTO_INCREMENT,
  `id_jenis_pembayaran` int(11) NOT NULL,
  `id_tahun_akademik` int(11) NOT NULL,
  `jumlah_biaya` int(11) NOT NULL,
  PRIMARY KEY (`id_biaya`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

#
# Data for table "tbl_biaya_sekolah"
#

INSERT INTO `tbl_biaya_sekolah` VALUES (3,1,1,600000),(4,2,1,900000);

#
# Structure for table "tbl_cash_transaksi"
#

DROP TABLE IF EXISTS `tbl_cash_transaksi`;
CREATE TABLE `tbl_cash_transaksi` (
  `id_ctran` int(11) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `pemberi` varchar(300) DEFAULT NULL,
  `penerima` varchar(300) DEFAULT NULL,
  `norek` varchar(60) DEFAULT NULL,
  `keperluan` varchar(300) DEFAULT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=ascii;

#
# Data for table "tbl_cash_transaksi"
#

INSERT INTO `tbl_cash_transaksi` VALUES (1,NULL,'anu','ke anu','898989','perlu','macam-macam yang ada'),(2,'2003-12-12','percobaab','coba saja lagi','9999','ada aja','makan-makan lagi ');

#
# Structure for table "tbl_cashflow"
#

DROP TABLE IF EXISTS `tbl_cashflow`;
CREATE TABLE `tbl_cashflow` (
  `id_cash` int(11) DEFAULT NULL,
  `no_kwitansi` varchar(150) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `rek_asal` varchar(60) DEFAULT NULL,
  `nama_rek_asal` varchar(300) DEFAULT NULL,
  `rek_tujuan` varchar(60) DEFAULT NULL,
  `nama_rek_tujuan` varchar(300) DEFAULT NULL,
  `tujuan_transfer` varchar(300) DEFAULT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=ascii;

#
# Data for table "tbl_cashflow"
#

INSERT INTO `tbl_cashflow` VALUES (1,'1234448888','2018-05-01','877878787',NULL,'79877987','anak baru','beli susu','beli macem2'),(2,'3123','0212-02-11','121','ghjffghjdsgfdfsgsadf','12312','123123','123123',' sfwerwe3rw'),(3,'asdasd','0002-12-11','asdasds','hgdrjtr6tymfgjnd','kljlj','kjkhk','8977','lkjlkjlkjlk '),(4,'56','1221-12-12','5656','i;tuyrertwtjyrfadfssgdf','565656','tert tujuan','gak tau','test lagi ');

#
# Structure for table "tbl_guru"
#

DROP TABLE IF EXISTS `tbl_guru`;
CREATE TABLE `tbl_guru` (
  `no_urut` int(11) DEFAULT NULL,
  `nip` varchar(48) DEFAULT NULL,
  `nama_guru` varchar(90) DEFAULT NULL,
  `kelas` varchar(24) DEFAULT NULL,
  `kelamin` char(3) DEFAULT NULL,
  `tempat_lahir` varchar(150) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `alamat` text,
  `no_telp` varchar(60) DEFAULT NULL,
  `no_kk` varchar(60) DEFAULT NULL,
  `file_kk` varchar(450) DEFAULT NULL,
  `no_ktp` varchar(60) DEFAULT NULL,
  `file_ktp` varchar(450) DEFAULT NULL,
  `ijazah_sd` varchar(450) DEFAULT NULL,
  `ijazah_smp` varchar(450) DEFAULT NULL,
  `ijazah_sma` varchar(450) DEFAULT NULL,
  `ijazah_s1` varchar(450) DEFAULT NULL,
  `ijazah_s2` varchar(450) DEFAULT NULL,
  `ijazah_s3` varchar(450) DEFAULT NULL,
  `nisn` varchar(60) DEFAULT NULL,
  `riwayat_penyakit` text
) ENGINE=InnoDB DEFAULT CHARSET=ascii;

#
# Data for table "tbl_guru"
#

INSERT INTO `tbl_guru` VALUES (3,'4343434434343434','irma muliana sst mpd',NULL,'w',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'46676768686','nuris akbar sst',NULL,'p',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1,'8728372382738273','drs diawan sst','011','p','asfas','2018-01-01','j. sembako','687786','87767','1f2074b7d56f7065ed2480f7ec67453d.jpg','13221312','4c9a0e12725e54a5ce1fdbf2c374f0a6.jpg','d6c6a2e1a379347bff5b5060b7f613f0.jpg','c3369f2ee8c1661749d70d90075bf6e6.jpg','da60f42793fe6feb6843d03665b6df9c.jpg','4be79aac187fc938357d3a5fbf3954cb.jpg','10dc3c77cca171c90627e1240dc1b8b2.jpg','46891d838d150bc8eb23eeab9bb451b1.jpg','adsad','adasdd'),(20,'100001819','Ole Gunnar Solkjaer','01A','P','Carrington','1999-06-02','Tangerang','555202897','100113201819','3bde25fc54fe0e03ae4dcc01e4f7a415.png','2020202020','a5ed4b1c8a6409785134501f4446956e.jpg',NULL,NULL,NULL,NULL,NULL,NULL,'121212','Bengek');

#
# Structure for table "tbl_history_kelas"
#

DROP TABLE IF EXISTS `tbl_history_kelas`;
CREATE TABLE `tbl_history_kelas` (
  `id_history` int(11) NOT NULL AUTO_INCREMENT,
  `id_rombel` int(11) NOT NULL,
  `nim` varchar(11) NOT NULL,
  `id_tahun_akademik` int(11) NOT NULL,
  PRIMARY KEY (`id_history`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

#
# Data for table "tbl_history_kelas"
#

INSERT INTO `tbl_history_kelas` VALUES (1,1,'TI3003239',1),(2,1,'RM00502',1),(3,1,'TI102132',1),(4,1,'TI102133',1),(5,1,'TIM102134',1),(6,1,'TIM102135',1),(7,1,'TI1021395',1),(8,3,'RM00509',1),(9,3,'142123123',1),(10,1,'1234567',1),(11,1,'1411501784',1);

#
# Structure for table "tbl_jadwal"
#

DROP TABLE IF EXISTS `tbl_jadwal`;
CREATE TABLE `tbl_jadwal` (
  `id_jadwal` int(11) NOT NULL AUTO_INCREMENT,
  `id_tahun_akademik` int(11) NOT NULL,
  `kd_jurusan` varchar(6) NOT NULL,
  `kelas` int(11) NOT NULL,
  `kd_mapel` varchar(4) NOT NULL,
  `id_guru` int(11) NOT NULL,
  `jam` varchar(14) NOT NULL,
  `kd_ruangan` varchar(4) NOT NULL,
  `semester` int(11) NOT NULL,
  `hari` varchar(10) NOT NULL,
  `id_rombel` int(11) NOT NULL,
  PRIMARY KEY (`id_jadwal`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

#
# Data for table "tbl_jadwal"
#

INSERT INTO `tbl_jadwal` VALUES (13,1,'RPL',1,'MTK',4,'08.00 - 08.45','01A',1,'SELASA',1),(14,1,'RPL',1,'MTK',2,'','01B',1,'',2),(15,1,'RPL',1,'BID',2,'09.30 - 10.00','01A',1,'RABU',1),(16,1,'RPL',1,'BID',2,'','011',1,'',2),(17,1,'RPL',1,'IPA',4,'10.00 - 10.45','01B',1,'JUMAT',1),(18,1,'RPL',1,'IPA',2,'','011',1,'',2),(19,1,'RPL',1,'MTK',2,'','011',1,'',1),(20,1,'RPL',1,'MTK',2,'','011',1,'',2),(21,1,'RPL',1,'BID',2,'','011',1,'',1),(22,1,'RPL',1,'BID',2,'','011',1,'',2),(23,1,'RPL',1,'IPA',2,'','011',1,'',1),(24,1,'RPL',1,'IPA',2,'','011',1,'',2),(25,1,'RPL',1,'MTK',2,'','011',2,'',1),(26,1,'RPL',1,'MTK',2,'','011',2,'',2),(27,1,'RPL',1,'BID',2,'','011',2,'',1),(28,1,'RPL',1,'BID',2,'','011',2,'',2),(29,1,'RPL',1,'IPA',2,'','011',2,'',1),(30,1,'RPL',1,'IPA',2,'','011',2,'',2);

#
# Structure for table "tbl_jenis_pembayaran"
#

DROP TABLE IF EXISTS `tbl_jenis_pembayaran`;
CREATE TABLE `tbl_jenis_pembayaran` (
  `id_jenis_pembayaran` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis_pembayaran` varchar(50) NOT NULL,
  PRIMARY KEY (`id_jenis_pembayaran`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

#
# Data for table "tbl_jenis_pembayaran"
#

INSERT INTO `tbl_jenis_pembayaran` VALUES (1,'spp'),(2,'dsp'),(3,'makan');

#
# Structure for table "tbl_jenjang_sekolah"
#

DROP TABLE IF EXISTS `tbl_jenjang_sekolah`;
CREATE TABLE `tbl_jenjang_sekolah` (
  `id_jenjang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenjang` varchar(10) NOT NULL,
  `jumlah_kelas` int(11) NOT NULL,
  PRIMARY KEY (`id_jenjang`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

#
# Data for table "tbl_jenjang_sekolah"
#

INSERT INTO `tbl_jenjang_sekolah` VALUES (1,'SD/ MI',6),(2,'SMP/ MTS',3),(3,'SMA/ SMK',3);

#
# Structure for table "tbl_jurusan"
#

DROP TABLE IF EXISTS `tbl_jurusan`;
CREATE TABLE `tbl_jurusan` (
  `kd_jurusan` varchar(4) NOT NULL,
  `nama_jurusan` varchar(30) NOT NULL,
  PRIMARY KEY (`kd_jurusan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "tbl_jurusan"
#

INSERT INTO `tbl_jurusan` VALUES ('RPL','REKAYASA PERANGKAT LUNAK'),('TKJ','TEKNIK KOMPUTER JARINGAN');

#
# Structure for table "tbl_keu_guru"
#

DROP TABLE IF EXISTS `tbl_keu_guru`;
CREATE TABLE `tbl_keu_guru` (
  `id_tran` int(11) DEFAULT NULL,
  `nip` varchar(48) DEFAULT NULL,
  `tgl_pembayaran` date DEFAULT NULL,
  `jumlah` decimal(15,0) DEFAULT NULL,
  `pembayaran` varchar(60) DEFAULT NULL,
  `rek_asal` varchar(90) DEFAULT NULL,
  `rek_tujuan` varchar(90) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ascii;

#
# Data for table "tbl_keu_guru"
#

INSERT INTO `tbl_keu_guru` VALUES (3,'34234','2018-05-16',34324,'234324','2343','234324');

#
# Structure for table "tbl_kurikulum"
#

DROP TABLE IF EXISTS `tbl_kurikulum`;
CREATE TABLE `tbl_kurikulum` (
  `id_kurikulum` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kurikulum` varchar(30) NOT NULL,
  `is_aktif` enum('y','n') NOT NULL,
  PRIMARY KEY (`id_kurikulum`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

#
# Data for table "tbl_kurikulum"
#

INSERT INTO `tbl_kurikulum` VALUES (1,'KURIKULUM 2016','y'),(2,'KURIKULUM 2013','n');

#
# Structure for table "tbl_kurikulum_detail"
#

DROP TABLE IF EXISTS `tbl_kurikulum_detail`;
CREATE TABLE `tbl_kurikulum_detail` (
  `id_kurikulum_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_kurikulum` int(11) NOT NULL,
  `kd_mapel` varchar(11) NOT NULL,
  `kd_jurusan` varchar(4) NOT NULL,
  `kelas` int(11) NOT NULL,
  PRIMARY KEY (`id_kurikulum_detail`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

#
# Data for table "tbl_kurikulum_detail"
#

INSERT INTO `tbl_kurikulum_detail` VALUES (9,1,'MTK','RPL',1),(10,1,'BID','RPL',1),(12,1,'IPA','RPL',1);

#
# Structure for table "tbl_level_user"
#

DROP TABLE IF EXISTS `tbl_level_user`;
CREATE TABLE `tbl_level_user` (
  `id_level_user` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(30) NOT NULL,
  PRIMARY KEY (`id_level_user`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

#
# Data for table "tbl_level_user"
#

INSERT INTO `tbl_level_user` VALUES (1,'Admin'),(2,'Walikelas'),(3,'Guru'),(5,'Keuangan');

#
# Structure for table "tbl_mapel"
#

DROP TABLE IF EXISTS `tbl_mapel`;
CREATE TABLE `tbl_mapel` (
  `kd_mapel` varchar(4) NOT NULL,
  `nama_mapel` varchar(30) NOT NULL,
  PRIMARY KEY (`kd_mapel`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "tbl_mapel"
#

INSERT INTO `tbl_mapel` VALUES ('BID','BAHASA INDONESIA'),('IPA','ILMU PENGETAHUAN ALAM'),('IPS','ILMU PENGETAHUAN SOSIAL'),('MTK','MATEMATIKA'),('TIK','TEKNOLOGI INFORMASI KOMPUTER');

#
# Structure for table "tbl_nilai"
#

DROP TABLE IF EXISTS `tbl_nilai`;
CREATE TABLE `tbl_nilai` (
  `id_nilai` int(11) NOT NULL AUTO_INCREMENT,
  `id_jadwal` int(11) NOT NULL,
  `nim` varchar(15) NOT NULL,
  `nilai` int(11) NOT NULL,
  PRIMARY KEY (`id_nilai`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

#
# Data for table "tbl_nilai"
#

INSERT INTO `tbl_nilai` VALUES (1,13,'TI3003239',65),(2,13,'RM00502',89),(3,13,'TI102132',89),(4,13,'TI102133',78),(5,13,'TIM102134',67),(6,13,'TIM102135',98),(7,13,'TI1021395',60),(8,17,'TI3003239',90),(9,17,'RM00502',87),(10,17,'TI102132',89),(11,17,'TI102133',99),(12,17,'TIM102134',90),(13,17,'TIM102135',86),(14,17,'TI1021395',89);

#
# Structure for table "tbl_phonebook"
#

DROP TABLE IF EXISTS `tbl_phonebook`;
CREATE TABLE `tbl_phonebook` (
  `id_phonebook` int(11) NOT NULL AUTO_INCREMENT,
  `id_group` int(11) NOT NULL,
  `no_hp` varchar(13) NOT NULL,
  PRIMARY KEY (`id_phonebook`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

#
# Data for table "tbl_phonebook"
#

INSERT INTO `tbl_phonebook` VALUES (1,7,'089699935552'),(2,7,'085310204081');

#
# Structure for table "tbl_rekening"
#

DROP TABLE IF EXISTS `tbl_rekening`;
CREATE TABLE `tbl_rekening` (
  `Id` int(11) NOT NULL,
  `norek` varchar(60) DEFAULT NULL,
  `saldo` decimal(15,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "tbl_rekening"
#

INSERT INTO `tbl_rekening` VALUES (1,'9956999901',45740000000.00),(3,'10034445679',4884000000.00),(2,'10034445677',10000000000.00),(4,'446784593',1500000000.00);

#
# Structure for table "tbl_rekening_yayasan"
#

DROP TABLE IF EXISTS `tbl_rekening_yayasan`;
CREATE TABLE `tbl_rekening_yayasan` (
  `id_rek` int(11) DEFAULT NULL,
  `norek_tujuan` varchar(60) DEFAULT NULL,
  `bank_tujuan` varchar(300) DEFAULT NULL,
  `nama` varchar(300) DEFAULT NULL,
  `norek_asal` varchar(60) DEFAULT NULL,
  `bank_asal` varchar(300) DEFAULT NULL,
  `nominal` decimal(15,0) DEFAULT NULL,
  `peruntukan` varchar(60) DEFAULT NULL,
  `saldo` decimal(15,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ascii;

#
# Data for table "tbl_rekening_yayasan"
#

INSERT INTO `tbl_rekening_yayasan` VALUES (1,'9956999901','Mandiri','Arsene Wenger','10041234400','Mandiri',50000000,'Transfer Pemain',45950000000),(1,'9956999901','Mandiri','Alexis Sanchez','10041234400','Mandiri',40000000,'Transfer Pemain',45960000000),(1,'9956999901','Mandiri','Alexis Sanchez','10041234400','Mandiri',40000000,'Transfer Pemain',45960000000),(1,'9956999901','Mandiri','Arsene Wenger','10041234400','BCA',220000000,'Transfer Pemain',45740000000),(3,'10034445679','Mandiri','Alessandro Del Piero','10012452013','BCA',18000000,'Transfer Pemain',4982000000),(3,'10034445679','Mandiri','Dasura','445643788','BNI',98000000,'Transfer Pemain',4884000000);

#
# Structure for table "tbl_rombel"
#

DROP TABLE IF EXISTS `tbl_rombel`;
CREATE TABLE `tbl_rombel` (
  `id_rombel` int(11) NOT NULL AUTO_INCREMENT,
  `nama_rombel` varchar(30) NOT NULL,
  `kelas` int(11) NOT NULL,
  `kd_jurusan` varchar(4) NOT NULL,
  PRIMARY KEY (`id_rombel`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

#
# Data for table "tbl_rombel"
#

INSERT INTO `tbl_rombel` VALUES (1,'RPL1A',1,'RPL'),(2,'RPL1B',1,'RPL'),(3,'RPL2A',3,'RPL'),(4,'RPL2B',2,'RPL');

#
# Structure for table "tbl_ruangan"
#

DROP TABLE IF EXISTS `tbl_ruangan`;
CREATE TABLE `tbl_ruangan` (
  `kd_ruangan` varchar(4) NOT NULL,
  `nama_ruangan` varchar(30) NOT NULL,
  PRIMARY KEY (`kd_ruangan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "tbl_ruangan"
#

INSERT INTO `tbl_ruangan` VALUES ('01A','RUANGAN KELAS 1 A'),('01B','RUANGAN KELAS 1B'),('01C','RUANGAN KELAS 1 C');

#
# Structure for table "tbl_sekolah_info"
#

DROP TABLE IF EXISTS `tbl_sekolah_info`;
CREATE TABLE `tbl_sekolah_info` (
  `id_sekolah` int(11) NOT NULL,
  `nama_sekolah` varchar(30) NOT NULL,
  `id_jenjang_sekolah` int(11) NOT NULL,
  `alamat_sekolah` text NOT NULL,
  `email` varchar(30) NOT NULL,
  `telpon` varchar(12) NOT NULL,
  PRIMARY KEY (`id_sekolah`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "tbl_sekolah_info"
#

INSERT INTO `tbl_sekolah_info` VALUES (1,'SMK N 2 LANGSA',3,'JL AHMAD YANI NO 2, DESA PAYA BUJOK SELEUMAK, KOTA LANGSA - ACEH','smkn2langsa@sch.id','02134235');

#
# Structure for table "tbl_siswa"
#

DROP TABLE IF EXISTS `tbl_siswa`;
CREATE TABLE `tbl_siswa` (
  `no_urut` int(8) DEFAULT NULL,
  `nim` varchar(33) DEFAULT NULL,
  `nama` varchar(120) DEFAULT NULL,
  `kelas` int(11) DEFAULT NULL,
  `gender` char(3) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `tempat_lahir` varchar(90) DEFAULT NULL,
  `alamat` text,
  `sekolah_asal` text,
  `nm_ibu` varchar(120) DEFAULT NULL,
  `nm_bapak` varchar(120) DEFAULT NULL,
  `pkjr_ibu` varchar(60) DEFAULT NULL,
  `pkjr_bapak` varchar(60) DEFAULT NULL,
  `nm_wali` varchar(120) DEFAULT NULL,
  `pkjr_wali` varchar(60) DEFAULT NULL,
  `alamat_wali` text,
  `no_telp` varchar(45) DEFAULT NULL,
  `no_kk` varchar(60) DEFAULT NULL,
  `file_kk` varchar(450) DEFAULT NULL,
  `ijazah` varchar(60) DEFAULT NULL,
  `file_ijazah` varchar(450) DEFAULT NULL,
  `nisn` varchar(60) DEFAULT NULL,
  `riwayat_penyakit` text,
  `abk` varchar(60) DEFAULT NULL,
  `no_skhu` varchar(60) DEFAULT NULL,
  `kd_agama` varchar(6) DEFAULT NULL,
  `foto` text,
  `id_rombel` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ascii;

#
# Data for table "tbl_siswa"
#

INSERT INTO `tbl_siswa` VALUES (21312,'12312','123',1,'P','2000-12-31','Pekanbaru','asfasf','jkh','yiuygv','gfg','eqawewqdwqeqweqwe','gu','vuy','bgj','ug','buyg','buygj','5856b508e59aae9b0586a3929c5ec839.png','jvuuyb','42f16f5ad2636979fc3346cfeda66e95.png','jhvuyu','jgiu','guyu','yugyu','01','',1),(1,'2321','21312',1,'P','1212-12-12','12','123','213','123','123','13','123','','','','','','02781ff5ea0f8bb373fc921c25f7f3a0.png','','8cbb5523f576d2e94e93a308ea74f9fe.png','','','','','','',0),(22222,'7876786','test',11,'P','2016-12-30','asdasdad','test','asdas','fasfasfa','asfas','asfasf','asfsaf','afasf','afsa','asfasf','afasfaf','123213','0bd590d4eca2db8f11601087186667d3.jpg','23123','fe1696d25fe5aac0c40a8ad66273db81.jpg','23123','123213','1232','1231','','',0),(99999999,'878786787','test nama',11,'P','1992-08-09','khjkjhjhjkh','kemuning','hkhkjh','hiuhkjhkj','hkjhkj','jkhkj','hkjh','kjhk','jhkj','hkjh','kjiyuuih','yiuui','','iuyiu',NULL,'yufkjb','uigjkb','jh','kjbkj','','',0),(1,'898988989','anu',NULL,'P','2018-04-13','khjkjhjhjkh','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'03','Multi-Channel-Banking-An-Overview.png',2),(2,'RM00502','SAFIKAH KAMAL',NULL,'P','2017-01-23','BANDA ACEH','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'02','',1),(3,'TI102132','NURIS AKBAR',NULL,'P','2017-01-22','LANGSA','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'01','',1),(5,'TI1021395','BALQIS HUMAIRA',NULL,'W','2017-01-11','KUALA SIMPANG','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'01','',1),(6,'TI3003239','JONO',NULL,'P','2017-02-18','BANDUNG','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'01','Yaya_yah10.png',1),(7,'TIM102134','DESI HANDAYANI',NULL,'W','2017-01-22','RANGKASBITUNG','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'01','',1),(11,'1411501784','Dasura',1,'P','1990-10-01','England','Belgia','Atm Madrid','Derpina','Derp','IRT','Kerja','Herp','Pengangguran','Los Angles','0899999999','753913','9ae5ff0f288ff68383ef1036b83f662a.jpg','14045','9ae5ff0f288ff68383ef1036b83f662a.jpg','141150001','Bengek','10','12231213',NULL,NULL,NULL),(20,NULL,NULL,1,'P',NULL,'Molde','Manchester',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'08999999','20202020202','5835b5317a7878bfef0f7049a8423b22.png',NULL,NULL,'121212','121223123',NULL,NULL,NULL,NULL,NULL);

#
# Structure for table "tbl_sms_group"
#

DROP TABLE IF EXISTS `tbl_sms_group`;
CREATE TABLE `tbl_sms_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_group` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

#
# Data for table "tbl_sms_group"
#

INSERT INTO `tbl_sms_group` VALUES (1,'group 1'),(2,'group 2'),(4,'asasas'),(5,'testing'),(7,'walimurid');

#
# Structure for table "tbl_tahun_akademik"
#

DROP TABLE IF EXISTS `tbl_tahun_akademik`;
CREATE TABLE `tbl_tahun_akademik` (
  `id_tahun_akademik` int(4) NOT NULL AUTO_INCREMENT,
  `tahun_akademik` varchar(10) NOT NULL,
  `is_aktif` enum('y','n') NOT NULL,
  `semester_aktif` int(11) NOT NULL,
  PRIMARY KEY (`id_tahun_akademik`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

#
# Data for table "tbl_tahun_akademik"
#

INSERT INTO `tbl_tahun_akademik` VALUES (1,'2016/ 2017','y',1),(2,'2015/2016','n',0),(6,'2017/2018','n',0);

#
# Structure for table "tbl_user"
#

DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(50) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(32) NOT NULL,
  `id_level_user` int(11) NOT NULL,
  `foto` text NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

#
# Data for table "tbl_user"
#

INSERT INTO `tbl_user` VALUES (1,'nuris akbar','nuris123','85a3281bee28b39d2c0cb14ff86a55cd',1,'dsdsdsds'),(2,'HAFIDZ MUZAKI','zaki','85a3281bee28b39d2c0cb14ff86a55cd',1,'Angin.jpeg'),(3,'David De Gea','degea123','85a3281bee28b39d2c0cb14ff86a55cd',3,''),(4,'solkjaer','solkjaer','85a3281bee28b39d2c0cb14ff86a55cd',2,''),(5,'fang sui','fang','85a3281bee28b39d2c0cb14ff86a55cd',1,'Gopal_Render.png'),(7,'desi handayani','desi123','14ddc434109d6e8df730d4ea4eefc06c',5,'Yaya_yah1.png');

#
# Structure for table "tbl_user_rule"
#

DROP TABLE IF EXISTS `tbl_user_rule`;
CREATE TABLE `tbl_user_rule` (
  `id_rule` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(11) NOT NULL,
  `id_level_user` int(11) NOT NULL,
  PRIMARY KEY (`id_rule`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

#
# Data for table "tbl_user_rule"
#

INSERT INTO `tbl_user_rule` VALUES (3,1,1),(4,2,1),(5,8,1),(6,14,2),(7,1,2),(8,16,3),(10,21,5),(11,9,1),(12,10,1),(13,11,1),(14,12,1),(15,13,1),(16,14,1),(17,17,1),(18,19,1),(19,20,1),(20,14,3),(25,22,1),(26,23,5),(27,24,5),(28,25,3),(29,26,1),(30,26,5),(31,0,1),(32,29,1),(33,30,1),(34,31,1),(35,29,5),(36,33,5),(37,30,5),(38,31,5),(39,34,5);

#
# Structure for table "tbl_walikelas"
#

DROP TABLE IF EXISTS `tbl_walikelas`;
CREATE TABLE `tbl_walikelas` (
  `id_walikelas` int(11) NOT NULL AUTO_INCREMENT,
  `id_guru` int(11) NOT NULL,
  `id_tahun_akademik` int(11) NOT NULL,
  `id_rombel` int(11) NOT NULL,
  PRIMARY KEY (`id_walikelas`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

#
# Data for table "tbl_walikelas"
#

INSERT INTO `tbl_walikelas` VALUES (7,2,1,1),(8,3,1,2),(9,1,1,3),(10,2,1,4);

#
# View "v_master_rombel"
#

DROP VIEW IF EXISTS `v_master_rombel`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `v_master_rombel`
  AS
  SELECT
    1 AS 'id_rombel',
    1 AS 'nama_rombel',
    1 AS 'kelas',
    1 AS 'kd_jurusan',
    1 AS 'nama_jurusan';

#
# View "v_tbl_user"
#

DROP VIEW IF EXISTS `v_tbl_user`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `v_tbl_user`
  AS
  SELECT
    1 AS 'id_user',
    1 AS 'nama_lengkap',
    1 AS 'username',
    1 AS 'password',
    1 AS 'id_level_user',
    1 AS 'foto',
    1 AS 'nama_level';
